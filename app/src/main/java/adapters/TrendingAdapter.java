package adapters;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import interfaces.OnListItemClickListener;
import model.RadarModel;


public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.ViewHolder>  {
    private ArrayList<RadarModel> trending_list;
    View v;
    FragmentActivity context;
    OnListItemClickListener onListItemClickListener;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teamlist_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RadarModel userDataModel = trending_list.get(position);
        AppDelegate.LogT("Radar Model in onBindViewHolder" + userDataModel + "");
        AppDelegate.LogT("Radar Model in " + userDataModel.coach_profile + "");
        holder.coach_name.setText(userDataModel.coach_profile.first_name+" "+ (AppDelegate.isValidString(userDataModel.coach_profile.last_name)?userDataModel.coach_profile.last_name: ""));
        holder.pkg.setText(String.valueOf(userDataModel.followers) + "");
      /*  if(AppDelegate.isValidString(userDataModel.coach_profile.avtar_thumb)) {*/
        if(!userDataModel.coach_profile.avtar_thumb.isEmpty()) {
            imageLoader.loadImage(userDataModel.coach_profile.avtar_thumb, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.image.setImageBitmap(loadedImage);
//                    holder.background_img.mWidth = bitmap.getWidth();
//                    holder.background_img.mHeight = bitmap.getHeight();
                    holder.background_img.setImageBitmap(AppDelegate.blurRenderScript(context, loadedImage));
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
          /*  Picasso.with(context).load(userDataModel.coach_profile.avtar_thumb).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.image.setImageBitmap(bitmap);
//                    holder.background_img.mWidth = bitmap.getWidth();
//                    holder.background_img.mHeight = bitmap.getHeight();
                    holder.background_img.setImageBitmap(AppDelegate.blurRenderScript(context, bitmap));
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });*/
        }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null) {
                    onListItemClickListener.setOnListItemClickListener("name", position);
                }
            }
        });
        holder.get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.get.isSelected()){
                    holder.get.setSelected(false);
                }else{
                    holder.get.setSelected(true);
                }
                if (onListItemClickListener != null) {
                    onListItemClickListener.setOnListItemClickListener("get", position);
                }
            }
        });
      /*  }*/
        /*if(AppDelegate.isValidString(userDataModel.coach_profile.avtar)) {
            Picasso.with(context).load(userDataModel.coach_profile.avtar).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("valid ="+userDataModel.coach_profile.avtar);
                    holder.background_img.mWidth = bitmap.getWidth();
                    holder.background_img.mHeight = bitmap.getHeight();
                    holder.background_img.setImageBitmap(AppDelegate.blurRenderScript(context, bitmap));
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });
        }*/
    }


    public TrendingAdapter(FragmentActivity context, ArrayList<RadarModel> trending_list, OnListItemClickListener onListItemClickListener) {
        this.context = context;
        this.trending_list = trending_list;
        this.onListItemClickListener = onListItemClickListener;
    }

    @Override
    public int getItemCount() {
        return trending_list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView get, coach_name, followers, pkg;
        ImageView star, dots;
        ImageView image;

        ImageView background_img;

        public ViewHolder(View itemView) {
            super(itemView);
            get = (TextView) itemView.findViewById(R.id.get);
            coach_name = (TextView) itemView.findViewById(R.id.coach_name);
            followers = (TextView) itemView.findViewById(R.id.followers);
            pkg = (TextView) itemView.findViewById(R.id.pkg);
            image = (ImageView) itemView.findViewById(R.id.img_content);
            background_img = (ImageView) itemView.findViewById(R.id.backgound_img);
        }
    }
}