package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.fitplus.R;

import java.util.ArrayList;



public class LanguageAdapter extends ArrayAdapter<String> {
    // TODO Auto-generated constructor stub
    private final Activity context;
    String [] title;
    Integer image;

    public LanguageAdapter(Activity context, String [] title) {
        super(context,R.layout.dialog_listitem, title);
        {
            this.context = context;
            this.title = title;
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.dialog_listitem, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.listvalue);
        txtTitle.setText(title[position]);

        return rowView;
    }
}
