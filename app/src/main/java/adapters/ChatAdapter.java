package adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.fitplus.ViewProfileActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Constants.Parameters;
import Constants.Tags;
import model.ChatModel;
import utils.DateUtils;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private final ArrayList<ChatModel> chat_list;
    View v;
    FragmentActivity context;
    private Bundle bundle;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
            /*.showImageForEmptyUri(R.drawable.dummy_user_icon)
            .showImageOnFail(fallback)
            .showImageOnLoading(fallback).*/build();
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ChatModel chatModel = chat_list.get(position);
        if (chatModel.chat_status == ChatModel.FROM_ACTION) {
            holder.ll_action.setVisibility(View.VISIBLE);
            holder.lin_receiver.setVisibility(View.GONE);
            holder.lin_sender.setVisibility(View.GONE);
            if (chatModel.action_message.contains("null")) {
               String msg= chatModel.action_message.replace("null", "");
                holder.txt_msg_action.setText(msg);
            }else{
                holder.txt_msg_action.setText(chatModel.action_message);
            }
           // holder.txt_msg_action.setText(chatModel.action_message);
        } else if (chatModel.chat_status == ChatModel.FROM_USER) {
            holder.ll_action.setVisibility(View.GONE);
            holder.lin_receiver.setVisibility(View.GONE);
            holder.lin_sender.setVisibility(View.VISIBLE);
            holder.txt_msg_sender.setText(chatModel.txt_msg_send);
            String time = chatModel.txt_time_send;
            AppDelegate.LogT("time = " + time);
            if (AppDelegate.isValidString(time)) {
                try {
                    time = time.replaceAll("/+0000", "");
                    Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time);
                    if (DateUtils.isToday(date)) {
                        time = new SimpleDateFormat("hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                    } else {
                        time = new SimpleDateFormat("dd MMM HH:mm").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                    }
                } catch (ParseException e) {
                    AppDelegate.LogE(e);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
            holder.txt_time_sender.setText(time);
//            2016-08-17T12:19:53+0000
        } else {
            holder.ll_action.setVisibility(View.GONE);
            holder.lin_receiver.setVisibility(View.VISIBLE);
            holder.lin_sender.setVisibility(View.GONE);
            holder.txt_msg_rcv.setText(chatModel.txt_msg_rcv);

            String time = chatModel.txt_time_rcv;
            AppDelegate.LogT("time = " + time);
            if (AppDelegate.isValidString(time)) {
                try {
                    time = time.replaceAll("/+0000", "");
                    Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time);
                    if (DateUtils.isToday(date)) {
                        time = new SimpleDateFormat("hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                    } else {
                        time = new SimpleDateFormat("dd MMM").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                    }
                } catch (ParseException e) {
                    AppDelegate.LogE(e);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
            holder.txt_time_rcv.setText(time);
            if (chatModel.txt_name_rcv.contains("null")) {
              String name=  chatModel.txt_name_rcv.replace("null", "");
                holder.txt_name_rcv.setText(name);
            }else{
                holder.txt_name_rcv.setText(chatModel.txt_name_rcv);
            }
            imageLoader.loadImage(chatModel.txt_pic_rcv, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.img_pic_rcv.setImageBitmap(loadedImage);

                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
           /* Picasso.with(context).load(chatModel.txt_pic_rcv).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("onBitmapLoaded" + chatModel.txt_pic_rcv);
                    holder.img_pic_rcv.setImageBitmap(AppDelegate.blurRenderScript(context, bitmap));
//                    try {
//                        notifyDataSetChanged();
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("onBitmapFailed" + chatModel.txt_pic_rcv);
//                    try {
//                        notifyDataSetChanged();
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("onPrepareLoad" + chatModel.txt_pic_rcv);
//                    try {
//                        notifyDataSetChanged();
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
                }
            });*/
            holder.rl_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(context, ViewProfileActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt(Parameters.login_id, chatModel.id);
                        bundle.putInt(Tags.FROM, ViewProfileActivity.FROM_VIEW);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }

    }

    public ChatAdapter(FragmentActivity context, ArrayList<ChatModel> chat_list) {
        this.context = context;
        this.chat_list = chat_list;
        // this. ds=ds;
    }

    @Override
    public int getItemCount() {
        return chat_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_msg_sender, txt_time_sender, txt_msg_rcv, txt_time_rcv, txt_name_rcv, txt_msg_action;
        ImageView img_pic_rcv;
        LinearLayout lin_sender, lin_receiver, ll_action;
        RelativeLayout rl_user;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_msg_sender = (TextView) itemView.findViewById(R.id.txt_msg_sender);
            txt_time_sender = (TextView) itemView.findViewById(R.id.txt_time_sender);
            txt_msg_rcv = (TextView) itemView.findViewById(R.id.txt_msg_rcv);
            txt_time_rcv = (TextView) itemView.findViewById(R.id.txt_time_rcv);
            txt_name_rcv = (TextView) itemView.findViewById(R.id.txt_name_rcv);
            txt_msg_action = (TextView) itemView.findViewById(R.id.txt_msg_action);
            ll_action = (LinearLayout) itemView.findViewById(R.id.ll_action);
            lin_sender = (LinearLayout) itemView.findViewById(R.id.lin_send);
            lin_receiver = (LinearLayout) itemView.findViewById(R.id.lin_receive);
            img_pic_rcv = (ImageView) itemView.findViewById(R.id.img_pic_rcv);
            rl_user = (RelativeLayout) itemView.findViewById(R.id.rl_user);
        }
    }
}