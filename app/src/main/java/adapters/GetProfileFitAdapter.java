package adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.ImageShowActivity;
import com.fitplus.PlayVideoActivity;
import com.fitplus.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.FitDayModel;
import staggeredView.DataSet;


public class GetProfileFitAdapter extends RecyclerView.Adapter<GetProfileFitAdapter.ViewHolder> {
    private final ArrayList<FitDayModel> trending_list;
    View v;
    DataSet ds;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fit_day_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FitDayModel userDataModel = trending_list.get(position);
       // holder.text.setText(userDataModel.comment);
        imageLoader.loadImage(userDataModel.file_thumb, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.image.setImageBitmap(loadedImage);

            }
            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
            /*Picasso.with(context).load(userDataModel.file_thumb).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.image.setImageBitmap(bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });*/
        if(userDataModel.file_type==1) {
            holder.video.setVisibility(View.GONE);
        }
        else if(userDataModel.file_type==2) {
            holder.video.setVisibility(View.VISIBLE);
                   }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userDataModel.file_type==1){
                    Intent intent=new Intent(context, ImageShowActivity.class);
                    bundle=new Bundle();
                    bundle.putParcelable("FitDay",userDataModel);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                }
                else if(userDataModel.file_type==2){
                    Intent intent=new Intent(context, PlayVideoActivity.class);
                    bundle=new Bundle();
                    bundle.putParcelable("FitDay",userDataModel);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                }
            }
        });
    }

    public GetProfileFitAdapter(FragmentActivity context, ArrayList<FitDayModel> trending_list) {
        this.context = context;
        this.trending_list = trending_list;
        // this. ds=ds;
    }

    @Override
    public int getItemCount() {
        return trending_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text;
        ImageView star, video;
        ImageView image;
        LinearLayout card,hide_visibility;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_content);
            video=(ImageView)itemView.findViewById(R.id.video);

        }
    }
}