package adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.fitplus.UserActionActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Constants.Tags;
import model.Notification_Fragments_Model;
import staggeredView.DataSet;


public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    private final ArrayList<Notification_Fragments_Model> trending_list;
    View v;
    DataSet ds;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Notification_Fragments_Model userDataModel = trending_list.get(position);
        holder.last_msg.setText(userDataModel.message);
        holder.team_name.setText(userDataModel.teamname);
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.NotificationList, userDataModel);
                Intent intent = new Intent(context, UserActionActivity.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
                // AppDelegate.showFragment(context.getSupportFragmentManager(), new CreateTeamActivity(), R.id.container, bundle, null);
            }
        });
        // holder.time.setText(userDataModel.created);
        AppDelegate.LogT("userDataModel.team_avtar => " + userDataModel.team_avtar);
        if (AppDelegate.isValidString(userDataModel.team_avtar)) {
            holder.img_loading.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                    frameAnimation.setCallback(holder.img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) holder.img_loading.getDrawable()).start();
                }
            });

            imageLoader.loadImage(userDataModel.team_avtar, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.image.setImageBitmap(loadedImage);
                    holder.img_loading.setVisibility(View.GONE);

                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

          /*  Picasso.with(context).load(userDataModel.team_avtar).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.image.setImageBitmap(bitmap);
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });*/
        }
    }

    public NotificationListAdapter(FragmentActivity context, ArrayList<Notification_Fragments_Model> trending_list) {
        this.context = context;
        this.trending_list = trending_list;
        // this. ds=ds;
    }

    @Override
    public int getItemCount() {
        return trending_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView last_msg, team_name, time;
        ImageView star, dots;
        ImageView image, img_loading;
        LinearLayout card;

        public ViewHolder(View itemView) {
            super(itemView);
            last_msg = (TextView) itemView.findViewById(R.id.lastmsg);
            image = (ImageView) itemView.findViewById(R.id.team_icon);
            img_loading = (ImageView) itemView.findViewById(R.id.img_loading);
            team_name = (TextView) itemView.findViewById(R.id.team_name);
            time = (TextView) itemView.findViewById(R.id.time);
            card = (LinearLayout) itemView.findViewById(R.id.card);
        }
    }
}