package adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.R;
import fragments.ViewImageVideoFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.FitDayModel;
import staggeredView.DataSet;
import staggeredView.STGVImageView;


public class FitDayAdapter extends RecyclerView.Adapter<FitDayAdapter.ViewHolder> {
    private final ArrayList<FitDayModel> trending_list;
    View v;
    DataSet ds;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
          build();
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trending_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FitDayModel userDataModel = trending_list.get(position);
        holder.text.setText(userDataModel.comment);
        holder.count.setText(String.valueOf(userDataModel.view));
        holder.star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.star.isSelected()){
                    holder.star.setSelected(false);
                }else{
                    holder.star.setSelected(true);
                }
            }
        });
        if (userDataModel.file_type == 1) {
            imageLoader.loadImage(userDataModel.file_name, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.image.mWidth = loadedImage.getWidth();
                    holder.image.mHeight = loadedImage.getHeight();
                    holder.image.setImageBitmap(loadedImage);

                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
           /* Picasso.with(context).load(userDataModel.file_name).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.image.mWidth = bitmap.getWidth();
                    holder.image.mHeight = bitmap.getHeight();
                    holder.image.setImageBitmap(bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });*/
            holder.video.setVisibility(View.GONE);
        } else if (userDataModel.file_type == 2) {
            imageLoader.loadImage(userDataModel.file_thumb, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.image.mWidth = loadedImage.getWidth();
                    holder.image.mHeight = loadedImage.getHeight();
                    holder.image.setImageBitmap(loadedImage);

                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

            holder.video.setVisibility(View.VISIBLE);
        }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewImageVideoFragment.class);
                bundle = new Bundle();
                bundle.putParcelableArrayList("FitDayActivity", trending_list);
                intent.putExtras(bundle);
                intent.putExtra("position",position);
               // intent.putExtra(Tags.abuse,1);
                context.startActivity(intent);
              /*  if (userDataModel.file_type == 1) {
                    Intent intent = new Intent(context, ImageShowActivity.class);
                    bundle = new Bundle();
                    bundle.putParcelable("FitDay", userDataModel);
                    intent.putExtras(bundle);
                    intent.putExtra(Tags.abuse,1);
                    context.startActivity(intent);
                } else if (userDataModel.file_type == 2) {
                    Intent intent = new Intent(context, PlayVideoActivity.class);
                    bundle = new Bundle();
                    bundle.putParcelable("FitDay", userDataModel);
                    intent.putExtras(bundle);
                    intent.putExtra(Tags.abuse,1);
                    context.startActivity(intent);
                }*/
            }
        });
    }
    public FitDayAdapter(FragmentActivity context, ArrayList<FitDayModel> trending_list) {
        this.context = context;
        this.trending_list = trending_list;
        // this. ds=ds;
    }

    @Override
    public int getItemCount() {
        return trending_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text,count;
        ImageView star, video;
        STGVImageView image;
        LinearLayout card;
        public ViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.desc);
            count = (TextView) itemView.findViewById(R.id.count);
            image = (STGVImageView) itemView.findViewById(R.id.img_content);
            star = (ImageView) itemView.findViewById(R.id.star);
            video = (ImageView) itemView.findViewById(R.id.video);
        }
    }
}