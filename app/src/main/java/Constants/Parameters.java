package Constants;

/**
 * Created by admin on 16-06-2016.
 */
public class Parameters {
    public static final String API_KEY = "apiKey";
    public static final String API_KEY_VALUE = "789";
    public static final String first_name = "first_name";
    public static final String last_name = "last_name";
    public static final String email = "email";
    public static final String password = "password";
    public static final String post_code = "post_code";
    public static final String device_type = "device_type";
    public static final String device_id = "device_id";
    public static final String country_id = "country_id";
    public static final String state_id = "state_id";
    public static final String city_id = "city_id";
    public static final String uploadedfile0 = "uploadedfile0";
    public static final String saveForgot = "saveForgot";
    public static final String user_id = "user_id";
    public static final String id = "id";
    public static final String city = "city";
    public static final String address = "address";
    public static final String name = "name";
    public static final String about_me = "about_me";
    public static final String notification = "notification";
    public static final String gender = "gender";
    public static final String birthdate = "birthdate";
    public static final String nick_name = "nick_name";
    public static final String category_one = "category_one";
    public static final String category_two = "category_two";
    public static final String category_three = "category_three";
    public static final String comment = "comment";
    public static final String file = "file";
    public static final String coach_id = "coach_id";
    public static final String encodedData = "encodedData";
    public static final String delimiter = ":~";
    public static final String seperator = "|";
    public static final String team_id = "team_id";
    public static final String type = "type";
    public static final String action = "action";
    public static final int accept = 1;
    public static final int reject = 2;
    public static final int do_not_disturb = 3;
    public static final String training_date = "training_date";
    public static final String training_period = "training_period";
    public static final String devicetype = "devicetype";
    public static final String deviceType = "deviceType";
    public static final String login_id = "login_id";
    public static final String follower_id = "follower_id";
    public static final String notify_id = "notify_id";
    public static final String date = "date";
    public static final String dateTime = "dateTime";
    public static final String fitday_id = "fitday_id";
    public static final String user_comments = "user_comments";
    public static final String session_id = "session_id";
    public static final String notification_status = "notification_status";
    public static final String language = "language";
    public static final String time = "time";

    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
}
