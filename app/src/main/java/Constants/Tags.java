package Constants;

/**
 * Created by admin on 26-05-2016.
 */
public class Tags {
    public static final String TAG_LAT = "lat";
    public static final String TAG_LONG = "long";
    public static final String TAG_LOGIN = "LoginResponse";
    public static final String TAG_SIGNUP = "signupResponse";
    public static final String ERROR = "error";
    public static final String BRANCH = "";
    public static final String PROVINCE = "province";
    public static final String CITY = "city";
    public static final String LOCATION = "location";
    public static final String AUTOPROVINCE = "autoprovince";
    public static final String AUTOCITY = "autocity";
    public static final String AUTOLOCATION = "autolocation";
    public static final String TAG_MYVEHICLELIST = "myvehiclelist";
    public static final String TAGMAKE = "make";
    public static final String EDITPROFILE = "editprofile";
    public static final String TAG_SELECTVEHICLE = "selectvehicle";
    public static final String URL = "url";
    public static final String URL_API = "urlApi";
    public static final String BHARAT = "Bharat";
    public static final String DATE = "Date";
    public static final String DATABASE = "Database";

    public static final String URL_RESPONSE = "urlResponse";
    public static final String URL_POST = "urlPost";
    public static final String TEST = "test";
    public static final String TRACKER = "Tracker";
    public static final String FORMATED = "formatted";
    public static final String SERVICE = "service";

    public static final String RESULT = "result";
    public static final String INTERNET = "intertnet";

    public static final String PREF = "pref";

    public static final String GCM = "GCM";

    public static final String GPS = "GPS";

    public static final String NULL = "null";
    public static final String FACEBOOK = "facebook";


    public static final String PUSHER = "pusher";

    public static final String POST = "post";

    public static final String user_id = "id";

    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String city = "city";

    public static final String result = "result";
    public static final String location = "location";


    public static final String status = "status";


    public static final String size = "size";
    public static final String content = "content";
    public static final String value = "value";
    public static final String exception = "exception";
    public static final String parent = "parent";
    public static final String name = "name";
    public static final String message = "message";
    public static final String last_name = "last_name";
    public static final String PUBNUB = "pubnub";
    public static final String FILE = "FILE";
    public static final String address = "address";
    public static final String TAG_Coupan = "Coupan_List";
    public static final String TAG_REDEEM = "redeem";
    public static final String REQUEST_RESPONSE = "responserequest";
    public static final String THANKS = "thanks";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String PLACE_NAME = "PLACE_NAME";
    public static final String PLACE_ADD = "PLACE_ADD";
    public static final String ADDRESS = "ADDRESS";
    public static final String city_param = "city_param";
    public static final String country_param = "country_param";
    public static final String postalCode = "postalCode";
    public static final String STATE = "state";
    public static final String jpg = "jpg";
    public static final String png = "png";
    public static final String svg = "svg";
    public static final String Location = "location";
    public static final String IDENTIFY_DRIVER = "DRIVER";
    public static final String IDENTIFY_FAN = "FAN";
    public static final String IDENTIFIER = "IDENTIFIER";
    public static final String REGISTRATION_ID = "REGISTRATION_ID";
    public static final String TRACK_ID = "TRACK_ID";
    public static final String TRACK_NAME = "TRACK_NAME";
    public static final String LOGIN_STATUS = "Login_status";
    public static final String LOGIN = "LOGIN";
    public static final String LOGOUT = "LOGOUT";
    public static final String parcel_becomeCoach = "parcel_becomeCoach";
    public static final String parcel_Detail = "parcel_Detail";
    public static final String parcel_FanDetail = "parcel_FanDetail";
    public static final String parcel_ProductDetail = "parcel_ProductDetail";
    public static final String LIST_ITEM_TRENDING = "LIST_ITEM_TRENDING";
    public static final String buydriver = "buydriver";
    public static final String competitionID = "competitionID";
    public static final String raceID = "raceID";
    public static final String parcel_shopHistoryItem = "parcel_shopHistoryItem";
    public static final String parcel_pointsHistoryItem = "parcel_pointsHistoryItem";
    public static final String parcel_gameHistoryItem = "parcel_gameHistoryItem";
    public static final String driverEmail = "driverEmail";
    public static final String driverPassword = "driverPassword";
    public static final String FanEmail = "driverEmail";
    public static final String fanPassword = "driverPassword";
    public static final String auth_key = "auth_key";
    public static final String USER_DATA = "USER_DATA";
    public static final String response = "response";
    public static final String first_name = "first_name";
    public static final String email = "email";

    public static final String birthdate = "birthdate";
    public static final String nick_name = "nick_name";
    public static final String device_id = "device_id";
    public static final String device_type = "device_type";
    public static final String created = "created";
    public static final String modified = "modified";
    public static final String apiKey = "apiKey";
    public static final String password = "password";
    public static final String dataFlow = "dataFlow";
    public static final String gender = "gender";
    public static final String object = "object";
    public static final String birthday = "birthday";
    public static final String id = "id";

    public static final String social_id = "social_id";
    public static final String ok = "ok";
    public static final String avtar = "avtar";
    public static final String lastMessage = "lastMessage";
    public static final String views = "views";
    public static final String follower_count = "follower_count";
    public static final String profile_visibility = "profile_visibility";
    public static final String presentation_vedio = "presentation_vedio";
    public static final String bank_account = "bank_account";
    public static final String certificates = "certificates";
    public static final String role_id = "role_id";
    public static final String country_id = "country_id";

    public static final String state_id = "state_id";
    public static final String city_id = "city_id";
    public static final String post_code = "post_code";
    public static final String sex_group = "sex_group";
    public static final String fat_status = "fat_status";
    public static final String type = "type";
    public static final String token = "token";
    public static final String is_login = "is_login";
    public static final String is_social = "is_social";
    public static final String is_verified = "is_verified";
    public static final String Email = "Email";
    public static final String Password = "Password";
    public static final String avtar_thumb = "avtar_thumb";
    public static final String TAG_SIGNUPDATA = "TAG_SIGNUPDATA";
    public static final String normal = "normal";
    public static final String follower = "follower";
    public static final String file_name = "file_name";
    public static final String file_type = "file_type";
    public static final String view = "view";
    public static final String file_thumb = "file_thumb";
    public static final String comment = "comment";
    public static final String coach_id = "coach_id";
    public static final String team_name = "team_name";
    public static final String TeamName = "TeamName";
    public static final String objective = "objective";
    public static final String first_member_id = "first_member_id";
    public static final String second_member_id = "second_member_id";
    public static final String third_member_id = "third_member_id";

    public static final String cancel = "cancel";
    public static final String Find_Coach = "Find_Coach";
    public static final int Prefindcoach = 0;
    public static final String teamfindcoach = "teamfindcoach";
    public static final String request_id = "request_id";
    public static final String coach_action = "coach_action";
    public static final String final_status = "final_status";
    public static final String followers = "followers";
    public static final String distance = "distance";
    public static final String createName = "createName";
    public static final String teamlist = "teamlist";
    public static final String radarresponse = "radarresponse";
    public static final String follower_id = "follower_id";
    public static final String gid = "gid";
    public static final String fid = "fid";
    public static final String verify = "verify";
    public static final String data = "data";
    public static final String about_me = "about_me";
    public static final String is_followed = "is_followed";
    public static final String FitDayModel = "FitDayModel";

    public static final String FROM = "FROM";
    public static final String CHAT = "CHAT";
    public static final String PAGE = "PAGE";
    public static final String follow = "follow";

    public static final String sender_id = "sender_id";
    public static final String team_id = "team_id";
    public static final String sender_avtar = "sender_avtar";
    public static final String sender_name = "sender_name";

    public static final String team_avtar = "team_avtar";
    public static final String notification = "notification";
    public static final String notification_value = "notification_value";
    public static final String notification_status = "notification_status";
    public static final String HOST_USER = "hostUser";
    public static final String HOST_TEAM = "hostTeam";
    public static final String block_on = "block_on";
    public static final String block_for = "block_for";

    public static final String remember = "remember";
    public static final String notificaton_type = "notificaton_type";
    public static final String action_taken = "action_taken";
    public static final String teamMembers = "teamMembers";
    public static final String teamname = "teamname";
    public static final String NotificationList = "NotificationList";
    public static final String user = "user";
    public static final String coach = "coach";
    public static final String Request = "Request";
    public static final String training_date = "training_date";
    public static final String request = "request";
    public static final String teamAvtar = "teamAvtar";
    public static final String teamName = "teamName";
    public static final String calender = "calender";
    public static final String user1_action = "user1_action";
    public static final String user1_id = "user1_id";
    public static final String user2_id = "user2_id";
    public static final String user3_id = "user3_id";
    public static final String user1_date = "user1_date";
    public static final String user2_action = "user2_action";
    public static final String request_type = "request_type";
    public static final String user3_action = "user3_action";
    public static final String training_period = "training_period";
    public static final String is_confirm = "is_confirm";
    public static final String username_first = "username_first";
    public static final String username_second = "username_second";
    public static final String username_third = "username_third";
    public static final String FirstAvtar = "FirstAvtar";
    public static final String SecondAvtar = "SecondAvtar";
    public static final String ThirdAvtar = "ThirdAvtar";
    public static final String teamMembersAvtar = "teamMembersAvtar";
    public static final String schedule = "schedule";
    public static final String video_id = "video_id";
    public static final String video = "video";
    public static final String datetime = "datetime";
    public static final String abuse = "abuse";
    public static final String viewstatus = "viewstatus";
    public static final String log_hours = "log_hours";
    public static final String radar = "radar";
    public static final String language = "language";
    public static final String time = "time";
    public static final String back = "back";
    public static final String picture = "picture";
    public static final String profile_url = "profile_url";
    public static final String finish = "finish";
    public static final String invite_friends = "invite_friends";
}
