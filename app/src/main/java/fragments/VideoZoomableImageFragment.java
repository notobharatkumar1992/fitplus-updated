package fragments;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import Constants.Tags;
import UI.TouchImageView;
import model.FitDayModel;
import universalvideoview.UniversalMediaController;
import universalvideoview.UniversalVideoView;
import utils.MoveGestureDetector;

/**
 * Created by admin on 17-08-2016.
 */

public class VideoZoomableImageFragment extends Fragment implements View.OnTouchListener, UniversalVideoView.VideoViewCallback, SeekBar.OnSeekBarChangeListener {
    private Handler mHandler = new Handler();
    ImageView image1, image2, image3, image4, image5, image6;
    private Bundle bundle;
    TextView text;
    // ImageView star, video;
    TouchImageView image;
    // public VideoView videoView;
    RelativeLayout ll_main, hide_visibility;
    FitDayModel fitDayModelArrayList;
    UniversalMediaController mMediaController;
    Boolean show = false;
    int position;
    ImageLoader imageLoader = ImageLoader.getInstance();
    UniversalVideoView mVideoView;
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
            /*.showImageForEmptyUri(R.drawable.dummy_user_icon)
            .showImageOnFail(fallback)
            .showImageOnLoading(fallback).*/build();
    private int cachedHeight;
    private DonutProgress donut_progress;
    private CountDownTimer countDownTimer;
    public final static int SWIPE_UP = 1;
    public final static int SWIPE_DOWN = 2;
    public final static int SWIPE_LEFT = 3;
    public final static int SWIPE_RIGHT = 4;

    public final static int MODE_TRANSPARENT = 0;
    public final static int MODE_SOLID = 1;
    public final static int MODE_DYNAMIC = 2;

    private final static int ACTION_FAKE = -13; //just an unlikely number
    private int swipe_Min_Distance = 100;
    private int swipe_Max_Distance = 350;
    private int swipe_Min_Velocity = 100;
    private GestureDetector gestureDetector;
    View.OnTouchListener gestureListener;
    private MoveGestureDetector mMoveDetector;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bundle = getArguments();
        fitDayModelArrayList = bundle.getParcelable(Tags.FitDayModel);
        position = bundle.getInt("position", 0);
        gestureDetector = new GestureDetector(getActivity(), new MyGestureDetector());
        return inflater.inflate(R.layout.fit_day_item_zoomable, container, false);

    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            if (e1.getX() < e2.getX()) {
                AppDelegate.LogT("Left to Right swipe performed");
            }

            if (e1.getX() > e2.getX()) {
                AppDelegate.LogT("Right to Left swipe performed");
            }

            if (e1.getY() < e2.getY()) {
                AppDelegate.LogT("Up to Down swipe performed");
                if (ViewImageVideoFragment.onListItemClickListener != null) {
                    ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("backfinish", position);
                }
                result = true;
                return true;
            }

            if (e1.getY() > e2.getY()) {
                AppDelegate.LogT("Down to Up swipe performed");

            }
            return result;
        }

        /*final float xDistance = Math.abs(e1.getX() - e2.getX());
        final float yDistance = Math.abs(e1.getY() - e2.getY());

        if (xDistance > swipe_Max_Distance || yDistance > swipe_Max_Distance)
            return false;

        velocityX = Math.abs(velocityX);
        velocityY = Math.abs(velocityY);
        boolean result = false;

        if (velocityX > swipe_Min_Velocity && xDistance > swipe_Min_Distance) {
           *//* if(e1.getX() > e2.getX()) // right to left
                   // listener.onSwipe(SWIPE_LEFT);
                else
                   // listener.onSwipe(SWIPE_RIGHT);

                result = true;*//*
            } else if (velocityY > swipe_Min_Velocity && yDistance > swipe_Min_Distance) {
                if (e1.getY() > e2.getY()) // bottom to up
                {
                    // listener.onSwipe(SWIPE_UP);
                } else {
                    AppDelegate.LogT("swipe down by method");
                    if (ViewImageVideoFragment.onListItemClickListener != null) {
                        ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("backfinish", position);
                    }
                }
                result = true;
            }

            return result;
        }
*/
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            /*if (ViewImageVideoFragment.onListItemClickListener != null) {
                ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("backfinish", position);
            }*/
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            AppDelegate.LogT("swipe onSingleTapConfirmed");
            if (ViewImageVideoFragment.onListItemClickListener != null) {
                ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("clicked", position);
            }
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onContextClick(MotionEvent e) {
            if (ViewImageVideoFragment.onListItemClickListener != null) {
                ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("clicked", position);
            }
            AppDelegate.LogT("swipe onContextClick");
            return super.onContextClick(e);
        }

        @Override
        public boolean onDown(MotionEvent e) {

            AppDelegate.LogT("swipe down");
            return true;
        }
    }


   /* private class MoveListener extends MoveGestureDetector.SimpleOnMoveGestureListener {
        @Override
        public boolean onMove(MoveGestureDetector detector) {
            AppDelegate.LogT("finally get gesture detector fragment");
            // mFocusX = detector.getFocusX();
            // mFocusY = detector.getFocusY();
            return true;
        }
    }*/

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        // mMoveDetector.onTouchEvent(event);
        return true;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (ViewImageVideoFragment.onListItemClickListener != null) {
                ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("visibility", 1);
            }
        } else {
            if (ViewImageVideoFragment.onListItemClickListener != null) {
                ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("visibility", 2);
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        image = (TouchImageView) view.findViewById(R.id.img_content);
        // videoView = (VideoView) view.findViewById(R.id.videoView);
        mVideoView = (UniversalVideoView) view.findViewById(R.id.videoView);
        donut_progress = (DonutProgress) view.findViewById(R.id.donut_progress);
        donut_progress.setVisibility(View.INVISIBLE);
        mMediaController = (UniversalMediaController) view.findViewById(R.id.media_controller);
        mVideoView.setVideoViewCallback(this);
        ll_main = (RelativeLayout) view.findViewById(R.id.ll_main);
        view.findViewById(R.id.main_view).setOnTouchListener(this);
        //mMoveDetector = new MoveGestureDetector(getActivity(), new MoveListener());
        view.findViewById(R.id.main_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.LogT(" view.setOnClickListener => " + (ViewImageVideoFragment.onListItemClickListener != null));
                if (ViewImageVideoFragment.onListItemClickListener != null) {
                    ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("clicked", position);
                }
            }
        });
        //   AppDelegate.LogT("File type => " + fitDayModelArrayList.file_type);
        if (fitDayModelArrayList.file_type == 1) {
            donut_progress.setVisibility(View.INVISIBLE);
            AppDelegate.LogT("File type => " + fitDayModelArrayList.file_type + ", " + fitDayModelArrayList.file_name);
            image.setVisibility(View.VISIBLE);
            mVideoView.setVisibility(View.GONE);
            imageLoader.displayImage(fitDayModelArrayList.file_name, image, options);
        } else if (fitDayModelArrayList.file_type == 2) {
            AppDelegate.LogT("File type => " + fitDayModelArrayList.file_type + ", " + fitDayModelArrayList.file_name);
            image.setVisibility(View.GONE);
            mVideoView.setVisibility(View.VISIBLE);
            playvdo(fitDayModelArrayList.file_name);
        }
    }

    private void playvdo(final String url) {

        mVideoView.setVisibility(View.VISIBLE);
        AppDelegate.LogT("String =" + position + ", url=" + url);
        if (ViewImageVideoFragment.onListItemClickListener != null) {
            ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("playing", position);
        }
    }

    private void updateProgressBar() {
        mHandler.postDelayed(updateTimeTask, 50);
    }

    private Runnable updateTimeTask = new Runnable() {
        public void run() {
            donut_progress.setMax(mVideoView.getDuration());
            donut_progress.setProgress(mVideoView.getDuration()-mVideoView.getCurrentPosition());
            mHandler.postDelayed(this, 50);
        }
    };


    public void playVideo(final String url) {

        donut_progress.setUnfinishedStrokeColor(Color.WHITE);
        donut_progress.setFinishedStrokeColor(R.color.blackish);
        mVideoView.setMediaController(mMediaController);
        updateProgressBar();
        AppDelegate.LogT("Start playvideo by position=" + position + " url  " + url);
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                AppDelegate.LogT("onPrepared => " + mp.getDuration() + ", " + position);
//                AppDelegate.showToast(getActivity(), "Can't play this video");
                if (ViewImageVideoFragment.onListItemClickListener != null) {
                    ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("clicked", position);
                }
                return true;
            }
        });
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(final MediaPlayer mp) {
                show = true;
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                    countDownTimer = null;
                }
                AppDelegate.LogT("onPrepared => " + mp.getDuration() + ", ");
                image.setVisibility(View.GONE);
                mVideoView.setVisibility(View.VISIBLE);
                donut_progress.setVisibility(View.VISIBLE);
          /*      if(mp.isPlaying()) {
                    try {
                        final int time = mp.getDuration();
                        final int duration = mp.getCurrentPosition();

                        AppDelegate.LogT("duration with position in timer==" + time + " position==" + position);
                        donut_progress.setMax(mVideoView.getDuration());

                        donut_progress.setUnfinishedStrokeColor(R.color.blackish);
                        donut_progress.setFinishedStrokeColor(Color.WHITE);
                        donut_progress.setProgress(duration);
                        countDownTimer = new CountDownTimer(time, 1) {
                            @Override
                            public void onTick(long dura) {
                                AppDelegate.LogT("duration with  mp.getCurrentPosition()==" + mp.getCurrentPosition() + " dura==" + dura);
                                donut_progress.setProgress(mVideoView.getCurrentPosition());
                            }

                            @Override
                            public void onFinish() {
                                AppDelegate.LogT("duration with  onFinish==" + time);
                                donut_progress.setProgress(mVideoView.getDuration());
                            }
                        };
                        countDownTimer.start();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }*/
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        image.setVisibility(View.VISIBLE);
                        if (ViewImageVideoFragment.onListItemClickListener != null) {
                            ViewImageVideoFragment.onListItemClickListener.setOnListItemClickListener("stopped", position);
                        }
                    }
                });
            }
        });
        mVideoView.setVideoURI(Uri.parse(url));
        if (mVideoView != null) {
            mVideoView.requestFocus();
            AppDelegate.LogT("Start playvideo by =" + position);
            mVideoView.start();
        }
    }

    private void setVideoAreaSize(final String VIDEO_URL) {
        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoView.getWidth();
                cachedHeight = (int) (width * 405f / 720f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoView.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                mVideoView.setLayoutParams(videoLayoutParams);
                // mVideoView.requestFocus();
            }
        });
    }

    @Override
    public void onScaleChange(boolean isFullscreen) {
        AppDelegate.LogT("Media onScaleChange => ");
    }

    @Override
    public void onPause(MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onPause => ");
    }

    @Override
    public void onStart(final MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onBufferingStart => ");
    }

    @Override
    public void onBufferingStart(MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onBufferingStart => ");
    }

    @Override
    public void onBufferingEnd(MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onBufferingEnd => ");
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(updateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(updateTimeTask);
        mVideoView.seekTo(donut_progress.getProgress());
        updateProgressBar();
    }
}

