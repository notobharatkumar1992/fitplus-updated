package fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import Constants.Tags;
import model.FitDayModel;

/**
 * Created by admin on 17-08-2016.
 */

public class VideoFragment extends Fragment {
    private View rootview;
    ImageView image1, image2, image3, image4, image5, image6;
    private Bundle bundle;
    TextView text;
    ImageView star, video;
    ImageView image;
    VideoView videoView;
    LinearLayout card, hide_visibility;
    FitDayModel fitDayModelArrayList;
    private MediaController mMediaController;
    Boolean show = false;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).showImageForEmptyUri(R.drawable.img).
    /* .showImageOnFail(fallback)
     .showImageOnLoading(fallback).*/
            build();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fit_day_item, container, false);
        bundle = getArguments();
        fitDayModelArrayList = bundle.getParcelable(Tags.FitDayModel);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = (ImageView) rootview.findViewById(R.id.img_content);
        video = (ImageView) rootview.findViewById(R.id.video);
        videoView = (VideoView) rootview.findViewById(R.id.videoView);
        imageLoader.loadImage(fitDayModelArrayList.file_thumb, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                image.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

/*
        Picasso.with(getActivity()).load(fitDayModelArrayList.file_thumb).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                image.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });*/
        if (fitDayModelArrayList.file_type == 1) {
            video.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
        } else if (fitDayModelArrayList.file_type == 2) {
            video.setVisibility(View.VISIBLE);
           // videoView.setVisibility(View.VISIBLE);
        }
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fitDayModelArrayList.file_type == 1) {
                       /* Intent intent = new Intent(getActivity(), ImageShowActivity.class);
                        bundle = new Bundle();
                        bundle.putParcelable("FitDay", fitDayModelArrayList);
                        intent.putExtras(bundle);
                        getActivity().startActivity(intent);
*/
                } else if (fitDayModelArrayList.file_type == 2) {
                       /* Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                        bundle = new Bundle();
                        bundle.putParcelable("FitDay", fitDayModelArrayList);
                        intent.putExtras(bundle);
                        getActivity().startActivity(intent);*/
                    playvdo(fitDayModelArrayList.file_name);

                }
            }
        });


    }

    private void playvdo(String url) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();
        mMediaController = new MediaController(getActivity());
        videoView.setMediaController(mMediaController);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (show == false) {
                    pd.dismiss();
                    // AppDelegate.showDialog_okCancel(PlayVideoActivity.this, "Service Time Out!!!", "next", "retry", PlayVideoActivity.this);
                }
            }
        }, 60000);
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("video", "setOnErrorListener ");
                pd.dismiss();
                video.setVisibility(View.VISIBLE);
                AppDelegate.ShowDialog(getActivity(), "Can't play this vedio", "Alert");
                return true;
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                show = true;
                pd.dismiss();
                AppDelegate.LogT("");
                video.setVisibility(View.GONE);
                videoView.setVisibility(View.VISIBLE);
                image.setVisibility(View.GONE);
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        video.setVisibility(View.VISIBLE);
                        video.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.play));
                    }
                });
            }
        });
        videoView.setVideoURI(Uri.parse(url));
        videoView.requestFocus();
        videoView.start();
    }
}

