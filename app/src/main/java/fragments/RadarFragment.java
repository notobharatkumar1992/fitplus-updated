package fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.fitplus.AppDelegate;
import com.fitplus.CoachListActivity;
import com.fitplus.CreateTeamActivity;
import com.fitplus.R;
import com.fitplus.ViewProfileActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.RadarModel;
import model.TeamListModel;
import model.UserDataModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class RadarFragment extends Fragment implements OnClickListener, OnReciveServerResponse {

    Activity mActivity;
    private int i;
    Bundle bundle = new Bundle();
    private Intent intent;
    private RelativeLayout contains;
    int left_margin = 100;
    int right_margin = 150;
    private View rootview;
    TeamListModel teamListModel = new TeamListModel();
    public static Handler mHandler;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
            /*.showImageForEmptyUri(R.drawable.dummy_user_icon)
            .showImageOnFail(fallback)
            .showImageOnLoading(fallback).*/build();
    private TextView address;
    private int team_id = 0;

    public static OnReciveServerResponse onReciveServerResponse;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.pre_find_coach, container, false);
        onReciveServerResponse = this;
        return rootview;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onReciveServerResponse = null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        bundle = getArguments();
        teamListModel = bundle.getParcelable(Tags.createName);
        AppDelegate.LogT("TEamlist=====" + teamListModel + "");
        team_id = teamListModel.team_id;
        tv = (TextView) rootview.findViewById(R.id.text);
        contains = (RelativeLayout) rootview.findViewById(R.id.random_textview);
        rootview.findViewById(R.id.back).setOnClickListener(this);
        execute_Radar();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 2 * 1000);
    }
/*
    private void execute_Radar() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.team_id, team_id);
                mPostAsyncObj = new PostAsync(getActivity(), RadarFragment.this, ServerRequestConstants.RADAR,
                        mPostArrayList, RadarFragment.this);
                // AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }*/

    private void setHandlergeo() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        execute_Radar();
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        address.setText(data.getString(Tags.ADDRESS) + "");
    }

    private void execute_Radar() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.team_id, team_id);
                mPostAsyncObj = new PostAsync(getActivity(), RadarFragment.this, ServerRequestConstants.RADAR,
                        mPostArrayList, RadarFragment.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewList:
                startActivity(intent);
                break;
            case R.id.back:

                break;
        }
    }

    public void backPressed() {
        Bundle bundle = new Bundle();
        bundle.putInt("key", 1);
        bundle.putParcelable(Tags.createName, teamListModel);
        Intent intent = new Intent(getActivity(), CreateTeamActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getFragmentManager().popBackStack();
            }
        }, 200);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (HomeFragment.onReciveServerResponse != null) {
                    HomeFragment.onReciveServerResponse.setOnReciveResult("name", HomeFragment.PANEL_GROUP + "");
                }
            }
        }, 500);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.RADAR)) {
            AppDelegate.save(getActivity(), result, Tags.radarresponse);
            parseaddTeammember(result);
        } else if (apiName.equalsIgnoreCase("test") && result.equalsIgnoreCase(Tags.back)) {
            backPressed();
        }
    }

    private void parseaddTeammember(String result) {
        try {
            ArrayList<RadarModel> radarmodel = new ArrayList<>();
            radarmodel.clear();
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray json = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < json.length(); i++) {
                    JSONObject obj = json.getJSONObject(i);
                    RadarModel radar = new RadarModel();
                    radar.id = obj.getInt(Tags.id);
                    radar.request_id = obj.getInt(Tags.request_id);
                    radar.coach_id = obj.getInt(Tags.coach_id);
                    radar.coach_action = obj.getInt(Tags.coach_action);
                    radar.final_status = obj.getInt(Tags.final_status);
                    radar.followers = obj.getInt(Tags.followers);
                    radar.distance = obj.has(Tags.distance) ? obj.getDouble(Tags.distance) : 0;
                    radar.status = obj.getInt(Tags.status);
                    JSONObject object = obj.getJSONObject("user");
                    UserDataModel userDataModel = new UserDataModel();
                    userDataModel.first_name = object.getString(Tags.first_name);
                    userDataModel.last_name = object.getString(Tags.last_name);
                    userDataModel.email = object.getString(Tags.email);
                    userDataModel.password = object.getString(Tags.password);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.str_Gender = object.getString(Tags.gender);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.dob = object.getString(Tags.birthdate);
                    userDataModel.nickname = object.getString(Tags.nick_name);
                    userDataModel.avtar = object.has(Tags.avtar) ? object.getString(Tags.avtar) : "";
                    userDataModel.avtar_thumb = object.has(Tags.avtar_thumb) ? object.getString(Tags.avtar_thumb) : "http://www.sjafhasfajfh";
                    userDataModel.views = object.getString(Tags.views);
                    userDataModel.follower_count = object.getInt(Tags.follower_count);
                    userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                    userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                    userDataModel.bank_account = object.getString(Tags.bank_account);
                    userDataModel.certificates = object.getString(Tags.certificates);
                    userDataModel.country_id = object.getString(Tags.country_id);
                    userDataModel.role_id = object.getInt(Tags.role_id);
                    userDataModel.state_id = object.getString(Tags.state_id);
                    userDataModel.city_id = object.getString(Tags.city_id);
                    userDataModel.post_code = object.getString(Tags.post_code);
                    userDataModel.sex_group = object.getString(Tags.sex_group);
                    userDataModel.fat_status = object.getString(Tags.fat_status);
                    userDataModel.type = object.getString(Tags.type);
                    userDataModel.token = object.getString(Tags.token);
                    userDataModel.is_login = object.getInt(Tags.is_login);
                    userDataModel.is_social = object.getInt(Tags.is_social);
                    userDataModel.is_verified = object.getInt(Tags.is_verified);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    radar.coach_profile = userDataModel;
                    radarmodel.add(radar);
                }
               /* bundle=new Bundle();
                bundle.putParcelableArrayList(Tags.teamlist,radarmodel);
*/
                contains.removeAllViews();
                intent = new Intent(getActivity(), CoachListActivity.class);
                intent.putParcelableArrayListExtra(Tags.teamlist, radarmodel);
                intent.putExtra(Tags.team_id, team_id);
                rootview.findViewById(R.id.viewList).setOnClickListener(this);
                AppDelegate.LogT("Radar Model " + radarmodel + " itemssss " + radarmodel.size());
                setImg(radarmodel);
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    /*    @Override
        public void onDestroyView() {
            Bundle bundle = new Bundle();
            bundle.putInt("key", 1);
            bundle.putParcelable(Tags.createName, teamListModel);
            Intent intent = new Intent(getActivity(), CreateTeamActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            getFragmentManager().popBackStack();
            super.onDestroyView();
        }
    */
    TextView tv;

    private void setImg(final ArrayList<RadarModel> radarmodel) {
        AppDelegate.LogE("rader loop called => ");
        for (int i = 0; i < radarmodel.size(); i++) {
            AppDelegate.LogE("rader loop called => " + i + ", " + radarmodel.get(i).coach_profile.avtar_thumb);
            final int finalI = i;
            final int finalI1 = i;
            imageLoader.loadImage(radarmodel.get(i).coach_profile.avtar_thumb, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    AppDelegate.LogE("onErrorResponse called");
                    // AppDelegate.LogE(error);
                    View child = getActivity().getLayoutInflater().inflate(R.layout.radar_icons, null);
                    RelativeLayout.LayoutParams params;
                    params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    if (counter % 2 == 0) {
                        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                        params.leftMargin = getRandomPosWidthTop();
                        params.topMargin = getRandomPosHeightTop();
                        counterWidthTop++;
                        counterHeightTop++;
                    } else {
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        params.leftMargin = getRandomPosWidthBottom();
                        params.bottomMargin = getRandomPosHeightBottom();
                        counterWidthBottom++;
                        counterHeightBottom++;
                    }

                    ImageView icon = (ImageView) child.findViewById(R.id.icon);
                    icon.setPadding(2, 2, 2, 2);
                    TextView name = (TextView) child.findViewById(R.id.name);
                    address = (TextView) child.findViewById(R.id.address);
                    name.setText(radarmodel.get(finalI).coach_profile.first_name + "  "
                            + (AppDelegate.isValidString(radarmodel.get(finalI).coach_profile.last_name) ? radarmodel.get(finalI).coach_profile.last_name : ""));
                    setHandlergeo();
                    icon.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), ViewProfileActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt(Parameters.login_id, radarmodel.get(finalI).coach_id);
                            bundle.putInt(Tags.FROM, ViewProfileActivity.FROM_TOGET);
                            intent.putExtra(Tags.team_id, team_id);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                    setloc(radarmodel.get(finalI).coach_profile.latitude, radarmodel.get(finalI).coach_profile.longitude);
                    contains.addView(child, params);
                    AppDelegate.LogT("leftMargin => " + params.leftMargin + ", topMargin => " + params.topMargin);
                    counter++;
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    AppDelegate.LogE("onResponse called");
                    if (loadedImage != null) {
                        View child = getActivity().getLayoutInflater().inflate(R.layout.radar_icons, null);
                        RelativeLayout.LayoutParams params;
                        params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        if (counter % 2 == 0) {
                            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                            params.leftMargin = getRandomPosWidthTop();
                            params.topMargin = getRandomPosHeightTop();
                            counterWidthTop++;
                            counterHeightTop++;
                        } else {
                            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            params.leftMargin = getRandomPosWidthBottom();
                            params.bottomMargin = getRandomPosHeightBottom();
                            counterWidthBottom++;
                            counterHeightBottom++;
                        }

                        ImageView icon = (ImageView) child.findViewById(R.id.icon);
                        icon.setPadding(5, 5, 5, 5);
                        TextView name = (TextView) child.findViewById(R.id.name);
                        // address = (TextView) child.findViewById(R.id.address);
                        icon.setImageBitmap(loadedImage);
                        address = (TextView) child.findViewById(R.id.address);
//                    icon.setImageBitmap(response.getBitmap());
                        name.setText(radarmodel.get(finalI).coach_profile.first_name + "  "
                                + (AppDelegate.isValidString(radarmodel.get(finalI).coach_profile.last_name) ? radarmodel.get(finalI).coach_profile.last_name : ""));
                        setHandlergeo();
                        icon.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getActivity(), ViewProfileActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putInt(Parameters.login_id, radarmodel.get(finalI).coach_id);
                                bundle.putInt(Tags.FROM, ViewProfileActivity.FROM_TOGET);
                                intent.putExtra(Tags.team_id, team_id);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                        setloc(radarmodel.get(finalI).coach_profile.latitude, radarmodel.get(finalI).coach_profile.longitude);
                        contains.addView(child, params);
                        AppDelegate.LogT("leftMargin => " + params.leftMargin + ", topMargin => " + params.topMargin);
                        counter++;
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
        }
        tv.setText(radarmodel.size() + " coaches around you.");
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, getActivity(), mHandler);
    }

    int counter = 0;
    int counterWidthTop = 0, counterHeightTop = 0, counterWidthBottom = 0, counterHeightBottom = 0;

    private int getRandomPosWidthTop() {
        Random r = new Random();
        // This isn't supposed to be an actual range, it's just for testing
        return r.nextInt(200) + 80 + (20 * counterWidthTop);
    }

    private int getRandomPosHeightTop() {
        Random r = new Random();
        // This isn't supposed to be an actual range, it's just for testing
        return r.nextInt(200) + 110 + (20 * counterHeightTop);
    }

    private int getRandomPosWidthBottom() {
        Random r = new Random();
        // This isn't supposed to be an actual range, it's just for testing
        return r.nextInt(200) + 80 + (20 * counterWidthBottom);
    }

    private int getRandomPosHeightBottom() {
        Random r = new Random();
        // This isn't supposed to be an actual range, it's just for testing
        return r.nextInt(200) + 110 + (20 * counterHeightBottom);
    }
}
