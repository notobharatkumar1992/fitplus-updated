package fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.PlayVideoActivity;
import com.fitplus.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.videodemo.VideoRecordingPreLollipopActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.PagerAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.FitDayModel;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class ViewProfileFragment extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {

    private View rootview;
    ImageView display_pic, background_img, back;
    TextView name, address, description, tagline, followers, about_title, about_desc, get, follow;
    ImageView txt_edit_profile, img_upload_video;
    private int login_id;
    private LinearLayout get_follow;
    private ViewPager sponcer;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();
    ImageView img_add1, img_add2, img_add3, img_add4, img_edit1, img_edit2, img_edit3, img_edit4, img_view1, img_view2, img_view3, img_view4;
    LinearLayout lin_view;
    RelativeLayout rel_view;
    private int isfollowed;
    private ImageView edit;
    public static Handler mHandler;
    private Prefs prefs;
    private ImageView img_loading;
    private ScrollView scroll;
    int video_id1 = 0, video_id2 = 0, video_id3 = 0, video_id4 = 0;
    private Bundle bundle;
    ArrayList<FitDayModel> fitDayModelArrayList = new ArrayList<>();
    ImageView img_loading4, img_loading1, img_loading2, img_loading3, video1, video2, video3, video4;
    private TextView txt_watch_videos;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).showImageForEmptyUri(R.drawable.img).
    /* .showImageOnFail(fallback)
     .showImageOnLoading(fallback).*/
            build();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.user_view_profile, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        Bundle bundle = getArguments();
        login_id = bundle.getInt(Parameters.login_id);
        initView();
        setHandler();
        mHandler.sendEmptyMessage(1);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                AppDelegate.LogT("dispatchMessage handler calld");
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    setprofile(new Prefs(getActivity()).getUserdata());
                } else if (msg.what == 2) {
                    setAddressFromGEOcoder(msg.getData());
                } else if (msg.what == 3) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scroll.fullScroll(View.FOCUS_UP);//if you move at the end of the scroll
                            scroll.pageScroll(View.FOCUS_UP);//if you move at the middle of the scroll
                            scroll.smoothScrollTo(0, 0);
                            scroll.fullScroll(ScrollView.FOCUS_UP);
                        }
                    }, 500);
                } else if (msg.what == 5) {
                    execute_getprofile();
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppDelegate.LogT("ViewProfile onDestroyView called");
        mHandler = null;
    }

    private void initView() {
        rootview.findViewById(R.id.txt_c_viewfitday).setOnClickListener(this);
        scroll = (ScrollView) rootview.findViewById(R.id.scroll);
        txt_watch_videos = (TextView) rootview.findViewById(R.id.txt_watch_videos);
        background_img = (ImageView) rootview.findViewById(R.id.user_img);
        display_pic = (ImageView) rootview.findViewById(R.id.display_pic);
        back = (ImageView) rootview.findViewById(R.id.back);
        name = (TextView) rootview.findViewById(R.id.name);
        address = (TextView) rootview.findViewById(R.id.address);
        description = (TextView) rootview.findViewById(R.id.description);
        tagline = (TextView) rootview.findViewById(R.id.Tagline);
        followers = (TextView) rootview.findViewById(R.id.followers);
        about_title = (TextView) rootview.findViewById(R.id.about_title);
        about_desc = (TextView) rootview.findViewById(R.id.about_desc);
        get = (TextView) rootview.findViewById(R.id.get);
        get_follow = (LinearLayout) rootview.findViewById(R.id.get_follow);
        follow = (TextView) rootview.findViewById(R.id.follow);
        txt_edit_profile = (ImageView) rootview.findViewById(R.id.txt_edit_profile);
        txt_edit_profile.setOnClickListener(this);
        img_add1 = (ImageView) rootview.findViewById(R.id.img_add1);
        img_add2 = (ImageView) rootview.findViewById(R.id.img_add2);
        img_add3 = (ImageView) rootview.findViewById(R.id.img_add3);
        img_add4 = (ImageView) rootview.findViewById(R.id.img_add4);
        img_edit1 = (ImageView) rootview.findViewById(R.id.img_edit1);
        img_edit2 = (ImageView) rootview.findViewById(R.id.img_edit2);
        img_edit3 = (ImageView) rootview.findViewById(R.id.img_edit3);
        img_edit4 = (ImageView) rootview.findViewById(R.id.img_edit4);
        img_view1 = (ImageView) rootview.findViewById(R.id.img_view1);
        img_view2 = (ImageView) rootview.findViewById(R.id.img_view2);
        img_view3 = (ImageView) rootview.findViewById(R.id.img_view3);
        img_view4 = (ImageView) rootview.findViewById(R.id.img_view4);
        img_add1.setOnClickListener(this);
        img_add2.setOnClickListener(this);
        img_add3.setOnClickListener(this);
        img_add4.setOnClickListener(this);
        img_edit1.setOnClickListener(this);
        img_edit2.setOnClickListener(this);
        img_edit3.setOnClickListener(this);
        img_edit4.setOnClickListener(this);
        img_view1.setOnClickListener(this);
        img_view2.setOnClickListener(this);
        img_view3.setOnClickListener(this);
        img_view4.setOnClickListener(this);
        lin_view = (LinearLayout) rootview.findViewById(R.id.lin_view);
        rel_view = (RelativeLayout) rootview.findViewById(R.id.rel_view);
        img_loading1 = (ImageView) rootview.findViewById(R.id.img_loading1);
        img_loading2 = (ImageView) rootview.findViewById(R.id.img_loading2);
        img_loading3 = (ImageView) rootview.findViewById(R.id.img_loading3);
        img_loading4 = (ImageView) rootview.findViewById(R.id.img_loading4);
        ViewTreeObserver viewTreeObserver = img_view1.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    img_view1.getLayoutParams().height = img_view1.getWidth();
                    img_view2.getLayoutParams().height = img_view1.getWidth();
                    img_view3.getLayoutParams().height = img_view1.getWidth();
                    img_view4.getLayoutParams().height = img_view1.getWidth();
                    img_view1.invalidate();
                    img_view2.invalidate();
                    img_view3.invalidate();
                    img_view4.invalidate();
//                    viewHeight = img_view1.getHeight();
                    img_view1.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }

        video1 = (ImageView) rootview.findViewById(R.id.video1);
        video2 = (ImageView) rootview.findViewById(R.id.video2);
        video3 = (ImageView) rootview.findViewById(R.id.video3);
        video4 = (ImageView) rootview.findViewById(R.id.video4);

        img_loading = (ImageView) rootview.findViewById(R.id.img_loading);
        if (login_id == new Prefs(getActivity()).getUserdata().userId) {
            get_follow.setVisibility(View.GONE);
        } else {
            get_follow.setVisibility(View.VISIBLE);
        }
        txt_edit_profile.setVisibility(View.GONE);
        follow.setOnClickListener(this);
        get.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    private void show_user_post(int id, String snippet, String avtar) {
        try {
            // Intent intent = new Intent(getActivity(), ViewImageVideoFragment.class);
            new Prefs(getActivity()).putIntValue("post_id", id);
            new Prefs(getActivity()).putStringValue("snippet", snippet);
            new Prefs(getActivity()).putStringValue("avtar", avtar);
            AppDelegate.LogT("clickedsnippet" + id + "checked");
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new ViewImageVideoFragment(), R.id.container_video);
            //startActivity(intent);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        execute_getprofile();
    }

    private void execute_getprofile() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, login_id);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.login_id, new Prefs(getActivity()).getUserdata().userId);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), ViewProfileFragment.this, ServerRequestConstants.GET_PROFILE,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_edit_profile:
                AppDelegate.LogT("edit clicked");
//                startActivity(new Intent(getActivity(), EditProfileActivity.class));
                break;
            case R.id.txt_c_viewfitday:
                show_user_post(login_id, "", "");

                break;
            case R.id.img_upload_video:
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    startActivity(new Intent(getActivity(), VideoRecordingPreLollipopActivity.class));
                } else {
                    startActivity(new Intent(getActivity(), VideoRecordingPreLollipopActivity.class));
                }
                break;

            case R.id.get:
                break;

            case R.id.follow:
                if (follow.isSelected()) {
                    follow.setSelected(false);
                    if (isfollowed == 0) {
                        execute_follow_status("follow");
                    } else {
                        execute_follow_status("unfollow");
                    }
                } else {
                    if (isfollowed == 0) {
                        execute_follow_status("follow");
                    } else {
                        execute_follow_status("unfollow");
                    }
                    follow.setSelected(true);
                }
                break;
            case R.id.back:
                getFragmentManager().popBackStack();
                break;
            case R.id.img_add1:
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, 0);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, 0);
                    startActivity(intent);
                }
                break;
            case R.id.img_add2:
                if (video_id1 == 0) {
                    AppDelegate.showToast(getActivity(), getActivity().getResources().getString(R.string.video_string));
                } else {
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                        Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                        intent.putExtra(Tags.video, 1);
                        intent.putExtra(Tags.video_id, 0);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                        intent.putExtra(Tags.video, 1);
                        intent.putExtra(Tags.video_id, 0);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.img_add3:
                if (video_id1 == 0 || video_id2 == 0) {
                    AppDelegate.showToast(getActivity(), getActivity().getResources().getString(R.string.video_string));
                } else {
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                        Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                        intent.putExtra(Tags.video, 1);
                        intent.putExtra(Tags.video_id, 0);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                        intent.putExtra(Tags.video, 1);
                        intent.putExtra(Tags.video_id, 0);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.img_add4:
                if (video_id1 == 0 || video_id2 == 0 || video_id3 == 0) {
                    AppDelegate.showToast(getActivity(), getActivity().getResources().getString(R.string.video_string));
                } else {
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                        Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                        intent.putExtra(Tags.video, 1);
                        intent.putExtra(Tags.video_id, 0);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                        intent.putExtra(Tags.video, 1);
                        intent.putExtra(Tags.video_id, 0);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.img_edit1:
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id1);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id1);
                    startActivity(intent);
                }
                break;
            case R.id.img_edit2:
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id2);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id2);
                    startActivity(intent);
                }
                break;
            case R.id.img_edit3:
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id3);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id3);
                    startActivity(intent);
                }
                break;
            case R.id.img_edit4:
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id4);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), VideoRecordingPreLollipopActivity.class);
                    intent.putExtra(Tags.video, 1);
                    intent.putExtra(Tags.video_id, video_id4);
                    startActivity(intent);
                }
                break;
            case R.id.img_view1:
                if (video_id1 != 0) {
                    Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                    bundle = new Bundle();
                    bundle.putParcelable("FitDay", fitDayModelArrayList.get(0));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.img_view2:
                if (video_id2 != 0) {
                    Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                    bundle = new Bundle();
                    bundle.putParcelable("FitDay", fitDayModelArrayList.get(1));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.img_view3:
                if (video_id3 != 0) {
                    Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                    bundle = new Bundle();
                    bundle.putParcelable("FitDay", fitDayModelArrayList.get(2));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.img_view4:
                if (video_id4 != 0) {
                    Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                    bundle = new Bundle();
                    bundle.putParcelable("FitDay", fitDayModelArrayList.get(3));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
        }
    }

    private void execute_follow_status(String status) {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, login_id);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.follower_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, status);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), ViewProfileFragment.this, ServerRequestConstants.FOLLOW,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            parsegetprofile(result);
        } else if (apiName.equals(ServerRequestConstants.FOLLOW)) {
            parseFOlLOW(result);
        }
    }

    private void parseFOlLOW(String result) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            AppDelegate.ShowDialogToast(getActivity(), jsonObject.getString(Tags.message), "Alert");
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                isfollowed = jsonObject.getInt("flag");
                if (isfollowed == 0) {
                    follow.setText(getActivity().getResources().getString(R.string.follow));
                } else {
                    if (isfollowed == 1) {
                        follow.setText(getActivity().getResources().getString(R.string.unfollow));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parsegetprofile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject obj = jsonObject.getJSONObject(Tags.response);
                JSONObject object = obj.getJSONObject("user");
                JSONArray fitday = obj.getJSONArray("fitday");
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.about_me = object.getString(Tags.about_me);
                userDataModel.is_followed = object.getInt(Tags.is_followed);
                isfollowed = userDataModel.is_followed;
                if (isfollowed == 0) {
                    follow.setText(getActivity().getResources().getString(R.string.follow));
                } else {
                    if (isfollowed == 1) {
                        follow.setText(getActivity().getResources().getString(R.string.unfollow));
                    }
                }
                setprofile(userDataModel);
                ArrayList<FitDayModel> fitDayModelArrayList = new ArrayList<>();
                for (int i = 0; i < fitday.length(); i++) {
                    JSONObject object1 = fitday.getJSONObject(i);
                    FitDayModel fitDayModel = new FitDayModel();
                    fitDayModel.id = object1.getInt(Tags.user_id);
                    fitDayModel.file_name = object1.getString(Tags.file_name);
                    fitDayModel.file_type = object1.getInt(Tags.file_type);
                    fitDayModel.user_id = object1.getInt(Parameters.user_id);
                    fitDayModel.view = object1.getInt(Tags.view);
                    fitDayModel.comment = object1.getString(Tags.comment);
                    fitDayModel.created = object1.getString(Tags.created);
                    fitDayModel.file_thumb = object1.getString(Tags.file_thumb);
                    fitDayModelArrayList.add(fitDayModel);
                }
                this.fitDayModelArrayList = fitDayModelArrayList;
                settvisibility(fitDayModelArrayList);
                setfitday(fitDayModelArrayList, userDataModel.role_id);
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(getActivity(), jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    private void settvisibility(ArrayList<FitDayModel> fitDayModelArrayList) {
        img_edit1.setVisibility(View.GONE);
        img_edit2.setVisibility(View.GONE);
        img_edit3.setVisibility(View.GONE);
        img_edit4.setVisibility(View.GONE);
        if (fitDayModelArrayList.size() == 1) {
            img_add1.setVisibility(View.GONE);
            img_add2.setVisibility(View.VISIBLE);
            img_add3.setVisibility(View.VISIBLE);
            img_add4.setVisibility(View.VISIBLE);
            img_edit1.setVisibility(View.VISIBLE);
            img_edit2.setVisibility(View.GONE);
            img_edit3.setVisibility(View.GONE);
            img_edit4.setVisibility(View.GONE);
        } else if (fitDayModelArrayList.size() == 2) {
            img_add1.setVisibility(View.GONE);
            img_add2.setVisibility(View.GONE);
            img_add3.setVisibility(View.VISIBLE);
            img_add4.setVisibility(View.VISIBLE);
            img_edit1.setVisibility(View.VISIBLE);
            img_edit2.setVisibility(View.VISIBLE);
            img_edit3.setVisibility(View.GONE);
            img_edit4.setVisibility(View.GONE);
        } else if (fitDayModelArrayList.size() == 3) {
            img_add1.setVisibility(View.GONE);
            img_add2.setVisibility(View.GONE);
            img_add3.setVisibility(View.GONE);
            img_add4.setVisibility(View.VISIBLE);
            img_edit1.setVisibility(View.VISIBLE);
            img_edit2.setVisibility(View.VISIBLE);
            img_edit3.setVisibility(View.VISIBLE);
            img_edit4.setVisibility(View.GONE);
        } else if (fitDayModelArrayList.size() == 4) {
            img_add1.setVisibility(View.GONE);
            img_add2.setVisibility(View.GONE);
            img_add3.setVisibility(View.GONE);
            img_add4.setVisibility(View.GONE);
            img_edit1.setVisibility(View.VISIBLE);
            img_edit2.setVisibility(View.VISIBLE);
            img_edit3.setVisibility(View.VISIBLE);
            img_edit4.setVisibility(View.VISIBLE);
        }
    }

    private void setfitday(ArrayList<FitDayModel> fitDayModelArrayList, int role_id) {
        if (role_id == 2) {
            for (int j = 0; j < fitDayModelArrayList.size(); j++) {
                if (j == 0) {
                    video_id1 = fitDayModelArrayList.get(j).id;
                    img_loading1.setVisibility(View.VISIBLE);
                    video1.setVisibility(View.GONE);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading1.getDrawable();
                            frameAnimation.setCallback(img_loading1);
                            frameAnimation.setVisible(true, true);
                            frameAnimation.start();
                            ((Animatable) img_loading1.getDrawable()).start();
                        }
                    });

                    imageLoader.loadImage(fitDayModelArrayList.get(j).file_thumb, options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                            img_view1.setImageBitmap(bitmap);
                            img_loading1.setVisibility(View.GONE);
                            video1.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });

/*
                    Picasso.with(getActivity()).load(fitDayModelArrayList.get(j).file_thumb)
                            .placeholder(R.drawable.img) // optional
                            .error(R.drawable.img).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            img_view1.setImageBitmap(bitmap);
                            img_loading1.setVisibility(View.GONE);
                            video1.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            AppDelegate.LogT("onBitmapFailed");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            AppDelegate.LogT("onPrepareLoad");
                        }
                    });*/
                } else if (j == 1) {
                    video_id2 = fitDayModelArrayList.get(j).id;
                    img_loading2.setVisibility(View.VISIBLE);
                    video2.setVisibility(View.VISIBLE);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading2.getDrawable();
                            frameAnimation.setCallback(img_loading2);
                            frameAnimation.setVisible(true, true);
                            frameAnimation.start();
                            ((Animatable) img_loading2.getDrawable()).start();
                        }
                    });
                    imageLoader.loadImage(fitDayModelArrayList.get(j).file_thumb, options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                            img_view2.setImageBitmap(bitmap);
                            img_loading2.setVisibility(View.GONE);
                            video2.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });
                 /*   Picasso.with(getActivity()).load(fitDayModelArrayList.get(j).file_thumb)
                            .placeholder(R.drawable.img) // optional
                            .error(R.drawable.img).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            img_view2.setImageBitmap(bitmap);
                            img_loading2.setVisibility(View.GONE);
                            video2.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            AppDelegate.LogT("onBitmapFailed");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            AppDelegate.LogT("onPrepareLoad");
                        }
                    });*/
                } else if (j == 2) {
                    img_loading3.setVisibility(View.VISIBLE);
                    video3.setVisibility(View.GONE);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading3.getDrawable();
                            frameAnimation.setCallback(img_loading3);
                            frameAnimation.setVisible(true, true);
                            frameAnimation.start();
                            ((Animatable) img_loading3.getDrawable()).start();
                        }
                    });
                    video_id3 = fitDayModelArrayList.get(j).id;
                    imageLoader.loadImage(fitDayModelArrayList.get(j).file_thumb, options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                            img_view3.setImageBitmap(bitmap);
                            img_loading3.setVisibility(View.GONE);
                            video3.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });

                    /*
                    Picasso.with(getActivity()).load(fitDayModelArrayList.get(j).file_thumb)
                            .placeholder(R.drawable.img) // optional
                            .error(R.drawable.img).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            img_view3.setImageBitmap(bitmap);
                            img_loading3.setVisibility(View.GONE);
                            video3.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            AppDelegate.LogT("onBitmapFailed");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            AppDelegate.LogT("onPrepareLoad");
                        }
                    });*/
                } else if (j == 3) {
                    video_id4 = fitDayModelArrayList.get(j).id;
                    img_loading4.setVisibility(View.VISIBLE);
                    video4.setVisibility(View.GONE);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading4.getDrawable();
                            frameAnimation.setCallback(img_loading4);
                            frameAnimation.setVisible(true, true);
                            frameAnimation.start();
                            ((Animatable) img_loading4.getDrawable()).start();
                        }
                    });
                    imageLoader.loadImage(fitDayModelArrayList.get(j).file_thumb, options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                            img_view4.setImageBitmap(bitmap);
                            img_loading4.setVisibility(View.GONE);
                            video4.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });
                   /* Picasso.with(getActivity()).load(fitDayModelArrayList.get(j).file_thumb)
                            .placeholder(R.drawable.img) // optional
                            .error(R.drawable.img).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            img_view4.setImageBitmap(bitmap);
                            img_loading4.setVisibility(View.GONE);
                            video4.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            AppDelegate.LogT("onBitmapFailed");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            AppDelegate.LogT("onPrepareLoad");
                        }
                    });*/

                }
            }
        } else {
            for (int i = 0; i < fitDayModelArrayList.size(); i++) {
                Fragment fragment = new VideoFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.FitDayModel, fitDayModelArrayList.get(i));
                fragment.setArguments(bundle);
                bannerFragment.add(fragment);
            }
            AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
            bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
            mHandler.sendEmptyMessage(3);
        }
    }

    /*scroll.fullScroll(View.FOCUS_UP);//if you move at the end of the scroll
    scroll.pageScroll(View.FOCUS_UP);//if you move at the middle of the scroll
    scroll.smoothScrollTo(0, 0);*/
    private void setprofile(UserDataModel userDataModel) {
        if (userDataModel.role_id == 2) {
            txt_watch_videos.setVisibility(View.VISIBLE);
            lin_view.setVisibility(View.VISIBLE);
            rel_view.setVisibility(View.GONE);
            lin_view.getLayoutParams().height = lin_view.getLayoutParams().width;
            AppDelegate.LogT("lin_view.getLayoutParams().height====" + lin_view.getLayoutParams().height + ".........." + "lin_view.getLayoutParams().width" + lin_view.getLayoutParams().width);
        } else {
            txt_watch_videos.setVisibility(View.GONE);
            lin_view.setVisibility(View.GONE);
            rel_view.setVisibility(View.VISIBLE);
        }
        AppDelegate.LogE("setprofile called");
        if (userDataModel.avtar.isEmpty()) {
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    img_loading.setVisibility(View.VISIBLE);
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            imageLoader.loadImage(userDataModel.avtar, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    display_pic.setImageBitmap(bitmap);
                    try {
                        background_img.setImageBitmap(AppDelegate.blurRenderScript(getActivity(), bitmap));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            img_loading.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
          /*  Picasso.with(getActivity()).load(userDataModel.avtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    display_pic.setImageBitmap(bitmap);
                    try {
                        background_img.setImageBitmap(AppDelegate.blurRenderScript(getActivity(), bitmap));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            img_loading.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("onPrepareLoad");
                }
            });*/
        }
        name.setText((AppDelegate.isValidString(userDataModel.first_name) ? userDataModel.first_name : "") + " " + (AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name : ""));
        about_title.setText("About " + userDataModel.first_name + ":");
        about_desc.setText(userDataModel.about_me + "");
        followers.setText(String.valueOf(userDataModel.follower_count) + "  " + getActivity().getResources().getString(R.string.followers));
        setloc(Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, "")), Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, "")));
        tagline.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + ", " + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "") + ", " + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));
       /* if (userDataModel.role_id == 2) {
            img_upload_video.setVisibility(View.VISIBLE);
        } else {
            img_upload_video.setVisibility(View.GONE);
        }*/

    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, getActivity(), mHandler);
    }

    private void setAddressFromGEOcoder(Bundle data) {
        address.setText(data.getString(Tags.country_param) + "");
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }
}

