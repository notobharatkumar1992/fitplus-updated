package fragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.fitplus.R;

import java.util.ArrayList;

import adapters.MonthsAdapter;
import adapters.Recycler_Stats_Adapter;
import interfaces.OnListItemClickListener;
import model.StatMonthsModel;

/**
 * Created by admin on 26-07-2016.
 */
public class StatsFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, OnListItemClickListener {

    private RecyclerView months, recycler_stats;
    private ArrayList<StatMonthsModel> monthArray = new ArrayList<>();
    private MonthsAdapter adapter;
    Handler mHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stats, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 1) {
                    adapter.notifyDataSetChanged();
                    months.invalidate();
                }
            }
        };
    }

    private void initView(View rootview) {
        months = (RecyclerView) rootview.findViewById(R.id.months);
        recycler_stats = (RecyclerView) rootview.findViewById(R.id.recycler_stats);
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.jan)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.feb)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.mar)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.apr)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.may)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.jun)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.jul)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.aug)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.sep)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.oct)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.nov)));
        monthArray.add(new StatMonthsModel(getResources().getString(R.string.dec)));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setmonth();
        set_recycler_stats();
    }

    private void set_recycler_stats() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_stats.setLayoutManager(layoutManager);
        Recycler_Stats_Adapter adapter = new Recycler_Stats_Adapter(getActivity());
        recycler_stats.setAdapter(adapter);
    }

    private void setmonth() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        months.setLayoutManager(layoutManager);
        adapter = new MonthsAdapter(getActivity(), monthArray, this);
        months.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase("state")) {
            monthArray.get(position).isSelected = true;
            for (int i = 0; i < monthArray.size(); i++) {
                if (i != position) {
                    monthArray.get(i).isSelected = false;
                }
            }
            mHandler.sendEmptyMessage(1);
        }
    }
}
