package fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.util.Property;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.github.lzyzsd.circleprogress.DonutProgress;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.PagerAdapter;
import butterknife.ButterKnife;
import interfaces.GetDurationClickListener;
import interfaces.OnDialogClickListener;
import interfaces.OnListItemClickListener;
import interfaces.OnReciveServerResponse;
import model.FitDayModel;
import model.PostAysnc_Model;
import utils.NonSwipeableViewPager;
import utils.Prefs;
import utils.codetail.animation.AnimatorPath;
import utils.codetail.animation.PathEvaluator;
import utils.codetail.animation.PathPoint;
import utils.codetail.animation.ViewAnimationUtils;

/**
 * Created by admin on 26-07-2016.
 */
public class ViewImageVideoFragment extends Fragment implements OnClickListener, OnReciveServerResponse, OnDialogClickListener, OnListItemClickListener, GetDurationClickListener {

    public static Handler mHandler;

    ImageView back;
    NonSwipeableViewPager video_list;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();
    GestureDetector gestureScanner;
    // LinearLayout pager_indicator;
    int team_id;
    private ArrayList<FitDayModel> trending_list = new ArrayList<>();
    Bundle bundle;
    private Dialog builder;
    int fitday_id = 0;
    //    int position = 0;
    private Prefs prefs;
    private int id;
    private String name;
    private String img;
    Handler handler = new Handler();
    public static OnListItemClickListener onListItemClickListener;
    DonutProgress donut_progress;
    public static GetDurationClickListener getDurationClickListener;
    private int duration;
    //  TextView txt_position;
    private boolean visibility = false;
    final static int SLOW_DURATION = 400;
    final static int FAST_DURATION = 200;
    private LinearLayout scroll;
    private float maskElevation;
    CardView activatorMask;
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        rootview = inflater.inflate(R.layout.view_imge_video, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AppDelegate.disablePanel = true;
        onListItemClickListener = this;
        getDurationClickListener = this;
        //  txt_position = (TextView) rootview.findViewById(R.id.position);
        prefs = new Prefs(getActivity());
        id = new Prefs(getActivity()).getIntValue("post_id", 0);
        name = new Prefs(getActivity()).getStringValue("snippet", "");
        img = new Prefs(getActivity()).getStringValue("avtar", "");
        AppDelegate.LogT("clickedsnippetvdfgdf " + new Prefs(getActivity()).getStringValue("avtar", "") + "checked");
        setHandler();
        initView();
        mHandler.sendEmptyMessage(3);
    }

    public void resetUi() {
        // cardsLine.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            final View target = ButterKnife.findById(getActivity(), R.id.activator);
            final ViewGroup parent = (ViewGroup) video_list.getParent();
            final Rect bounds = new Rect();
            final Rect maskBounds = new Rect();
            target.getDrawingRect(bounds);
            video_list.getDrawingRect(maskBounds);
            parent.offsetDescendantRectToMyCoords(target, bounds);
            parent.offsetDescendantRectToMyCoords(video_list, maskBounds);
            maskElevation = 5;
            // video_list.setElevation(0);
            final int cX = maskBounds.centerX();
            final int cY = maskBounds.centerY();

            final Animator circularReveal = ViewAnimationUtils.createCircularReveal(video_list, cX, cY,
                    (float) Math.hypot(maskBounds.width() * .5f, maskBounds.height() * .5f),
                    target.getWidth() / 2f, View.LAYER_TYPE_HARDWARE);

            final float c0X = bounds.centerX() - maskBounds.centerX();
            final float c0Y = bounds.centerY() - maskBounds.centerY();

            AnimatorPath path = new AnimatorPath();
            path.moveTo(0, 0);
            path.curveTo(0, 0, 0, c0Y, c0X, c0Y);

            ObjectAnimator pathAnimator = ObjectAnimator.ofObject(this, "maskLocation", new PathEvaluator(),
                    path.getPoints().toArray());

            AnimatorSet set = new AnimatorSet();
            set.playTogether(circularReveal, pathAnimator);
            set.setInterpolator(new FastOutSlowInInterpolator());
            set.setDuration(SLOW_DURATION);
            set.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    //  video_list.setElevation(maskElevation);
                    video_list.setVisibility(View.INVISIBLE);
                    //circlesLine.setVisibility(View.VISIBLE);
                    // executeCirclesDropDown();
                    target.setEnabled(true);
                    if (isAdded())
                        getFragmentManager().popBackStack();
                }
            });
            set.start();
        }
    }

    /*  private void executeCirclesDropDown() {
          final int length = activatorMask.getChildCount();
          Animator[] animators = new Animator[length];
          for (int i = 0; i < length; i++) {
              View target = activatorMask.getChildAt(i);
              final float x0 = -10 * i;
              final float y0 = -10 * i;

              target.setTranslationX(x0);
              target.setTranslationY(y0);

              AnimatorPath path = new AnimatorPath();
              path.moveTo(x0, y0);
              path.curveTo(x0, y0, 0, y0, 0, 0);

              PathPoint[] points = new PathPoint[path.getPoints().size()];
              path.getPoints().toArray(points);

              AnimatorSet set = new AnimatorSet();
              set.play(ObjectAnimator.ofObject(target, PATH_POINT, new PathEvaluator(), points))
                      .with(ObjectAnimator.ofFloat(target, View.ALPHA, (length - i) * 0.1f + 0.6f, 1f));

              animators[i] = set;
              animators[i].setStartDelay(15 * i);
          }

          AnimatorSet set = new AnimatorSet();
          set.playTogether(animators);
          set.setInterpolator(new FastOutSlowInInterpolator());
          set.setDuration(FAST_DURATION);
          set.start();
      }*/
    void activateAwareMotion(final View target) {
        // Cancel all concurrent events on view
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            target.cancelPendingInputEvents();
        }
        target.setEnabled(false);

        // Coordinates of circle initial point
        final ViewGroup parent = (ViewGroup) target.getParent();
        final Rect bounds = new Rect();
        final Rect maskBounds = new Rect();

        target.getDrawingRect(bounds);
        target.getDrawingRect(maskBounds);
        parent.offsetDescendantRectToMyCoords(target, bounds);
        parent.offsetDescendantRectToMyCoords(target, maskBounds);

        // Put Mask view at circle initial points
        //   maskElevation = target.getCardElevation();
        //activatorMask.setCardElevation(0);
        target.setVisibility(View.VISIBLE);
        target.setX(bounds.left - maskBounds.centerX());
        target.setY(bounds.top - maskBounds.centerY());

        //  circlesLine.setVisibility(View.INVISIBLE);

        final int cX = maskBounds.centerX();
        final int cY = maskBounds.centerY();

        Animator circularReveal =
                ViewAnimationUtils.createCircularReveal(target, cX, cY, target.getWidth() / 2,
                        (float) Math.hypot(maskBounds.width() * .5f, maskBounds.height() * .5f),
                        View.LAYER_TYPE_HARDWARE);

        final float c0X = bounds.centerX() - maskBounds.centerX();
        final float c0Y = bounds.centerY() - maskBounds.centerY();

        AnimatorPath path = new AnimatorPath();
        path.moveTo(c0X, c0Y);
        path.curveTo(c0X, c0Y, 0, c0Y, 0, 0);

        ObjectAnimator pathAnimator = ObjectAnimator.ofObject(this, "maskLocation", new PathEvaluator(),
                path.getPoints().toArray());

        AnimatorSet set = new AnimatorSet();
        set.playTogether(circularReveal, pathAnimator);
        set.setInterpolator(new FastOutSlowInInterpolator());
        set.setDuration(SLOW_DURATION);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                executeCardsSequentialAnimation();

            }
        });
        set.start();
    }

    @Override
    public void onDestroyView() {
        AppDelegate.disablePanel = false;
        super.onDestroyView();
    }

    private void executeCardsSequentialAnimation() {
        final int length = scroll.getChildCount();
        scroll.setVisibility(View.VISIBLE);

        final Animator[] animators = new Animator[length];
        for (int i = 0; i < length; i++) {
            View target = scroll.getChildAt(i);
            final float x0 = 0;// i == 0 ? 0 : -10 * (1 + i * 0.2f);
            final float y0 = 10 * i;
            target.setTranslationX(x0);
            target.setTranslationY(y0);
            AnimatorPath path = new AnimatorPath();
            path.moveTo(x0, y0);
            path.lineTo(0, 0);
            PathPoint[] points = new PathPoint[path.getPoints().size()];
            path.getPoints().toArray(points);
            AnimatorSet set = new AnimatorSet();
            set.play(ObjectAnimator.ofObject(target, PATH_POINT, new PathEvaluator(), points))
                    .with(ObjectAnimator.ofFloat(target, View.ALPHA, 0.3f, 1f));
            animators[i] = set;
            animators[i].setStartDelay(15 * i);
        }

        final AnimatorSet sequential = new AnimatorSet();
        sequential.playTogether(animators);
        sequential.setInterpolator(new FastOutLinearInInterpolator());
        sequential.setDuration(SLOW_DURATION);
        sequential.start();
        if (isAdded())
            getFragmentManager().popBackStack();
    }

    private final static Property<View, PathPoint> PATH_POINT =
            new Property<View, PathPoint>(PathPoint.class, "PATH_POINT") {
                PathPoint point;

                @Override
                public PathPoint get(View object) {
                    return point;
                }

                @Override
                public void set(View object, PathPoint value) {
                    point = value;

                    object.setTranslationX(value.mX);
                    object.setTranslationY(value.mY);
                }
            };

    private void startProgress(final int time) {
        AppDelegate.LogT("timeis " + time);
        donut_progress.setMax(time * 1000);
        donut_progress.setUnfinishedStrokeColor(Color.RED);
        donut_progress.setFinishedStrokeColor(Color.TRANSPARENT);
        countDownTimer = new CountDownTimer(time * 1000, 1) {
            @Override
            public void onTick(long millisUntilFinished) {
                donut_progress.setProgress((int) millisUntilFinished);
            }

            @Override
            public void onFinish() {

                donut_progress.setProgress(time * 1000);
            }
        };
        countDownTimer.start();
    }

    private void stopProgress() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    private void execute_trendingList() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.login_id, new Prefs(getActivity()).getUserdata().userId, ServerRequestConstants.Key_PostintValue);
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.VIEW_FITDAY,
                        mPostArrayList, ViewImageVideoFragment.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void parseTrendingList(String result) {
        donut_progress.setVisibility(View.VISIBLE);
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                ArrayList<FitDayModel> trending = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    FitDayModel userDataModel = new FitDayModel();
                    JSONObject object = jsonArray.getJSONObject(i);
                    userDataModel.id = object.getInt(Tags.user_id);
                    userDataModel.file_name = object.getString(Tags.file_name);
                    userDataModel.file_type = object.getInt(Tags.file_type);
                    userDataModel.user_id = object.getInt(Parameters.user_id);
                    userDataModel.view = object.getInt(Tags.view);
                    userDataModel.comment = object.getString(Tags.comment);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.file_thumb = object.getString(Tags.file_thumb);
                    userDataModel.viewstatus = object.getInt(Tags.viewstatus);
                    userDataModel.time = object.getString(Tags.time);
                    if (!AppDelegate.isValidString(userDataModel.time) || userDataModel.time.equalsIgnoreCase("(null)")) {
                        userDataModel.time = "3";
                    }
                    trending.add(userDataModel);
                }
                trending_list = trending;


                if (trending_list.size() > 0) {
                    mHandler.sendEmptyMessage(0);
                    mHandler.sendEmptyMessage(2);
                    setTrendingList(trending);
                } else {
                    AppDelegate.showToast(getActivity(), "No fit day available.");
                    resetUi();
                    //getActivity().getFragmentManager().popBackStack();
                }
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, FitDayActivity.this);
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, FitDayActivity.this);
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }

    private void setTrendingList(ArrayList<FitDayModel> trending) {
        if (trending != null) {
            setfitday(trending);
            video_list.setCurrentItem(0);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        //  setUiPageViewController();
                        break;
                    case 2:
                        bannerPagerAdapter.notifyDataSetChanged();
                        video_list.invalidate();
                        break;
                    case 3:
                        execute_trendingList();
                        break;
                }
            }
        };
    }

    private void initView() {
        // scroll = (LinearLayout) findViewById(R.id.scroll);
       /* findViewById(R.id.txt_report_abuse).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);*/
      /*  TextView title = (TextView) findViewById(R.id.title);
        title.setText(name + "");*/
        trending_list = new ArrayList<>();
        //pager_indicator = (LinearLayout) findViewById(R.id.pager_indicator);
        video_list = (NonSwipeableViewPager) rootview.findViewById(R.id.video_list);
        video_list.setOffscreenPageLimit(1);
        donut_progress = (DonutProgress) rootview.findViewById(R.id.donut_progress);
        donut_progress.setVisibility(View.INVISIBLE);
    }

    /*  private ImageView[] dots;
      private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
  */
    private void setUiPageViewController() {
        fitday_id = trending_list.get(0).id;
        if (trending_list.get(0).viewstatus == 1) {
            execute_viewStatus();
            trending_list.get(0).viewstatus = 0;
        }
        // pager_indicator.removeAllViews();
        setValues(trending_list, 0);
        video_list.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                AppDelegate.LogT("onPageSelected time == " + trending_list.get(position).time + ", type = " + trending_list.get(position).file_type);
                // txt_position.setText("Pos = " + String.valueOf(position) + ", type = " + trending_list.get(position).file_type);
                fitday_id = trending_list.get(position).id;
                setValues(trending_list, position);
                if (trending_list.get(position).viewstatus == 1) {
                    execute_viewStatus();
                    trending_list.get(position).viewstatus = 0;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        // switchBannerPage(0);
    }

    CountDownTimer countDownTimer, countDownTimerDonut;

    public void candelCountDownTimer() {
        AppDelegate.LogT("candelCountDownTimer called");
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    private void setValues(final ArrayList<FitDayModel> trending_list, final int position) {
        candelCountDownTimer();
//        stopProgress();
        if (trending_list.get(position).file_type == 1) {
            AppDelegate.LogT("OnPageChange Image => " + position);
            showImageTimer();
        } else if (trending_list.get(position).file_type == 2) {
            AppDelegate.LogT("OnPageChange Video => " + position + ", play_status = " + (AppDelegate.isValidString(trending_list.get(position).play_status) && trending_list.get(position).play_status.equalsIgnoreCase(PLAY)));
            if (AppDelegate.isValidString(trending_list.get(position).play_status) && trending_list.get(position).play_status.equalsIgnoreCase(PLAY)) {
                ((VideoZoomableImageFragment) bannerFragment.get(position)).playVideo(trending_list.get(position).file_name);
                set_timer_onvideo(video_list.getCurrentItem());
            } else {
            }
        }
    }

    private void showImageTimer() {
        donut_progress.setVisibility(View.VISIBLE);
        final int time = Integer.parseInt(trending_list.get(video_list.getCurrentItem()).time);
        try {
            donut_progress.setMax(time * 1000);
            donut_progress.setUnfinishedStrokeColor(Color.WHITE);
            donut_progress.setFinishedStrokeColor(R.color.blackish);
            countDownTimer = new CountDownTimer(time * 1000, 1) {
                @Override
                public void onTick(long millisUntilFinished) {
                    donut_progress.setProgress((int) millisUntilFinished);
                }

                @Override
                public void onFinish() {
                    donut_progress.setProgress((int) time * 1000);
                    if (trending_list.size() - 1 == video_list.getCurrentItem()) {
                        AppDelegate.LogT("CountDownTimer time is == " + trending_list.get(video_list.getCurrentItem()).time);
                        resetUi();
                    } else {
                        AppDelegate.LogT("CountDownTimer time is == " + trending_list.get(video_list.getCurrentItem()).time);
                        video_list.setCurrentItem(video_list.getCurrentItem() + 1);
                    }
                }
            };
            countDownTimer.start();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void set_timer_onvideo(final int currentItem) {
        final int time = Integer.parseInt(trending_list.get(currentItem).time);
        // AppDelegate.LogT("Time is ==" + time);
        donut_progress.setVisibility(View.INVISIBLE);
        try {
            // AppDelegate.LogT("duration with position in timer==" + duration + " position==" + currentItem);
            donut_progress.setMax(time * 1000);
            donut_progress.setUnfinishedStrokeColor(R.color.blackish);
            donut_progress.setFinishedStrokeColor(Color.WHITE);
            countDownTimer = new CountDownTimer(time * 1000, 1) {
                @Override
                public void onTick(long millisUntilFinished) {
                    donut_progress.setProgress((int) millisUntilFinished);
                }

                @Override
                public void onFinish() {
                    donut_progress.setProgress((int) time * 1000);
                   /* if (trending_list.size() - 1 == currentItem) {
                        AppDelegate.LogT("trending_list.size()==" + trending_list.size() + "position-1====" + (currentItem) + "");
                        AppDelegate.LogT("time is==" + trending_list.get(currentItem).time);
                        finish();
                    } else {
                        AppDelegate.LogT("trending_list.size()==" + trending_list.size() + "position====" + (currentItem) + "");
                        AppDelegate.LogT("time is==" + trending_list.get(currentItem).time);
                        video_list.setCurrentItem(currentItem + 1);
                    }*/
                }
            };
            countDownTimer.start();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.txt_report_abuse:
                show_dialog();
                break;

            case R.id.back:
                finish();
                break;*/
        }
    }

    private void show_dialog() {
        if (builder != null && builder.isShowing()) {
            builder.dismiss();
        }
        builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.report_abuse);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok, cancel;
        final EditText et_comment;
        builder.setCancelable(false);
        cancel = (TextView) builder.findViewById(R.id.no);
        ok = (TextView) builder.findViewById(R.id.yes);
        et_comment = (EditText) builder.findViewById(R.id.et_comment);
        builder.show();
        // builder.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppDelegate.isValidString(et_comment.getText().toString())) {
                    execute_Report_abuse(et_comment.getText().toString());
                    builder.dismiss();
                } else {
                    AppDelegate.showToast(getActivity(), getResources().getString(R.string.abuse_validation));
                }
            }
        });
    }

    private void execute_viewStatus() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.fitday_id, fitday_id);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), ViewImageVideoFragment.this, ServerRequestConstants.FITDAYCOUNT,
                        mPostArrayList, ViewImageVideoFragment.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), getResources().getString(R.string.try_again), "Alert!!!");
        }
    }

    private void execute_Report_abuse(String comment) {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.fitday_id, fitday_id);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_comments, comment);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), ViewImageVideoFragment.this, ServerRequestConstants.ABUSE_REPORT,
                        mPostArrayList, ViewImageVideoFragment.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), getResources().getString(R.string.try_again), "Alert!!!");
        }

    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), ViewImageVideoFragment.this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.ABUSE_REPORT)) {
            parse_ABUSE_REPORT(result);
        } else if (apiName.equals(ServerRequestConstants.VIEW_FITDAY)) {
            parseTrendingList(result);
        } else if (apiName.equals(ServerRequestConstants.FITDAYCOUNT)) {
            /*if(FitDayActivity.mHandler!=null){
                FitDayActivity.mHandler.sendEmptyMessage(3);
            }*/
        }
    }

    private void parse_ABUSE_REPORT(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    private void setfitday(ArrayList<FitDayModel> fitDayModelArrayList) {
        for (int i = 0; i < fitDayModelArrayList.size(); i++) {
            Fragment fragment = new VideoZoomableImageFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", i);
            bundle.putParcelable(Tags.FitDayModel, fitDayModelArrayList.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
        bannerPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), bannerFragment);
        video_list.setAdapter(bannerPagerAdapter);
        setUiPageViewController();
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }

    public static final String PLAY = "PLAY", STOP = "STOP";

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase("stopped")) {
            AppDelegate.LogT("******stopped => " + position);
            trending_list.get(position).play_status = STOP;
            if (trending_list.size() - 1 == position) {
                resetUi();
            } else {
                video_list.setCurrentItem(position + 1);
            }
        } else if (name.equalsIgnoreCase("playing")) {
            trending_list.get(position).play_status = PLAY;

            if (position == video_list.getCurrentItem()) {
                AppDelegate.LogT("******playing => " + position);
                set_timer_onvideo(video_list.getCurrentItem());
            }

        } else if (name.equalsIgnoreCase("clicked")) {
            AppDelegate.LogT("******clicked => " + position);
            if (trending_list.size() - 1 == position) {
                resetUi();
            } else {
                video_list.setCurrentItem(position + 1);
            }
        } else if (name.equalsIgnoreCase("visibility")) {
            AppDelegate.LogT("******visibility====" + position);
            if (position == 1) {
                visibility = true;
            } else {
                visibility = false;
            }
        } else if (name.equalsIgnoreCase("onbackpressed")) {
            AppDelegate.LogT("onbackpressed method called");
            resetUi();
        } else if (name.equalsIgnoreCase("backfinish")) {
            AppDelegate.LogT("backfinish method called");
            resetUi();
        }
    }

    @Override
    public void setonGetDurationClickListener(String name, int position, int duration) {
        AppDelegate.LogT("setonGetDurationClickListener => duration with position in on click==" + duration + " position==" + position + ", name = " + name);
        this.duration = duration;
        if (name.equalsIgnoreCase("video")) {
        } else if (name.equalsIgnoreCase("click")) {
            AppDelegate.LogT("click+ position");
            if (trending_list.size() - 1 == position) {
                resetUi();
            } else {
                video_list.setCurrentItem(position + 1);
            }
        }
    }


}

