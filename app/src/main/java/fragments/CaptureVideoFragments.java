package fragments;

import android.content.res.Configuration;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.fitplus.AppDelegate;
import com.fitplus.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class CaptureVideoFragments extends Fragment implements View.OnClickListener, View.OnLongClickListener, SurfaceHolder.Callback {

    private Button btnStartRec;
    MediaRecorder recorder;
    SurfaceHolder holder;
    boolean recording = false;
    private int randomNum;
    private Camera mCamera;

    public static int LONG_PRESS_TIME = 500;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view001 = inflater.inflate(R.layout.camera, container, false);
        recorder = new MediaRecorder();

        btnStartRec = (Button) view001.findViewById(R.id.capture);
        btnStartRec.setOnClickListener(this);
        SurfaceView cameraView = (SurfaceView) view001.findViewById(R.id.surfaceCamera);
        holder = cameraView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        cameraView.setClickable(true);
        cameraView.setOnClickListener(this);

        return view001;
    }

    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (Exception e) {
            // cannot get camera or does not exist
        }
        return camera;
    }

    Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if (pictureFile == null) {
                return;
            }
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {

            } catch (IOException e) {
            }
        }
    };

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    /* @SuppressLint({ "SdCardPath", "NewApi" })*/
    private void initRecorder() {

        Random rn = new Random();
        int maximum = 10000000;
        int minimum = 00000001;
        int range = maximum - minimum + 1;
        randomNum = rn.nextInt(range) + minimum + 1 - 10;

        recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        AppDelegate.LogT("setVideoSource start");
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);

        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            recorder.setOrientationHint(90);//plays the video correctly
        } else {
            recorder.setOrientationHint(180);
        }
        recorder.setOutputFile("/sdcard/MediaAppVideos/" + randomNum + ".mp4");
        AppDelegate.LogT("recording finish");
    }

    private void prepareRecorder() {
        recorder.setPreviewDisplay(holder.getSurface());
        try {
            recorder.prepare();
            AppDelegate.LogT("recording prepare");
        } catch (IllegalStateException e) {
            AppDelegate.LogE(e);
            //finish();
        } catch (IOException e) {
            AppDelegate.LogE(e);
            //finish();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.capture:
                // mCamera.takePicture(null, null, mPicture);
                try {
                    if (recording) {
                        recorder.stop();
                        recording = false;
                    } else {
                        initRecorder();
                        recording = true;
                        recorder.start();
                        AppDelegate.LogT("recording start");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recorder.stop();
                                AppDelegate.LogT("recording stop");
                            }
                        }, 10000);
                    }

                } catch (Exception e) {

                }
                break;

            default:
                break;
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        prepareRecorder();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        try {
            if (recording) {
                recorder.stop();
                recording = false;
            }
            recorder.release();
            // finish();
        } catch (Exception e) {

        }

    }


    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.capture:
                AppDelegate.LogT("recording new start");
                try {
                    if (recording) {
                        recorder.stop();
                        recording = false;
                    } else {
                        initRecorder();
                        recording = true;
                        recorder.start();
                        AppDelegate.LogT("recording start");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recorder.stop();
                                AppDelegate.LogT("recording stop");
                            }
                        }, 10000);
                    }

                } catch (Exception e) {

                }
                break;
            default:
                break;

        }
        return true;
    }


}