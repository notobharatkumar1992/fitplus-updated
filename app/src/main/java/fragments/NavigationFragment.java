package fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.FitDayActivity;
import com.fitplus.MainActivity;
import com.fitplus.R;
import com.fitplus.TrendingActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

//import com.camerademo.CameraActivity;

public class NavigationFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener, LocationListener {
    public static final int USER = 1, COACH = 2;
    //    private NonSwipeableViewPager mViewPager;
//    public FragmentManager fragmentManager;
//    private SectionsPagerAdapter mSectionsPagerAdapter;
    ImageView img_news_indicator, img_globe_indicator;
    public static final int PANEL_GlOBE = 0, PANEL_NEWS = 1;

    View rootview;
    private MapView mapview;
    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mMap;
    private Double latitude, longitude;
    private GoogleMap map_business;
    private SupportMapFragment fragment;
    private Bitmap bitmap;
    private Drawable img;
    private Marker customMarker;
    HashMap<String, Integer> mMarkers = new HashMap<String, Integer>();
    private RelativeLayout toolbar;
    private Location center;
    LatLng previous, current;
    private double difference;
    private LocationManager locationManager;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    ArrayList<UserDataModel> trendingGlobal = new ArrayList<>();
    ArrayList<UserDataModel> trendingFollowers = new ArrayList<>();

    Handler mHandler;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).showImageForEmptyUri(R.drawable.female).
    /* .showImageOnFail(fallback)
     .showImageOnLoading(fallback).*/
            build();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.navigation_fragment, container, false);
//        latitude = 26.78;
//        longitude = 72.56;
        initView();
        setHandler();
        return rootview;
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 1) {
                    try {
                        if (selected_tab == 0) {
                            setIcons(trendingGlobal);
                        } else {
                            setIcons(trendingFollowers);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.map_Frame, fragment, "MAP1").addToBackStack(null)
                .commit();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showMap();
            }
        }, 1500);
    }

    boolean apiCalled = false;

    private void initView() {
        rootview.findViewById(R.id.menu).setOnClickListener(this);
        rootview.findViewById(R.id.rl_globe).setOnClickListener(this);
        rootview.findViewById(R.id.rl_news).setOnClickListener(this);
        img_news_indicator = (ImageView) rootview.findViewById(R.id.img_news_indicator);
        img_globe_indicator = (ImageView) rootview.findViewById(R.id.img_globe_indicator);
        rootview.findViewById(R.id.trending_link).setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupTabLayout();
    }

    private void setupTabLayout() {
//        mViewPager = (NonSwipeableViewPager) rootview.findViewById(R.id.sub_container);
//        mViewPager.setAdapter(mSectionsPagerAdapter);
//        fragmentManager = getActivity().getSupportFragmentManager();
//        mViewPager.setCurrentItem(0);
        setInitailToolBar(0);
    }

    public void setInitailToolBar(int value) {
        img_news_indicator.setSelected(false);
        img_globe_indicator.setSelected(false);
        switch (value) {
            case 0:
                img_globe_indicator.setSelected(true);
                break;
            case 1:
                img_news_indicator.setSelected(true);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.trending_link:
                Intent intent = new Intent(getActivity(), TrendingActivity.class);
                startActivity(intent);
                break;
            case R.id.menu:
                AppDelegate.LogT("menu clicked");
                ((MainActivity) getActivity()).toggleSlider();
                break;
            case R.id.rl_globe:
                selected_tab = 0;
                setInitailToolBar(0);
//                setIcons(trendingGlobal);
                mHandler.sendEmptyMessage(1);
//                mSectionsPagerAdapter.getItem(PANEL_GlOBE);
                break;
            case R.id.rl_news:
                selected_tab = 1;
                setInitailToolBar(1);
//                setIcons(trendingFollowers);
                mHandler.sendEmptyMessage(1);
//                mSectionsPagerAdapter.getItem(PANEL_NEWS);
                break;
        }
    }

    int selected_tab = 0;

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    AppDelegate.LogT("position clicked" + position + "");
                    return new GlobalMapFragments();
                case 1:
                    return new ListedMapFragments();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    private void execute_getNearby() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
               /* AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""));*/
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, center.getLatitude());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, center.getLongitude());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), NavigationFragment.this, ServerRequestConstants.GET_NEARBY_FITDAYS,
                        mPostArrayList, null);
                //  AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
                apiCalled = true;
                previous = new LatLng(center.getLatitude(), center.getLongitude());
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void showMap() {
        map_business = fragment.getMap();
        if (map_business == null) {
            return;
        }
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map_business.animateCamera(CameraUpdateFactory.zoomIn());
        if (AppDelegate.isValidString(new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, "")) && AppDelegate.isValidString(new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""))) {
            LatLng latLng = new LatLng(Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, "")), Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, "")));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 4.5f);
            map_business.animateCamera(cameraUpdate);
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        center = new Location("center");
        previous = new LatLng(center.getLatitude(), center.getLongitude());
        map_business.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(final CameraPosition cameraPosition) {
                center.setLatitude(cameraPosition.target.latitude);
                center.setLongitude(cameraPosition.target.longitude);
                current = new LatLng(center.getLatitude(), center.getLongitude());
                difference = distance(previous.latitude, previous.longitude, current.latitude, current.longitude);
                AppDelegate.LogT("difference current =>" + difference);
                if (difference >= 3.0) {
                    AppDelegate.LogT("difference =>" + difference);
                    execute_getNearby();
                }
            }
        });
        try {
            View locationButton = ((View) fragment.getView().findViewById(0x1).getParent()).findViewById(0x2);
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void stopStartThread(Thread theThread, final CameraPosition cameraPosition) {
        if (theThread != null)
            onlyStopThread(theThread);
        theThread = new Thread(new Runnable() {
            @Override
            public void run() {
                center.setLatitude(cameraPosition.target.latitude);
                center.setLongitude(cameraPosition.target.longitude);
                execute_getNearby();
            }
        });
        theThread.start();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_NEARBY_FITDAYS)) {
            apiCalled = false;
            parseNEarBy(result);
        }
    }

    private void parseNEarBy(String result) {
        try {
            trendingGlobal.clear();
            trendingFollowers.clear();
            JSONObject jsonObject = new JSONObject(result);
            new Prefs(getActivity()).setInviteFriendsTest(jsonObject.getString(Tags.invite_friends));
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject response = jsonObject.getJSONObject(Tags.response);
                JSONArray follower = response.getJSONArray(Tags.follower);
                for (int i = 0; i < follower.length(); i++) {
                    UserDataModel userDataModel = new UserDataModel();
                    JSONObject object = follower.getJSONObject(i);
                    userDataModel.first_name = object.getString(Tags.first_name);
                    userDataModel.last_name = object.getString(Tags.last_name);
                    userDataModel.email = object.getString(Tags.email);
                    userDataModel.password = object.getString(Tags.password);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.str_Gender = object.getString(Tags.gender);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.dob = object.getString(Tags.birthdate);
                    userDataModel.nickname = object.getString(Tags.nick_name);
                    userDataModel.avtar = object.getString(Tags.avtar);
                    userDataModel.views = object.getString(Tags.views);
                    userDataModel.follower_count = object.getInt(Tags.follower_count);
                    userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                    userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                    userDataModel.bank_account = object.getString(Tags.bank_account);
                    userDataModel.certificates = object.getString(Tags.certificates);
                    userDataModel.country_id = object.getString(Tags.country_id);
                    userDataModel.role_id = object.getInt(Tags.role_id);
                    userDataModel.state_id = object.getString(Tags.state_id);
                    userDataModel.city_id = object.getString(Tags.city_id);
                    userDataModel.post_code = object.getString(Tags.post_code);
                    userDataModel.sex_group = object.getString(Tags.sex_group);
                    userDataModel.fat_status = object.getString(Tags.fat_status);
                    userDataModel.type = object.getString(Tags.type);
                    userDataModel.token = object.getString(Tags.token);
                    userDataModel.is_login = object.getInt(Tags.is_login);
                    userDataModel.is_social = object.getInt(Tags.is_social);
                    userDataModel.is_verified = object.getInt(Tags.is_verified);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    if (object.has(Tags.avtar_thumb)) {
                        userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                    }
                    trendingFollowers.add(userDataModel);
                }

                JSONArray jsonArray = response.getJSONArray(Tags.normal);
                for (int i = 0; i < jsonArray.length(); i++) {
                    UserDataModel userDataModel = new UserDataModel();
                    JSONObject object = jsonArray.getJSONObject(i);
                    userDataModel.first_name = object.getString(Tags.first_name);
                    userDataModel.last_name = object.getString(Tags.last_name);
                    userDataModel.email = object.getString(Tags.email);
                    userDataModel.password = object.getString(Tags.password);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.str_Gender = object.getString(Tags.gender);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.dob = object.getString(Tags.birthdate);
                    userDataModel.nickname = object.getString(Tags.nick_name);
                    userDataModel.avtar = object.getString(Tags.avtar);
                    userDataModel.views = object.getString(Tags.views);
                    userDataModel.follower_count = object.getInt(Tags.follower_count);
                    userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                    userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                    userDataModel.bank_account = object.getString(Tags.bank_account);
                    userDataModel.certificates = object.getString(Tags.certificates);
                    userDataModel.country_id = object.getString(Tags.country_id);
                    userDataModel.role_id = object.getInt(Tags.role_id);
                    userDataModel.state_id = object.getString(Tags.state_id);
                    userDataModel.city_id = object.getString(Tags.city_id);
                    userDataModel.post_code = object.getString(Tags.post_code);
                    userDataModel.sex_group = object.getString(Tags.sex_group);
                    userDataModel.fat_status = object.getString(Tags.fat_status);
                    userDataModel.type = object.getString(Tags.type);
                    userDataModel.token = object.getString(Tags.token);
                    userDataModel.is_login = object.getInt(Tags.is_login);
                    userDataModel.is_social = object.getInt(Tags.is_social);
                    userDataModel.is_verified = object.getInt(Tags.is_verified);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    if (object.has(Tags.avtar_thumb)) {
                        userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                    }
                    trendingGlobal.add(userDataModel);
                }
                mHandler.sendEmptyMessage(1);
            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }

    private void setIcons(ArrayList<UserDataModel> markers) {
        if (map_business != null)
            map_business.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        Marker testMarker = null;
        for (int i = 0; i < markers.size(); i++) {
            // createMarker(markers.get(i).latitude, markers.get(i).longitude, markers.get(i).nickname, markers.get(i).first_name, markers.get(i).avtar_thumb, markers.get(i).userId);
            Marker marker = createMarker(markers.get(i));
            builder.include(marker.getPosition());
            testMarker = marker;
        }
        LatLngBounds bounds = builder.build();
        int padding = 0; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map_business.animateCamera(cu);

        final Marker finalTestMarker = testMarker;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (finalTestMarker != null)
                    map_business.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().
                            target(finalTestMarker.getPosition()).
                            tilt(60).zoom(4.5f).
                            build()));
            }
        }, 300);
    }

   /* private Marker createMarker(UserDataModel markers) {
        try {
           // AppDelegate.LogT("latitude = " + latitude + ", longitude = " + longitude + ",title = " + title + ", image = " + image + ",id = " + id);
            LatLng latlong = new LatLng(markers.latitude, markers.longitude);
            if (getActivity() != null) {
                BitmapDescriptor icon = null;
                if(markers.role_id==USER) {
                    icon = BitmapDescriptorFactory.fromResource(R.drawable.user_nav);
                }else if (markers.role_id==COACH) {
                    icon = BitmapDescriptorFactory.fromResource(R.drawable.body);
                }
                customMarker = map_business.addMarker(new MarkerOptions()
                        .position(latlong)
                        .title(markers.first_name)
                        .snippet(markers.avtar)
                        .icon(icon));
                mMarkers.put(customMarker.getId(), markers.userId);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlong, 15);
                map_business.animateCamera(cameraUpdate);
                AppDelegate.LogT("Marker Created+" + "id" +  markers.userId + customMarker.getSnippet() + " Check");
                show_pin_dialog(markers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customMarker;
    }*/

    protected Marker createMarker(UserDataModel markers) {
        AppDelegate.LogT("createMarker====" + markers.latitude + "");
        try {
            // AppDelegate.LogT("latitude = " + latitude + ", longitude = " + longitude + ",title = " + title + ", image = " + image + ",id = " + id);
            LatLng latlong = new LatLng(markers.latitude, markers.longitude);
            if (getActivity() != null) {
                View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
                final CircleImageView cimg_user = (CircleImageView) marker.findViewById(R.id.cimg_user);
                final ImageView img_loading = (ImageView) marker.findViewById(R.id.img_loading);
                img_loading.setVisibility(View.VISIBLE);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                        frameAnimation.setCallback(img_loading);
                        frameAnimation.setVisible(true, true);
                        frameAnimation.start();
                        ((Animatable) img_loading.getDrawable()).start();
                    }
                });
//                img_loading.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
//                        frameAnimation.setCallback(img_loading);
//                        frameAnimation.setVisible(true, true);
//                        frameAnimation.start();
//                        ((Animatable) img_loading.getDrawable()).start();
//                    }
//                });
                //   AppDelegate.LogT("logo" + image + "=");
                if (AppDelegate.isValidString(markers.avtar_thumb)) {
                    imageLoader.loadImage(markers.avtar_thumb, options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                            cimg_user.setImageBitmap(bitmap);
                            img_loading.setVisibility(View.GONE);
                            if (shouldLoad) {
                                loadImages();
                                if (selected_tab == 0) {
                                    setIcons(trendingGlobal);
                                } else {
                                    setIcons(trendingFollowers);
                                }
                            }
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });

/*
                    Picasso.with(getActivity())
                            .load(markers.avtar_thumb)
                            .error(R.drawable.female)
                            .placeholder(R.drawable.female)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                    cimg_user.setImageBitmap(bitmap);
                                    img_loading.setVisibility(View.GONE);
                                    if (shouldLoad) {
                                        loadImages();
                                        if (selected_tab == 0) {
                                            setIcons(trendingGlobal);
                                        } else {
                                            setIcons(trendingFollowers);
                                        }
                                    }
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {
                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {
                                }
                            });*/
                } else {

                }
                customMarker = map_business.addMarker(new MarkerOptions()
                        .position(latlong)
                        .title(markers.first_name)
                        .snippet(markers.avtar)
                        .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))));
              /*  customMarker = map_business.addMarker(new MarkerOptions()
                        .position(latlong)
                        .title(title)
                        .snippet(firstname)
                        .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))));*/
                mMarkers.put(customMarker.getId(), markers.userId);
                //  CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlong, 15);
                //  map_business.animateCamera(cameraUpdate);

                AppDelegate.LogT("Marker Created+" + "id" + markers.userId + customMarker.getSnippet() + " Check");
                //  show_pin_dialog(markers);
                map_business.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        try {
                            int id = mMarkers.get(marker.getId());
                            AppDelegate.LogT("post user id" + id + "checked" + marker.getId() + "marker Id");
                            show_user_post(id, marker.getTitle(), marker.getSnippet());
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customMarker;
    }

    boolean shouldLoad = true;

    private void loadImages() {
        shouldLoad = false;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                shouldLoad = true;
            }
        }, 2000);
    }

    /*private void show_pin_dialog(final int id) {
        // TODO Auto-generated method stub
        map_business.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // TODO Auto-generated method stub
                View v = getActivity().getLayoutInflater().inflate(R.layout.map_pin_showinfo_row, null);
                try {
                    // Set desired height and width
                    v.setLayoutParams(new RelativeLayout.LayoutParams(300, RelativeLayout.LayoutParams.WRAP_CONTENT));
                    TextView BranchId = (TextView) v.findViewById(R.id.tagIdTextView);
                    BranchId.setText(marker.getSnippet());
                    AppDelegate.LogT("post user id" + id + "checked" + marker.getId() + "marker Id => " + marker.getSnippet());
                    map_business.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        public void onInfoWindowClick(Marker marker) {
                            try {
                                int id = mMarkers.get(marker.getId());
                                AppDelegate.LogT("post user id" + id + "checked" + marker.getId() + "marker Id");
                                show_user_post(id, marker.getSnippet());
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                return v;
            }
        });
    }*/
    private void show_pin_dialog(final UserDataModel userDataModel) {
        // TODO Auto-generated method stub
        map_business.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // TODO Auto-generated method stub
                View v = getActivity().getLayoutInflater().inflate(R.layout.map_pin_showinfo_row, null);
                try {
                    // Set desired height and width
                    v.setLayoutParams(new RelativeLayout.LayoutParams(300, RelativeLayout.LayoutParams.WRAP_CONTENT));
                    TextView BranchId = (TextView) v.findViewById(R.id.tagIdTextView);
                    BranchId.setText(marker.getTitle());
                    // AppDelegate.LogT("post user id" + id + "checked" + marker.getId() + "marker Id => " + marker.getSnippet());
                    map_business.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        public void onInfoWindowClick(Marker marker) {
                            try {
                                int id = mMarkers.get(marker.getId());
                                AppDelegate.LogT("post user id" + id + "checked" + marker.getId() + "marker Id");
                                show_user_post(id, marker.getTitle(), marker.getSnippet());
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                return v;
            }
        });
    }

    private void show_user_post(int id, String snippet, String avtar) {
        try {
            // Intent intent = new Intent(getActivity(), ViewImageVideoFragment.class);
            new Prefs(getActivity()).putIntValue("post_id", id);
            new Prefs(getActivity()).putStringValue("snippet", snippet);
            new Prefs(getActivity()).putStringValue("avtar", avtar);
            AppDelegate.LogT("clickedsnippet" + id + "checked");
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new ViewImageVideoFragment(), R.id.container2);
            //startActivity(intent);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void show_user_post(int id) {
        Intent intent = new Intent(getActivity(), FitDayActivity.class);
        new Prefs(getActivity()).putIntValue("post_id", id);
        AppDelegate.LogT("clicked user id" + id + "checked");
        startActivity(intent);
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        AppDelegate.LogT("Bitmap" + bitmap);
        return bitmap;
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }

    @Override
    public void onLocationChanged(Location location) {
       /* LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
        map_business.animateCamera(cameraUpdate);
        locationManager.removeUpdates(this);
        execute_getNearby();*/
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
