package fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fitplus.AppDelegate;
import com.fitplus.CreateTeamActivity;
import com.fitplus.MainActivity;
import com.fitplus.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CustomTimePickerDialog;
import adapters.TeamListAdapter;
import butterknife.ButterKnife;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.TeamListModel;
import utils.Prefs;
import utils.decorators.CurrentDateDecorator;
import utils.decorators.MySelectorDecorator;
import utils.decorators.OneDayDecorator;

/**
 * Created by admin on 26-07-2016.
 */
public class SetDateTimeFragments extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {
    private View rootview;
    RelativeLayout toolbar;
    TextView toolbar_title;
    ImageView tool_menu;
    RecyclerView team_list;
    private Bundle bundle;
    private CustomTimePickerDialog timePicker;
    private int selectfromhour, selectfromminute;
    private String timeset;
    private TextView time;
    private MaterialCalendarView date;
    private String setdate;
    private int team_id_type;
    private TextView month, txt_hour, txt_monthly;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private RelativeLayout rel_tab_bar;
    private OneDayDecorator oneDayDecorator = new OneDayDecorator(getActivity());
    private SimpleDateFormat month_date;
    private Dialog builder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.set_datetime, container, false);
        ButterKnife.bind(getActivity());
        bundle = getArguments();
        team_id_type = bundle.getInt(Tags.Find_Coach);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        show_dialog();
    }
    private void initView() {
        toolbar = (RelativeLayout) rootview.findViewById(R.id.toolbar);
        rel_tab_bar = (RelativeLayout) rootview.findViewById(R.id.rel_tab_bar);
        toolbar_title = (TextView) rootview.findViewById(R.id.title);
        tool_menu = (ImageView) rootview.findViewById(R.id.back);
        tool_menu.setOnClickListener(this);
        time = (TextView) rootview.findViewById(R.id.time);
        txt_hour = (TextView) rootview.findViewById(R.id.hour);
        txt_monthly = (TextView) rootview.findViewById(R.id.monthly);
        date = (MaterialCalendarView) rootview.findViewById(R.id.date);
        month = (TextView) rootview.findViewById(R.id.month);
        Calendar calender = Calendar.getInstance();
        Date currentLocalTime = calender.getTime();
        DateFormat dates = new SimpleDateFormat("HH:mm");
        String localTime = dates.format(currentLocalTime);
        time.setText(localTime + "");
        date.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                Date calender = date.getDate();
                setdate = new SimpleDateFormat("yyyy-MM-dd").format(calender).toString();
                Log.e("Selected date", setdate + "");
                widget.invalidateDecorators();
                set_time();
            }
        });
        date.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        Calendar instance = Calendar.getInstance();
        CalendarDay day = CalendarDay.from(instance.getTime());
        ArrayList<CalendarDay> calenderdates = new ArrayList<>();
        calenderdates.add(day);
        date.addDecorator(new CurrentDateDecorator(getActivity(), calenderdates));
        date.setTopbarVisible(true);
        Calendar cal = Calendar.getInstance();
        month_date = new SimpleDateFormat("MMMM yyyy");
        String month_name = month_date.format(cal.getTime());
        month.setText("" + month_name);
        date.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, date.getMonth());
                calendar.set(Calendar.YEAR, date.getYear());
                month_date = new SimpleDateFormat("MMMM yyyy");
                String month_name = month_date.format(calendar.getTime());
                month.setText(month_name + "");
            }
        });
        date.state().edit().setMinimumDate(Calendar.getInstance().getTime())
                //.setMaximumDate(instance2.getTime())
                .commit();
        date.addDecorators(
                new MySelectorDecorator(getActivity()),
                //new HighlightWeekendsDecorator(),
                oneDayDecorator
        );
        time.setOnClickListener(this);
        rootview.findViewById(R.id.done).setOnClickListener(this);
        rel_tab_bar.setVisibility(View.GONE);
    }

    private class DateSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {
        ArrayList<CalendarDay> calenderdates;

        public DateSimulator(ArrayList<CalendarDay> calenderdates) {
            this.calenderdates = calenderdates;
        }

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return calenderdates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);
            if (getActivity().isFinishing()) {
                return;
            }
             date.addDecorator(new CurrentDateDecorator(getActivity(), calendarDays));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void show_dialog() {
        if (builder != null && builder.isShowing()) {
            builder.dismiss();
        }
        builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.findcoach_alert);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok;
        builder.setCancelable(false);
        ok = (TextView) builder.findViewById(R.id.yes);
        builder.show();
        // builder.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                getFragmentManager().popBackStack();
                break;
            case R.id.time:
                AppDelegate.LogT("time clicked");
                set_time();
                break;
            case R.id.done:
                if (AppDelegate.isValidString(timeset) && AppDelegate.isValidString(setdate)) {
                    AppDelegate.showDialog_twoButtons(getActivity(), getResources().getString(R.string.find_a_coach), Tags.Find_Coach, this);
                } else if (!AppDelegate.isValidString(setdate)) {
                    AppDelegate.ShowDialog(getActivity(), "Please select date.", "Time out!!!");
                } else {
                    if (!AppDelegate.isValidString(setdate)) {
                        AppDelegate.ShowDialog(getActivity(), "Please select time.", "Time out!!!");
                    }
                }
                break;
        }
    }

    private void execute_find_coach() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.team_id, team_id_type, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.training_date, setdate + " " + timeset);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.deviceType, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.training_period, 1, ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(getActivity(), SetDateTimeFragments.this, ServerRequestConstants.FIND_COACH,
                        mPostArrayList, SetDateTimeFragments.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.try_again), "Alert!!!");
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {

        }
    }

    private void set_time() {
        timePicker = new CustomTimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar calender = Calendar.getInstance();
                calender.set(Calendar.HOUR, selectedHour);
                calender.set(Calendar.MINUTE, selectedMinute);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                String test = sdf.format(calender.getTime());
                selectfromhour = selectedHour;
                selectfromminute = selectedMinute;
                time.setText(test+"");
                timeset =test;
             /*   if (selectedHour < 12 && selectedHour >= 0) {
                    time.setText(selectedHour + ":" + selectedMinute );
                    timeset = selectedHour + ":" + selectedMinute;
                } else {
                    selectedHour -= 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                    time.setText(selectedHour + ":" + selectedMinute );
                    timeset = selectedHour + ":" + selectedMinute;
                }*/

            }
        }, selectfromhour, selectfromminute, false);//Yes 24 hour time
        timePicker.show();

        AppDelegate.LogT("date=>..." + timeset);
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FIND_COACH)) {
            parseFitTeamList(result);
        } else if (apiName.equals(ServerRequestConstants.CREATE_TEAM)) {
            parseCreateTeamList(result);
        }
    }

    private void parseCreateTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
//            AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, SetDateTimeFragments.this);
            //int request_id = jsonObject.getInt("request_id");
            /*
            if (jsonObject.getInt(Tags.status) == 1) {

            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, SetDateTimeFragments.this);
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null *//*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*//*) {
                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, SetDateTimeFragments.this);
                }
            }*/
            getFragmentManager().popBackStack();
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    private void parseFitTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                TeamListModel userDataModel = new TeamListModel();
                userDataModel.team_id = object.getInt(Tags.id);
                userDataModel.team_name = object.getString(Tags.team_name);
                userDataModel.objective = object.getString(Tags.objective);
                userDataModel.first_member_id = object.getInt(Tags.first_member_id);
                userDataModel.second_member_id = object.getInt(Tags.second_member_id);
                userDataModel.third_member_id = object.getInt(Tags.third_member_id);
                userDataModel.coach_id = object.getInt(Tags.coach_id);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.team_avtar = object.getString(Tags.team_avtar);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.createName, userDataModel);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new RadarFragment(), R.id.main_content, bundle, null);
               // AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, GlobalMapFragments.this);
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
//                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null/*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }


    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.Find_Coach)) {
            execute_find_coach();
        }
    }


}
