package model;

import com.fitplus.AppDelegate;

import org.json.JSONException;
import org.json.JSONObject;

import Constants.Tags;

/**
 * Created by admin on 28-07-2016.
 */
public class FbDetails {
    private String profilePicUrl;

    public FbDetails() {
    }

    public Fb_detail_GetSet getFacebookDetail(String string) {
        Fb_detail_GetSet fb_detail_getSet = new Fb_detail_GetSet();
        try {
            AppDelegate.LogT("message+" + string + "");
            JSONObject object = new JSONObject(string);
            if (object.has("picture")) {
                profilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                if (profilePicUrl.contains("&")) {
                    profilePicUrl = profilePicUrl.replace("&", "~");
                }
            }
            AppDelegate.LogT("profilePicUrl===" + profilePicUrl);
            fb_detail_getSet.setSocial_id(object.getString(Tags.id));
            fb_detail_getSet.setFirstname(object.getString(Tags.first_name));
            fb_detail_getSet.setLast_name(object.getString(Tags.last_name));
            if(object.has(Tags.birthday)) {
                fb_detail_getSet.setBitrhday(object.getString(Tags.birthday));
            }
            fb_detail_getSet.setGender(object.getString(Tags.gender));
            fb_detail_getSet.setName(object.getString(Tags.name));
            fb_detail_getSet.setGid("");
            fb_detail_getSet.setProfile_pic(profilePicUrl + "");
            //AppDelegate.LogT(fb_detail_getSet + "");
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
        return fb_detail_getSet;
    }
}
