package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 11-08-2016.
 */
public class TeamListModel implements Parcelable {
    public int team_id,first_member_id,second_member_id,third_member_id,coach_id,status;
    public double latitude,longitude;
    public String team_name,objective,lastMessage,created, team_avtar,datetime;

    public TeamListModel() {

    }

    protected TeamListModel(Parcel in) {
        team_id = in.readInt();
        first_member_id = in.readInt();
        second_member_id = in.readInt();
        third_member_id = in.readInt();
        coach_id = in.readInt();
        status = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        team_name = in.readString();
        objective = in.readString();
        lastMessage = in.readString();
        created = in.readString();
        team_avtar = in.readString();
        datetime=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(team_id);
        dest.writeInt(first_member_id);
        dest.writeInt(second_member_id);
        dest.writeInt(third_member_id);
        dest.writeInt(coach_id);
        dest.writeInt(status);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(team_name);
        dest.writeString(objective);
        dest.writeString(lastMessage);
        dest.writeString(created);
        dest.writeString(team_avtar);
        dest.writeString(datetime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TeamListModel> CREATOR = new Creator<TeamListModel>() {
        @Override
        public TeamListModel createFromParcel(Parcel in) {
            return new TeamListModel(in);
        }

        @Override
        public TeamListModel[] newArray(int size) {
            return new TeamListModel[size];
        }
    };
}
