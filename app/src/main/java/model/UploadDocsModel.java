package model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

/**
 * Created by admin on 11-08-2016.
 */
public class UploadDocsModel implements Parcelable {
    public int index;
    public File image;
    public Bitmap bitmap;

    @Override
    public String toString() {
        return "UploadDocsModel{" +
                "index=" + index +
                ", image=" + image +
                ", bitmap=" + bitmap +
                '}';
    }

    public UploadDocsModel() {
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    protected UploadDocsModel(Parcel in) {
        index = in.readInt();
        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<UploadDocsModel> CREATOR = new Creator<UploadDocsModel>() {
        @Override
        public UploadDocsModel createFromParcel(Parcel in) {
            return new UploadDocsModel(in);
        }

        @Override
        public UploadDocsModel[] newArray(int size) {
            return new UploadDocsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(index);
        dest.writeParcelable(bitmap, flags);
    }
}
