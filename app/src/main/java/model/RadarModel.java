package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 13-08-2016.
 */
public class RadarModel implements Parcelable
{
    public UserDataModel coach_profile;
   public int status, dataFlow,id,request_id,coach_id,coach_action,final_status,followers;
    public double distance;
    public String message;

    public RadarModel() {
    }

    @Override
    public String toString() {
        return "RadarModel{" +
                "coach_profile=" + coach_profile +
                ", status=" + status +
                ", dataFlow=" + dataFlow +
                ", id=" + id +
                ", request_id=" + request_id +
                ", coach_id=" + coach_id +
                ", coach_action=" + coach_action +
                ", final_status=" + final_status +
                ", followers=" + followers +
                ", distance=" + distance +
                ", message='" + message + '\'' +
                '}';
    }

    protected RadarModel(Parcel in) {
        coach_profile = (UserDataModel)in.readParcelable(UserDataModel.class.getClassLoader());
        status = in.readInt();
        dataFlow = in.readInt();
        id = in.readInt();
        request_id = in.readInt();
        coach_id = in.readInt();
        coach_action = in.readInt();
        final_status = in.readInt();
        followers = in.readInt();
        distance = in.readDouble();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(coach_profile, flags);
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeInt(id);
        dest.writeInt(request_id);
        dest.writeInt(coach_id);
        dest.writeInt(coach_action);
        dest.writeInt(final_status);
        dest.writeInt(followers);
        dest.writeDouble(distance);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RadarModel> CREATOR = new Creator<RadarModel>() {
        @Override
        public RadarModel createFromParcel(Parcel in) {
            return new RadarModel(in);
        }

        @Override
        public RadarModel[] newArray(int size) {
            return new RadarModel[size];
        }
    };
}
