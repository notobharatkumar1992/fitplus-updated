package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by parul on 15/1/16.
 */
public class UserDataModel implements Parcelable {
    public String block_on,block_for,message, first_name, last_name, email, dob, str_Gender, password, nickname, created, modified, city_name, views, social_id;
    public int userId, status, datatflow;
    public String avtar, about_me,language, avtar_thumb, presentation_vedio, bank_account, certificates, country_id, state_id, city_id, post_code, sex_group, fat_status, type, token;
    public int is_login, is_social, is_verified, follower_count, profile_visibility;
    public double latitude, longitude;
    public int role_id, is_followed,notification_status;
    public String gid, fid;

    public UserDataModel() {
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "block_on='" + block_on + '\'' +
                ", block_for='" + block_for + '\'' +
                ", message='" + message + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", dob='" + dob + '\'' +
                ", str_Gender='" + str_Gender + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", created='" + created + '\'' +
                ", modified='" + modified + '\'' +
                ", city_name='" + city_name + '\'' +
                ", views='" + views + '\'' +
                ", social_id='" + social_id + '\'' +
                ", userId=" + userId +
                ", status=" + status +
                ", datatflow=" + datatflow +
                ", avtar='" + avtar + '\'' +
                ", about_me='" + about_me + '\'' +
                ", language='" + language + '\'' +
                ", avtar_thumb='" + avtar_thumb + '\'' +
                ", presentation_vedio='" + presentation_vedio + '\'' +
                ", bank_account='" + bank_account + '\'' +
                ", certificates='" + certificates + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", city_id='" + city_id + '\'' +
                ", post_code='" + post_code + '\'' +
                ", sex_group='" + sex_group + '\'' +
                ", fat_status='" + fat_status + '\'' +
                ", type='" + type + '\'' +
                ", token='" + token + '\'' +
                ", is_login=" + is_login +
                ", is_social=" + is_social +
                ", is_verified=" + is_verified +
                ", follower_count=" + follower_count +
                ", profile_visibility=" + profile_visibility +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", role_id=" + role_id +
                ", is_followed=" + is_followed +
                ", notification_status=" + notification_status +
                ", gid='" + gid + '\'' +
                ", fid='" + fid + '\'' +
                '}';
    }

    protected UserDataModel(Parcel in) {
        block_on = in.readString();
        block_for = in.readString();
        message = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        dob = in.readString();
        str_Gender = in.readString();
        password = in.readString();
        nickname = in.readString();
        created = in.readString();
        modified = in.readString();
        city_name = in.readString();
        views = in.readString();
        social_id = in.readString();
        userId = in.readInt();
        status = in.readInt();
        datatflow = in.readInt();
        avtar = in.readString();
        about_me = in.readString();
        language = in.readString();
        avtar_thumb = in.readString();
        presentation_vedio = in.readString();
        bank_account = in.readString();
        certificates = in.readString();
        country_id = in.readString();
        state_id = in.readString();
        city_id = in.readString();
        post_code = in.readString();
        sex_group = in.readString();
        fat_status = in.readString();
        type = in.readString();
        token = in.readString();
        is_login = in.readInt();
        is_social = in.readInt();
        is_verified = in.readInt();
        follower_count = in.readInt();
        profile_visibility = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        role_id = in.readInt();
        is_followed = in.readInt();
        notification_status = in.readInt();
        gid = in.readString();
        fid = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(block_on);
        dest.writeString(block_for);
        dest.writeString(message);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(dob);
        dest.writeString(str_Gender);
        dest.writeString(password);
        dest.writeString(nickname);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(city_name);
        dest.writeString(views);
        dest.writeString(social_id);
        dest.writeInt(userId);
        dest.writeInt(status);
        dest.writeInt(datatflow);
        dest.writeString(avtar);
        dest.writeString(about_me);
        dest.writeString(language);
        dest.writeString(avtar_thumb);
        dest.writeString(presentation_vedio);
        dest.writeString(bank_account);
        dest.writeString(certificates);
        dest.writeString(country_id);
        dest.writeString(state_id);
        dest.writeString(city_id);
        dest.writeString(post_code);
        dest.writeString(sex_group);
        dest.writeString(fat_status);
        dest.writeString(type);
        dest.writeString(token);
        dest.writeInt(is_login);
        dest.writeInt(is_social);
        dest.writeInt(is_verified);
        dest.writeInt(follower_count);
        dest.writeInt(profile_visibility);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(role_id);
        dest.writeInt(is_followed);
        dest.writeInt(notification_status);
        dest.writeString(gid);
        dest.writeString(fid);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel in) {
            return new UserDataModel(in);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };
}
