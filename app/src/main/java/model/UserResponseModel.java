package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 12-08-2016.
 */
public class UserResponseModel implements Parcelable {
    public int status, dataflow;
    public String message;
    public String response, team_name, team_members, notify_id;
    public String team_id;
    public String request_type, team_avtar, user_avtar, coachname, coach_id;
    public String FirstAvtar,ThirdAvtar,SecondAvtar,username_second,username_first,username_third;

    public UserResponseModel() {
    }

    @Override
    public String toString() {
        return "UserResponseModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", message='" + message + '\'' +
                ", response='" + response + '\'' +
                ", team_name='" + team_name + '\'' +
                ", team_members='" + team_members + '\'' +
                ", notify_id='" + notify_id + '\'' +
                ", team_id='" + team_id + '\'' +
                ", request_type='" + request_type + '\'' +
                ", team_avtar='" + team_avtar + '\'' +
                ", user_avtar='" + user_avtar + '\'' +
                ", coachname='" + coachname + '\'' +
                ", coach_id='" + coach_id + '\'' +
                ", FirstAvtar='" + FirstAvtar + '\'' +
                ", ThirdAvtar='" + ThirdAvtar + '\'' +
                ", SecondAvtar='" + SecondAvtar + '\'' +
                ", username_second='" + username_second + '\'' +
                ", username_first='" + username_first + '\'' +
                ", username_third='" + username_third + '\'' +
                '}';
    }

    protected UserResponseModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        message = in.readString();
        response = in.readString();
        team_name = in.readString();
        team_members = in.readString();
        notify_id = in.readString();
        team_id = in.readString();
        request_type = in.readString();
        team_avtar = in.readString();
        user_avtar = in.readString();
        coachname = in.readString();
        coach_id = in.readString();
        FirstAvtar = in.readString();
        ThirdAvtar = in.readString();
        SecondAvtar = in.readString();
        username_second = in.readString();
        username_first = in.readString();
        username_third = in.readString();
    }

    public static final Creator<UserResponseModel> CREATOR = new Creator<UserResponseModel>() {
        @Override
        public UserResponseModel createFromParcel(Parcel in) {
            return new UserResponseModel(in);
        }

        @Override
        public UserResponseModel[] newArray(int size) {
            return new UserResponseModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeString(message);
        dest.writeString(response);
        dest.writeString(team_name);
        dest.writeString(team_members);
        dest.writeString(notify_id);
        dest.writeString(team_id);
        dest.writeString(request_type);
        dest.writeString(team_avtar);
        dest.writeString(user_avtar);
        dest.writeString(coachname);
        dest.writeString(coach_id);
        dest.writeString(FirstAvtar);
        dest.writeString(ThirdAvtar);
        dest.writeString(SecondAvtar);
        dest.writeString(username_second);
        dest.writeString(username_first);
        dest.writeString(username_third);
    }
}
