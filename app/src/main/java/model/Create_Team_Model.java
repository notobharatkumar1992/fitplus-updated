package model;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created by admin on 10-08-2016.
 */
public class Create_Team_Model implements Parcelable {
    public int status, dataflow ,id,coach_id;
    public String first_member_id,longitude,latitude,team_name,created,modified,message,team_avtar;

    public Create_Team_Model() {
    }

    protected Create_Team_Model(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        first_member_id = in.readString();
        longitude = in.readString();
        latitude = in.readString();
        team_name = in.readString();
        created = in.readString();
        modified = in.readString();
        message = in.readString();
        team_avtar=in.readString();
        coach_id=in.readInt();
    }

    public static final Creator<Create_Team_Model> CREATOR = new Creator<Create_Team_Model>() {
        @Override
        public Create_Team_Model createFromParcel(Parcel in) {
            return new Create_Team_Model(in);
        }

        @Override
        public Create_Team_Model[] newArray(int size) {
            return new Create_Team_Model[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeString(first_member_id);
        dest.writeString(longitude);
        dest.writeString(latitude);
        dest.writeString(team_name);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(message);
        dest.writeString(team_avtar);
        dest.writeInt(coach_id);
    }
}
