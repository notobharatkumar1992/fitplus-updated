package Async;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by admin on 23-06-2016.
 */
public class LoadImage extends AsyncTask<String, String, Bitmap> {
    private Bitmap bitmap;
    ImageView userimg;
    public LoadImage(ImageView userimg) {
       this. userimg=userimg;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
      /*  pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading Image ....");
        pDialog.show();*/

    }
    protected Bitmap doInBackground(String... args) {
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
    protected void onPostExecute(Bitmap image) {

        if(image != null){
            userimg.setImageBitmap(image);
          //  pDialog.dismiss();
        }else{
                    //  Toast.makeText(MainActivity.this, "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();

        }
    }
}
