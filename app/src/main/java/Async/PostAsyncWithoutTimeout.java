package Async;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.fitplus.AppDelegate;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.File;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;

import Constants.ServerRequestConstants;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;


/**
 * BMA_POSTASYNC class is used to execute POST Request By Using this class we
 * can send Data(image, text, multimedia content etc) on Server
 */

@SuppressWarnings("deprecation")
public class PostAsyncWithoutTimeout extends AsyncTask<String, Void, String> {
    /**
     * BMA_GETASYNC Members Declarations
     */
    private OnReciveServerResponse mOnReciveServerResponse;
    private String str_PostApiName;
    private String str_PostRequestURL;
    private ArrayList<PostAysnc_Model> arr_PostModels;
    private StringBuilder builderPost;
    private Fragment fragment;
    @SuppressWarnings("unused")
    private Context mContext;

    /**
     * Constructor Implementations
     */
    public PostAsyncWithoutTimeout(Context context,
                                   OnReciveServerResponse onReciveServerResponse, String mPostApiName,
                                   String mPostRequestURL, ArrayList<PostAysnc_Model> mPostModelArray,
                                   Fragment fragment) {
        this.mOnReciveServerResponse = onReciveServerResponse;
        this.str_PostApiName = mPostApiName;
        this.str_PostRequestURL = mPostRequestURL;
        this.arr_PostModels = mPostModelArray;
        this.builderPost = null;
        this.fragment = fragment;
        this.mContext = context;
    }

    /**
     * Constructor Implementations
     */
    public PostAsyncWithoutTimeout(Context context,
                                   OnReciveServerResponse onReciveServerResponse,
                                   String mPostRequestURL, ArrayList<PostAysnc_Model> mPostModelArray,
                                   Fragment fragment) {
        this.mOnReciveServerResponse = onReciveServerResponse;
        this.str_PostApiName = mPostRequestURL;
        this.str_PostRequestURL = mPostRequestURL;
        this.arr_PostModels = mPostModelArray;
        this.builderPost = null;
        this.fragment = fragment;
        this.mContext = context;
    }

    /**
     * This Method is called before the execution start
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * This Method is called during the execution
     */
    @Override
    protected String doInBackground(String... params) {
        try {
            /**
             * Establishing Connection with Server
             */
            HttpClient mHttpClient = new DefaultHttpClient(getHttpParameters());
            HttpContext mHttpContext = new BasicHttpContext();
            HttpPost mHttpPost = new HttpPost(str_PostRequestURL);
            /**
             * Setting up Parameters with the request
             */
            MultipartEntity mMultipartEntity = new MultipartEntity(
                    HttpMultipartMode.BROWSER_COMPATIBLE);
            /**
             * Process to get Request Parameter Values from ArrayList ArryList
             * Value str_PostParamMIME Represent the MIME Type of Data and
             * str_PostParamKey Represent the Request Param Key and
             * Obj_PostParamValue Represent the their values
             */
            for (int i = 0; i < arr_PostModels.size(); i++) {
                if (arr_PostModels
                        .get(i)
                        .getStr_PostParamType()
                        .equalsIgnoreCase(
                                (ServerRequestConstants.Key_PostStrValue))) {

                    if (arr_PostModels.get(i).getObj_PostParamValue() != null) {
//                        if (str_PostRequestURL.equalsIgnoreCase(ServerRequestConstants.REGISTRATION)) {
//                            mMultipartEntity.addPart("student_detail['" + arr_PostModels.get(i)
//                                    .getStr_PostParamKey() + "']", new StringBody(
//                                    (String) arr_PostModels.get(i)
//                                            .getObj_PostParamValue()));
//
//                        } else
                        mMultipartEntity.addPart(arr_PostModels.get(i)
                                .getStr_PostParamKey(), new StringBody(
                                arr_PostModels.get(i)
                                        .getObj_PostParamValue() + ""));
                    }
                } else if (arr_PostModels
                        .get(i)
                        .getStr_PostParamType()
                        .equalsIgnoreCase(
                                (ServerRequestConstants.Key_PostintValue))) {

                    if (arr_PostModels.get(i).getObj_PostParamValue() != null) {
                        mMultipartEntity.addPart(
                                arr_PostModels.get(i).getStr_PostParamKey(),
                                new StringBody(String.valueOf(arr_PostModels
                                        .get(i).getObj_PostParamValue())));
                    }
                } else if (arr_PostModels
                        .get(i)
                        .getStr_PostParamType()
                        .equalsIgnoreCase(
                                (ServerRequestConstants.Key_PostDoubleValue))) {

                    if (arr_PostModels.get(i).getObj_PostParamValue() != null) {
                        mMultipartEntity.addPart(
                                arr_PostModels.get(i).getStr_PostParamKey(),
                                new StringBody(String.valueOf(arr_PostModels
                                        .get(i).getObj_PostParamValue())));
                    }
                } else if (arr_PostModels
                        .get(i)
                        .getStr_PostParamType()
                        .equalsIgnoreCase(
                                (ServerRequestConstants.Key_PostFileValue))) {

                    if (arr_PostModels.get(i).getObj_PostParamValue() != null) {
                        mMultipartEntity.addPart(
                                arr_PostModels.get(i).getStr_PostParamKey(),
                                new FileBody(new File(String.valueOf(arr_PostModels
                                        .get(i).getObj_PostParamValue()))));
                    }
                }
                if (builderPost == null) {
                    builderPost = new StringBuilder();
                    if (arr_PostModels.size() == 1) {
                        builderPost.append("{ "
                                + arr_PostModels.get(i).getStr_PostParamKey()
                                + " = "
                                + arr_PostModels.get(i).getObj_PostParamValue()
                                + " }");
                    } else {
                        builderPost
                                .append("{ "
                                        + arr_PostModels.get(i)
                                        .getStr_PostParamKey()
                                        + " = "
                                        + arr_PostModels.get(i)
                                        .getObj_PostParamValue());
                    }
                } else if (arr_PostModels.size() - 1 == i) {
                    builderPost.append(", "
                            + arr_PostModels.get(i).getStr_PostParamKey()
                            + " = "
                            + arr_PostModels.get(i).getObj_PostParamValue()
                            + " }");
                } else {
                    builderPost.append(", "
                            + arr_PostModels.get(i).getStr_PostParamKey()
                            + " = "
                            + arr_PostModels.get(i).getObj_PostParamValue());
                }
            }
            AppDelegate.LogUA(str_PostRequestURL);
            if (arr_PostModels.size() > 0) {
//                String str_access_token = GCMClientManager.getRegistrationId(mContext);
//                builderPost.append(", " + Tags.ACCESS_TOKEN + " = "
//                        + str_access_token);
//                mMultipartEntity.addPart(Tags.ACCESS_TOKEN, new StringBody(
//                        str_access_token));
                AppDelegate.LogUP(builderPost.toString());
                mHttpPost.setEntity(mMultipartEntity);
            }
            HttpResponse mHttpResponse = mHttpClient.execute(mHttpPost,
                    mHttpContext);
            if (mHttpResponse != null) {
                HttpEntity mHttpEntity = mHttpResponse.getEntity();
                InputStream mInputStream = mHttpEntity.getContent();
                if (mInputStream != null) {
                    String mPostResult = AppDelegate
                            .convertStreamToString(mInputStream);
                    if (mPostResult != null) {
                        return mPostResult;
                    } else {
                        AppDelegate.LogE("mPostResult = null for api = "
                                + str_PostApiName);
                        return null;
                    }
                } else {
                    AppDelegate.LogE("mInputStream = null for api = "
                            + str_PostApiName);
                    return null;
                }
            } else {
                AppDelegate.LogE("mHttpResponse = null for api = "
                        + str_PostApiName);
                return null;
            }
        } catch (UnknownHostException unkownHostEx) {
            AppDelegate.LogE(unkownHostEx);
            AppDelegate.LogE("for api = " + str_PostApiName, unkownHostEx);
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.LogE("for api = " + str_PostApiName, e);
        }
        return null;
    }

    /**
     * This Method is called after the execution finish
     */
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (fragment == null) {
            AppDelegate.LogUR(result);
            this.mOnReciveServerResponse.setOnReciveResult(str_PostApiName,
                    result);
        } else if (fragment != null && fragment.isAdded()) {
            AppDelegate.LogUR(result);
            this.mOnReciveServerResponse.setOnReciveResult(str_PostApiName,
                    result);
        } else {
            // if (mContext != null)
            // AppDelegate.hideLoadingDialog(mContext);
            AppDelegate.LogE(fragment.getClass().getName()
                    + " is removed on api result = " + result);
        }
    }

    public static HttpParams getHttpParameters() {
        HttpParams httpParameters = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        HttpConnectionParams.setConnectionTimeout(httpParameters, 60000);
        // Set the default socket timeout (SO_TIMEOUT)
        // in milliseconds which is the timeout for waiting for data.
        HttpConnectionParams.setSoTimeout(httpParameters, 60000);
        return httpParameters;
    }
}
