package utils.decorators;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.style.StyleSpan;

import com.fitplus.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Date;

/**
 * Decorate a day by making the text big and bold
 */
public class OneDayDecorator implements DayViewDecorator {

    private CalendarDay date;
    private Drawable drawable;
    Context context;
    public OneDayDecorator(Context context) {
        this.context=context;
        date = CalendarDay.today();
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return date != null && day.equals(date);
    }

    @Override
    public void decorate(DayViewFacade view) {
        if(context!=null) {
            drawable = context.getResources().getDrawable(R.drawable.my_selector);
            view.addSpan(new StyleSpan(Typeface.BOLD));
            if(drawable!=null) {
                Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                //  view.setSelectionDrawable(drawable);
                view.setSelectionDrawable(transparentDrawable);
                view.setBackgroundDrawable(drawable);
            }
           // view.setSelectionDrawable(drawable);
        }
       /* view.addSpan(new RelativeSizeSpan(1.4f));*/
    }
    /**
     * We're changing the internals, so make sure to call {@linkplain MaterialCalendarView#invalidateDecorators()}
     */
    public void setDate(Date date) {
        this.date = CalendarDay.from(date);
    }
}
