package com.videodemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Bharat on 09/27/2016.
 */
public class MyView extends ImageView {

    public Bitmap mBitmap;
    public Canvas mCanvas;
    public Path mPath, circlePath;
    public Paint mPaint;
    public Paint mBitmapPaint, circlePaint;

    private ArrayList<Path> undonePaths = new ArrayList<Path>();
    private ArrayList<Path> paths = new ArrayList<Path>();


    private ArrayList<Bitmap> undoneBitmap = new ArrayList<>();
    private ArrayList<Bitmap> pathsBitmap = new ArrayList<>();

    public MyView(Context c) {
        super(c);
        circlePaint = new Paint();
        circlePath = new Path();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(Color.TRANSPARENT);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeJoin(Paint.Join.MITER);
        circlePaint.setStrokeWidth(4f);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFFFF0000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);

        mPath = new Path();
        paths.add(mPath);

        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
    }

    public void undoPaint() {
        com.fitplus.AppDelegate.LogT("undoPaint = " + paths.size() + ", " + undonePaths.size() + " == " + pathsBitmap.size() + ", " + undoneBitmap.size());
        if (paths.size() > 1 && pathsBitmap.size() > 1) {
            paths.remove(paths.size() - 1)/*)*/;
            pathsBitmap.remove(pathsBitmap.size() - 1)/*)*/;
            circlePath.reset();
            mPath.reset();
            if (pathsBitmap.size() == 1) {
                mBitmap = getBitmap();
            } else {
                mBitmap = pathsBitmap.get(pathsBitmap.size() - 1);
            }
            mCanvas = new Canvas(mBitmap);
            invalidate();
        }
    }

    public void redoPaint() {
        com.fitplus.AppDelegate.LogT("redoPaint = " + paths.size() + ", " + undonePaths.size() + " == " + pathsBitmap.size() + ", " + undoneBitmap.size());
        if (undonePaths.size() > 0 && undoneBitmap.size() > 0) {
            paths.add(undonePaths.remove(undonePaths.size() - 1));
            pathsBitmap.add(undoneBitmap.remove(undoneBitmap.size() - 1));
            invalidate();
        }
    }

    public void setPaintColor(int color) {
        mPaint.setColor(color);
    }

    int width = 0, height = 0;

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width = w;
        height = h;
        if (PreviewActivity.file != null) {
            Bitmap mainBitmap = VideoRecordingPreLollipopActivity.decodeFile(PreviewActivity.file);
            com.fitplus.AppDelegate.LogT("bitmap width => " + mainBitmap.getWidth() + ", " + mainBitmap.getHeight());
            mainBitmap = Bitmap.createScaledBitmap(mainBitmap, w, h, true);
            mBitmap = mainBitmap.copy(Bitmap.Config.ARGB_8888, true);
//            mBitmap = Bitmap.createBitmap(BitmapFactory.decodeFile(PreviewActivity.file.getAbsolutePath())).copy(Bitmap.Config.ARGB_8888, true);
        } else {
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        }
        pathsBitmap.add(mBitmap.copy(Bitmap.Config.ARGB_8888, true));
        mCanvas = new Canvas(mBitmap);
    }

    int count = 0;

    @Override
    protected void onDraw(Canvas canvas) {
        com.fitplus.AppDelegate.LogT("onDraw => " + paths.size() + " " + pathsBitmap.size());
//        canvas.drawBitmap(mBitmap, 0, 0, null);
        if (pathsBitmap.size() > 0)
            canvas.drawBitmap(pathsBitmap.get(pathsBitmap.size() - 1), 0, 0, null);
        for (Path p : paths) {
            canvas.drawPath(p, mPaint);
        }
//        canvas.drawPath(mPath, mPaint);
        canvas.drawPath(circlePath, circlePaint);
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
        circlePath.reset();
        circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
    }

    public Bitmap getBitmap() {
        Bitmap bitmap;
        if (PreviewActivity.file != null) {
            Bitmap mainBitmap = VideoRecordingPreLollipopActivity.decodeFile(PreviewActivity.file);
            com.fitplus.AppDelegate.LogT("bitmap width => " + mainBitmap.getWidth() + ", " + mainBitmap.getHeight());
            mainBitmap = Bitmap.createScaledBitmap(mainBitmap, width, height, true);
            bitmap = mainBitmap.copy(Bitmap.Config.ARGB_8888, true);
        } else {
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        }
        return bitmap;
    }

    private void touch_up() {
        try {
            mPath.lineTo(mX, mY);
            // commit the path to our offscreen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw

            circlePath.reset();
            mPath.reset();

            mPath = new Path();
            paths.add(mPath);

            pathsBitmap.add(mBitmap.copy(Bitmap.Config.ARGB_8888, true));
        } catch (Exception e) {
            com.fitplus.AppDelegate.LogE(e);
        } catch (OutOfMemoryError e) {
            com.fitplus.AppDelegate.LogE(e);
            for (int i = 0; i < 10; i++) {
                if (paths.size() > 0)
                    paths.remove(0);
            }
        }
        com.fitplus.AppDelegate.LogT("touch_up => pathsBitmap size = " + pathsBitmap.size());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        com.fitplus.AppDelegate.LogT("onTouchEvent called");
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                com.fitplus.AppDelegate.LogT("onTouchEvent touch_start called");
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                com.fitplus.AppDelegate.LogT("onTouchEvent touch_move called");
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                com.fitplus.AppDelegate.LogT("onTouchEvent touch_up called");
                invalidate();
                break;
        }
        return true;
    }
}
