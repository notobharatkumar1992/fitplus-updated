package com.videodemo;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import sardar.CompressListener;
import sardar.Compressor;
import sardar.InitListener;
import utils.Prefs;
import utils.VerticalSlideColorPicker;

/**
 * Created by Bharat on 08/15/2016.
 */
public class PreviewActivity extends FragmentActivity implements View.OnClickListener, OnReciveServerResponse, NumberPicker.OnValueChangeListener/*, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener*/ {

    private ImageView img_captured, img_dustbin, img_text, img_edit, img_icon, img_download, img_attach, img_send, img_undo;

    private int type = 0;
    private String filePath = "";
    public static File file;

    private VideoView videoView;

    private boolean fileDelete = true;
    private boolean fileSaved = false;
    private boolean commandValidationFailedFlag = false;
    public static final int FILE_TYPE_IMAGE = 0, FILE_TYPE_VIDEO = 1;

    public EditText et_view;
    private LinearLayout ll_view;
    private RelativeLayout rl_view;

    public Handler mHandler;
    private Dialog dialog;
    private int time = 5;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private TextView txt_time;

    public Compressor com;
    String cmd;
    public String outPutPath = "";

    private VerticalSlideColorPicker color_picker;
    public int selectedColor = -1;
    public View view_layer;
    private ShareDialog shareDialog;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.image_preview_activity);
        facebookSDKInitialize();
        type = getIntent().getExtras().getInt("from");
        filePath = getIntent().getExtras().getString("file");

        file = new File(filePath);
        initView();
        setValues();
        setHandler();
    }


    @Override
    public void onBackPressed() {

        deleteFile(file);
        finish();
    }

    private void deleteFile(File file) {
        try {
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(PreviewActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(PreviewActivity.this);
                }
            }
        };
    }

    Fragment mFragment = new MainFragment();

    private void setValues() {
        if (type == FILE_TYPE_IMAGE) {
            showFragment(this, mFragment);
//            img_captured.setVisibility(View.VISIBLE);
//            img_captured.setImageBitmap(VideoRecordingPreLollipopActivity.decodeFile(file));
            videoView.setVisibility(View.GONE);
        } else {
            img_captured.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
            videoView.setMediaController(new MediaController(this));
            videoView.setVideoURI(Uri.fromFile(file));
            videoView.requestFocus();
            videoView.start();
        }
    }

    public static void showFragment(FragmentActivity mContext, Fragment mFragment) {
        try {
            FragmentTransaction mFragmentTransaction = mContext
                    .getSupportFragmentManager()
                    .beginTransaction();
            mFragmentTransaction.replace(R.id.main_content, mFragment)
                    .addToBackStack(null).commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap mark(Bitmap src, String watermark, Point location, int color, int alpha, int size, boolean underline) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAlpha(alpha);
        paint.setTextSize(size);
        paint.setAntiAlias(true);
        paint.setUnderlineText(underline);
        canvas.drawText(watermark, location.x, location.y, paint);
        return result;
    }

    private int _xDelta;
    private int _yDelta;

    private void initView() {
        // outputPath=VideoRecordingLollipopActivity.getVideoFilePath(this);
        img_captured = (ImageView) findViewById(R.id.img_captured);
        videoView = (VideoView) findViewById(R.id.videoView);
        txt_time = (TextView) findViewById(R.id.txt_time);
        img_dustbin = (ImageView) findViewById(R.id.img_dustbin);
        img_dustbin.setOnClickListener(this);
        img_text = (ImageView) findViewById(R.id.img_text);
        img_text.setOnClickListener(this);
        img_edit = (ImageView) findViewById(R.id.img_edit);
        img_edit.setOnClickListener(this);
        img_edit.setSelected(false);
        img_undo = (ImageView) findViewById(R.id.img_undo);
        img_undo.setOnClickListener(this);
        img_icon = (ImageView) findViewById(R.id.img_icon);
        img_icon.setOnClickListener(this);
        img_download = (ImageView) findViewById(R.id.img_download);
        img_download.setOnClickListener(this);
        img_attach = (ImageView) findViewById(R.id.img_attach);
        img_attach.setOnClickListener(this);
        img_send = (ImageView) findViewById(R.id.img_send);
        img_send.setOnClickListener(this);
        et_view = (EditText) findViewById(R.id.et_view);
        rl_view = (RelativeLayout) findViewById(R.id.rl_view);
        if (type == FILE_TYPE_IMAGE) {
            rl_view.setVisibility(View.VISIBLE);
        } else {
            rl_view.setVisibility(View.GONE);
        }
        txt_time.setText(String.valueOf(new Prefs(this).getIntValue(Tags.time, 0)));
        if (txt_time.getText().toString().equalsIgnoreCase("0")) {
            txt_time.setText("5");
        }

        view_layer = (View) findViewById(R.id.view_layer);
        view_layer.setVisibility(View.VISIBLE);
        color_picker = (VerticalSlideColorPicker) findViewById(R.id.color_picker);
        color_picker.setVisibility(View.INVISIBLE);
        color_picker.setOnColorChangeListener(new VerticalSlideColorPicker.OnColorChangeListener() {
            @Override
            public void onColorChange(int selectedColor) {
                PreviewActivity.this.selectedColor = selectedColor;
                MainFragment.setPaintColor(selectedColor);
                AppDelegate.LogT("selectedColor = " + selectedColor);
            }
        });

        ll_view = (LinearLayout) findViewById(R.id.ll_view);
        ll_view.setVisibility(View.GONE);
        ll_view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                final int Y = (int) event.getRawY();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        _xDelta = X - lParams.leftMargin;
                        _yDelta = Y - lParams.topMargin;
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        int left, top, right, bottom;

                        left = X - _xDelta;
                        top = Y - _yDelta;
                        right = -250;
                        bottom = -250;
                        layoutParams.leftMargin = 0;
                        layoutParams.topMargin = top;
                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = bottom;
                        Log.d("test", "margin => " + layoutParams.leftMargin + ", " + layoutParams.topMargin + ", " + layoutParams.rightMargin + ", " + layoutParams.bottomMargin);
                        view.setLayoutParams(layoutParams);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_dustbin:
                if (img_edit.isSelected()) {
                    img_dustbin.setImageResource(R.drawable.dustbin_white);
                    img_edit.setSelected(false);
                    color_picker.setVisibility(View.INVISIBLE);
                    view_layer.setVisibility(View.VISIBLE);
                    img_undo.setVisibility(View.INVISIBLE);
                    img_text.setVisibility(View.VISIBLE);
                    ll_view.setVisibility(View.GONE);

                } else {
                    try {
                        deleteFile(file.getAbsolutePath());
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    finish();
                }
                break;

            case R.id.img_text:
                ll_view.setVisibility(ll_view.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                break;

            case R.id.img_undo:
                if (MainFragment.mv != null) {
                    MainFragment.mv.undoPaint();
                    MainFragment.mv.invalidate();
                }
                break;

            case R.id.img_edit:
                img_edit.setSelected(!img_edit.isSelected());
                color_picker.setVisibility(img_edit.isSelected() ? View.VISIBLE : View.INVISIBLE);
                view_layer.setVisibility(img_edit.isSelected() ? View.GONE : View.VISIBLE);
                img_undo.setVisibility(img_edit.isSelected() ? View.VISIBLE : View.INVISIBLE);
                img_text.setVisibility(img_edit.isSelected() ? View.GONE : View.VISIBLE);

                img_dustbin.setImageResource(img_edit.isSelected() ? R.drawable.close_white : R.drawable.dustbin_white);
                ll_view.setVisibility(View.GONE);
                if (view_layer.getVisibility() == View.GONE) {
                    MainFragment.setPaintColor(selectedColor > 0 ? selectedColor : Color.RED);
                }
                break;

            case R.id.img_icon:
//                MainFragment.mv.redoPaint();
//                MainFragment.mv.invalidate();
                showtimeAlert();
                break;

            case R.id.img_download:
//                MainFragment.mv.undoPaint();
//                MainFragment.mv.invalidate();
                mHandler.sendEmptyMessage(10);
                downloadImage();
                break;

            case R.id.img_attach:
                //  mHandler.sendEmptyMessage(10);
                new Thread(new Runnable() {
                    public void run() {
                        shareText = et_view.getText().toString();
                        AppDelegate.LogT("type => " + type + ", FILE_TYPE_IMAGE = " + FILE_TYPE_IMAGE + ", FILE_TYPE_VIDEO = " + FILE_TYPE_VIDEO);
                        if (type == FILE_TYPE_IMAGE) {
                            try {
                                if (shareText.length() == 0) {
                                    ll_view.setVisibility(View.GONE);
                                }
                                String newFile = AppDelegate.getNewFile(PreviewActivity.this);
                                storeImage(newFile, overlay(MainFragment.getPaintImage(), getBitmapFromView(rl_view)));
                                System.gc();
//                                VideoRecordingPreLollipopActivity.decodeFileLower(new File(newFile));
                                getBitmapAndWriteFile(PreviewActivity.this, newFile).recycle();
                                filePath = newFile;
                            } catch (OutOfMemoryError e) {
                                AppDelegate.LogE(e);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                            shareImageVideo(filePath, type);
                        } else if (type == FILE_TYPE_VIDEO) {
                            outPutPath = AppDelegate.getNewVideoFile(PreviewActivity.this);

//                            cmd = "-i " + filePath + " -i " + filePath + " -filter_complex 'overlay=10:main_h-overlay_h-10' " + outPutPath;
//                            cmd = "-y -i " + filePath + " -strict -2 -vcodec libx264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x352 -aspect 4:3 " + outPutPath;
                            cmd = "-y -i " + filePath + " -strict -2 -vcodec libx264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x352 -aspect 4:3 " + outPutPath;
                            com = new Compressor(PreviewActivity.this);

                            com.loadBinary(new InitListener() {
                                @Override
                                public void onLoadSuccess() {
                                    com.execCommand(cmd, new CompressListener() {
                                        @Override
                                        public void onExecSuccess(String message) {
                                            Log.i("test success", message);

                                            shareImageVideo(outPutPath, type);
                                        }

                                        @Override
                                        public void onExecFail(String reason) {
                                            Log.i("test fail", reason);
                                            AppDelegate.showToast(PreviewActivity.this, "Video upload failed reason : " + reason);
                                            mHandler.sendEmptyMessage(11);
                                        }

                                        @Override
                                        public void onExecProgress(String message) {
                                            Log.i("test progress", message);
                                        }
                                    });
                                }

                                @Override
                                public void onLoadFail(String reason) {
                                    Log.i("test fail", reason);
                                    AppDelegate.showToast(PreviewActivity.this, "Video upload failed reason : " + reason);
                                }
                            });
                        }
                    }
                }).start();

                break;

            case R.id.img_send:
                // Send code
//                executeSend();
                mHandler.sendEmptyMessage(10);
                new Thread(new Runnable() {

                    public void run() {
                        shareText = et_view.getText().toString();
                        AppDelegate.LogT("type => " + type + ", FILE_TYPE_IMAGE = " + FILE_TYPE_IMAGE + ", FILE_TYPE_VIDEO = " + FILE_TYPE_VIDEO);
                        if (type == FILE_TYPE_IMAGE) {
                            try {
                                if (shareText.length() == 0) {
                                    ll_view.setVisibility(View.GONE);
                                }
                                String newFile = AppDelegate.getNewFile(PreviewActivity.this);
                                storeImage(newFile, overlay(MainFragment.getPaintImage(), getBitmapFromView(rl_view)));
                                System.gc();
//                                VideoRecordingPreLollipopActivity.decodeFileLower(new File(newFile));
                                getBitmapAndWriteFile(PreviewActivity.this, newFile).recycle();
                                filePath = newFile;
                            } catch (OutOfMemoryError e) {
                                AppDelegate.LogE(e);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                            executeSend(filePath);
                        } else if (type == FILE_TYPE_VIDEO) {
                            outPutPath = AppDelegate.getNewVideoFile(PreviewActivity.this);

//                            cmd = "-i " + filePath + " -i " + filePath + " -filter_complex 'overlay=10:main_h-overlay_h-10' " + outPutPath;
//                            cmd = "-y -i " + filePath + " -strict -2 -vcodec libx264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x352 -aspect 4:3 " + outPutPath;
                            cmd = "-y -i " + filePath + " -strict -2 -vcodec libx264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x352 -aspect 4:3 " + outPutPath;
                            com = new Compressor(PreviewActivity.this);

                            com.loadBinary(new InitListener() {
                                @Override
                                public void onLoadSuccess() {
                                    com.execCommand(cmd, new CompressListener() {
                                        @Override
                                        public void onExecSuccess(String message) {
                                            Log.i("test success", message);
                                            executeSend(outPutPath);
                                        }

                                        @Override
                                        public void onExecFail(String reason) {
                                            Log.i("test fail", reason);
                                            AppDelegate.showToast(PreviewActivity.this, "Video upload failed reason : " + reason);
                                            mHandler.sendEmptyMessage(11);
                                        }

                                        @Override
                                        public void onExecProgress(String message) {
                                            Log.i("test progress", message);
                                        }
                                    });
                                }

                                @Override
                                public void onLoadFail(String reason) {
                                    Log.i("test fail", reason);
                                    AppDelegate.showToast(PreviewActivity.this, "Video upload failed reason : " + reason);
                                }
                            });
                        }
                    }
                }).start();
                break;
        }
    }

    public void shareImageVideo(String filePath, int type) {
        switch (type) {
            case FILE_TYPE_IMAGE: {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/jpeg");
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));
                startActivity(Intent.createChooser(share, "Share Image"));
            }
            break;
            case FILE_TYPE_VIDEO: {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("video/*");
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));
                startActivity(Intent.createChooser(share, "Share Video..."));
            }
            break;
        }
    }

    private void showtimeAlert() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.number_picker);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView b1 = (TextView) dialog.findViewById(R.id.no);
        TextView b2 = (TextView) dialog.findViewById(R.id.yes);
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberPicker1);
        np.setMaxValue(10);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // tv.setText(String.valueOf(np.getValue()));
                time = np.getValue();
                dialog.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public String newFile = "";

    private void downloadImage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!fileSaved) {
                    if (type == FILE_TYPE_IMAGE) {
                        try {
                            shareText = et_view.getText().toString();
                            if (shareText.length() == 0) {
                                ll_view.setVisibility(View.GONE);
                            }
                            newFile = getNewImageFilePath();
                            storeImage(newFile, overlay(MainFragment.getPaintImage(), getBitmapFromView(rl_view)));
                            System.gc();
                            getBitmapAndWriteFile(PreviewActivity.this, newFile).recycle();
                        } catch (OutOfMemoryError e) {
                            AppDelegate.LogE(e);
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }

                        fileSaved = true;
                    } else {
                        fileSaved = true;
                        newFile = getVideoFilePath();
                        try {
                            copyFile(new File(filePath), new File(newFile));
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                    }
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(PreviewActivity.this, (type == FILE_TYPE_IMAGE ? "Image" : "Video") + " saved at location : " + newFile, Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(PreviewActivity.this, (type == FILE_TYPE_IMAGE ? "Image" : "Video") + " already saved.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                mHandler.sendEmptyMessage(11);
            }
        }).start();
    }

    public String getNewImageFilePath() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "fitplus/SendFiles");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".jpg");
        AppDelegate.LogT("new File created: getNewImageFilePath => " + uriSting);
        return uriSting;
    }

    public String getVideoFilePath() {
        File file1 = new File(Environment.getExternalStorageDirectory()
                .getPath(), "fitplus/SendFiles");
        if (!file1.exists()) {
            file1.mkdirs();
        }
        String uriSting = System.currentTimeMillis() + ".mp4";
        File file = null;
        try {
            file = new File(file1.getAbsolutePath() + "/" + uriSting);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        Log.d("test", "getVideoFilename => " + file.getAbsolutePath());
        return file.getAbsolutePath();
    }


    /**
     * copies content from source file to destination file
     *
     * @param sourceFile
     * @param destFile
     * @throws IOException
     */
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
    }

    public static void storeImage(String filePath, Bitmap image) {
        File pictureFile = new File(filePath);
        if (pictureFile == null) {
            Log.d("test", "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
            Log.d("test", "storeImage => File saved successfully : " + filePath);
        } catch (FileNotFoundException e) {
            Log.d("test", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("test", "Error accessing file: " + e.getMessage());
        }
    }

    public static Bitmap getCompressedBitmap(Bitmap original) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        original.compress(Bitmap.CompressFormat.PNG, 0, out);
        return BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
    }

    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        int width, height = 0;
        if (bmp1.getWidth() > bmp1.getWidth()) {
            width = bmp1.getWidth();
            height = bmp1.getHeight();
        } else {
            width = bmp2.getWidth();
            height = bmp2.getHeight();
        }
        Bitmap cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas comboImage = new Canvas(cs);
        Rect dest1 = new Rect(0, 0, width, height); // left,top,right,bottom
        comboImage.drawBitmap(bmp1, null, dest1, null);
//        Rect dest2 = new Rect(0, height / 2, width, height); // left,top,right,bottom
        comboImage.drawBitmap(bmp2, null, dest1, null);
        AppDelegate.LogT("overlay called return result");
        return cs;
    }

    public static Bitmap getBitmapFromFile(String photoPath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
        Log.d("test", "getBitmapFromFile called and return result");
        return bitmap;
    }

//    private Bitmap decodeFile(File f) {
//        try {
//            // Decode image size
//            BitmapFactory.Options o = new BitmapFactory.Options();
//            o.inJustDecodeBounds = true;
//            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
//
//            // The new size we want to scale to
//            final int REQUIRED_SIZE = 70;
//
//            // Find the correct scale value. It should be the power of 2.
//            int scale = 1;
//            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
//                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
//                scale *= 2;
//            }
//            // Decode with inSampleSize
//            BitmapFactory.Options o2 = new BitmapFactory.Options();
//            o2.inSampleSize = scale;
//            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
//        } catch (FileNotFoundException e) {
//        }
//        return null;
//    }

    public static Bitmap getBitmapAndWriteFile(Context mContext, String path) {
        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 500000; // 0.5MP
            in = mContext.getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
                scale++;
            }
            Bitmap b = null;
            in = mContext.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);
                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();

                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();
            Log.d("test", "getBitmapAndWriteFile => File saved successfully : ");

            storeImage(path, b);
            return b;
        } catch (IOException e) {
            AppDelegate.LogE(e);
            return null;
        }
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        Log.d("test", "getBitmapFromView called and return result");
        return returnedBitmap;
    }

    private void executeSend(String uploadFile) {
        AppDelegate.LogT("Execute Send Called");
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.LogT("file path => " + new File(uploadFile));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.time, time, ServerRequestConstants.Key_PostintValue);
                if (type == FILE_TYPE_IMAGE) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, uploadFile, ServerRequestConstants.Key_PostFileValue);
                } else if (type == FILE_TYPE_VIDEO) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, uploadFile, ServerRequestConstants.Key_PostFileValue);
                }
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.comment, shareText + "");

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(this).getStringValue(Tags.TAG_LONG, ""));

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.FIT_DAY_UPLOAD,
                        mPostArrayList, null, true);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            //  AppDelegate.showToast(this, "Please try again.");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FIT_DAY_UPLOAD)) {
            parseFitDay(result);
        }
    }

    private void parseFitDay(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                deleteFile(file);
                finish();
//                AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "");
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                // finish();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            // AppDelegate.ShowDialog(this, "Please try after some time.", "");
//          AppDelegate.ShowDialog(this, "Please try after some time.", "Alert!!!");
        }
    }

    private String shareText = "";

    private void showTextAlert() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_text_share, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.et_text);
        userInput.setText(shareText);
        userInput.setSelection(shareText.length());
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                shareText = userInput.getText().toString();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        try {
            if (fileDelete) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Log.i("value is", "" + newVal);
        time = newVal;
        txt_time.setText(String.valueOf(newVal));
        new Prefs(this).putIntValue(Tags.time, newVal);
    }

    /*facebook i9nitialize and sharing*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("LoginActivity onActivityResult called");
        if (callbackManager != null) {
            AppDelegate.LogT("callbackManager onActivityResult called");
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
    }



    public void openFacebook(final String image, final String comment) {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        shareFacebook(image, comment);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook(image, comment);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(PreviewActivity.this);
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(image, comment);
                                }
                            }
                        }
                    }
                });
    }

    private void shareFacebook(final String image, final String comment) {
        AppDelegate.LogT("shareFacebook => +" + ShareDialog.canShow(ShareLinkContent.class));
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Fit+")
//                    .setImageUrl(imageURI)
                    .setImageUrl(Uri.parse(image))
                    .setContentDescription(comment)
                    .setContentUrl(Uri.parse(image))
                    .build();
            shareDialog.show(linkContent);
//            getFragmentManager().popBackStack();

        }
    }


}
