package com.videodemo;

import android.app.Application;
import android.support.multidex.MultiDex;

/**
 * Created by Bharat on 08/16/2016.
 */
public class AppDelegate extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            MultiDex.install(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
