package com.camerademo;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.camerademo.util.ImageUtils;
import com.fitplus.AppDelegate;
import com.fitplus.R;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

public class MainActivity extends AppCompatActivity implements OnReciveServerResponse {

    private EditText et_comment;

    private Prefs prefs;
    private UserDataModel userDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_image);
        prefs = new Prefs(this);
        userDataModel = prefs.getUserdata();

        /*final ImageView imageView = (ImageView) findViewById(R.id.imageView);
//        imageView.setImageURI(getIntent().getData());
        ImageUtils.asyncLoadImage(this, getIntent().getData(), new ImageUtils.LoadImageCallback() {
            @Override
            public void callback(Bitmap result) {
                try {
                    result = CameraActivity.rotateImageIfRequired(result, MainActivity.this, getIntent().getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imageView.setImageBitmap(result);
            }
        });
        et_comment = (EditText) findViewById(R.id.et_comment);

        findViewById(R.id.txt_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.txt_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CameraActivity.mActivity != null) {
                    CameraActivity.mActivity.finish();
                }
                executeLogin(getIntent().getData());
            }
        });*/
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void executeLogin(Uri uri) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, userDataModel.userId);
//                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.file, getRealPathFromURI(MainActivity.this, uri), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.LogT("file path => " + new File(uri.getPath()).getAbsolutePath());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, new File(uri.getPath()).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.comment, et_comment.getText().toString());
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.FIT_DAY_UPLOAD,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FIT_DAY_UPLOAD)) {
            parseFitDay(result);
        }
    }

    private void parseFitDay(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("0")) {
                AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "");
            } else {
                AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message),"");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try after some time.","");
//          AppDelegate.ShowDialog(this, "Please try after some time.", "Alert!!!");
        }
    }
}
