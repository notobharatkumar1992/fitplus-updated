package com.fitplus;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CustomTimePickerDialog;
import butterknife.ButterKnife;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.ChatModel;
import model.PostAysnc_Model;
import model.ScheduleModel;
import model.TeamListModel;
import model.UserDataModel;
import utils.Prefs;
import utils.decorators.CurrentDateDecorator;
import utils.decorators.EventDecorator;
import utils.decorators.MySelectorDecorator;
import utils.decorators.OneDayDecorator;

/**
 * Created by admin on 26-07-2016.
 */
public class SetLocationActivity extends AppCompatActivity implements OnDialogClickListener, OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse {
    RelativeLayout toolbar;
    TextView toolbar_title;
    ImageView tool_menu;
    RecyclerView team_list;
    private Bundle bundle;
    private CustomTimePickerDialog timePicker;
    private int selectfromhour, selectfromminute;
    private String timeset;
    private TextView time, location;
    private MaterialCalendarView date;
    private String setdate;
    private int team_id_type;
    private TextView month;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    public static double LATITUDE = 0.0, LONGITUDE = 0.0;
    public static String addressName = "";
    public static Handler mHandler;
    private SimpleDateFormat month_date;
    private OneDayDecorator oneDayDecorator = new OneDayDecorator(this);

    @Nullable
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_location);
        ButterKnife.bind(this);
        bundle = getIntent().getExtras();
        team_id_type = bundle.getInt(Tags.Find_Coach);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(SetLocationActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(SetLocationActivity.this);
                } else if (msg.what == 12) {
                    updateLocation();
                }
            }
        };
    }

    private void updateLocation() {
        if (AppDelegate.isValidString(addressName)) {
            location.setText(addressName + "");
        }
    }


    private void initView() {
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.title);
        tool_menu = (ImageView) findViewById(R.id.back);
        tool_menu.setOnClickListener(this);
        time = (TextView) findViewById(R.id.time);
        location = (TextView) findViewById(R.id.location);
        location.setOnClickListener(this);
        date = (MaterialCalendarView) findViewById(R.id.date);
        month = (TextView) findViewById(R.id.month);
        date=(MaterialCalendarView)findViewById(R.id.date);
        date.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                Date calender = date.getDate();
                setdate = new SimpleDateFormat("yyyy-MM-dd").format(calender).toString();

                Log.e("Selected date", setdate + "");
                widget.invalidateDecorators();
                set_time();
            }
        });
        Calendar calender = Calendar.getInstance();
        Date currentLocalTime = calender.getTime();
        DateFormat dates = new SimpleDateFormat("HH:mm");
        String localTime = dates.format(currentLocalTime);
        time.setText(localTime+"");
        date.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        Calendar instance = Calendar.getInstance();
        CalendarDay day = CalendarDay.from(instance.getTime());
        ArrayList<CalendarDay> calenderdates = new ArrayList<>();
        calenderdates.add(day);
        new ApiSimulator(calenderdates).execute();
        date.setTopbarVisible(true);
        time=(TextView)findViewById(R.id.time);
        Calendar cal = Calendar.getInstance();
        month_date = new SimpleDateFormat("MMMM yyyy");
        String month_name = month_date.format(cal.getTime());
        month.setText("" + month_name);
        date.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                Calendar calendar=Calendar.getInstance();
                calendar.set(Calendar.MONTH,date.getMonth() );
                calendar.set(Calendar.YEAR,date.getYear() );
                month_date = new SimpleDateFormat("MMMM yyyy");
                String month_name = month_date.format(calendar.getTime());
                month.setText(month_name+"");
            }
        });
        date.state().edit().setMinimumDate(Calendar.getInstance().getTime())
                //.setMaximumDate(instance2.getTime())
                .commit();
        date.addDecorators(
                new MySelectorDecorator(this),
                //new HighlightWeekendsDecorator(),
                oneDayDecorator
        );
        time.setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.time:
                AppDelegate.LogT("time clicked");
                set_time();
                break;
            case R.id.location:
                AppDelegate.LogT("time clicked");
                Intent intent = new Intent(SetLocationActivity.this, SelectLocationActivity.class);
                startActivity(intent);
                break;
            case R.id.done:
                if (AppDelegate.isValidString(timeset) && AppDelegate.isValidString(setdate) && (LATITUDE != 0.0 || LONGITUDE != 0.0)) {
                    AppDelegate.showDialog_twoButtons(this, getResources().getString(R.string.find_a_coach), Tags.schedule, this);
                } else if (!AppDelegate.isValidString(setdate)) {
                    AppDelegate.showToast(this, "Please select date.");
                } else if (!AppDelegate.isValidString(timeset)) {
                    AppDelegate.showToast(this, "Please select time.");
                } else {
                    AppDelegate.showToast(this, "Please select Location.");
                }
                break;
        }
    }
    private void execute_Schedule() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, LATITUDE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, LONGITUDE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.team_id, team_id_type, ServerRequestConstants.Key_PostintValue);
               /* String timeValue = new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("hh:mm aa").parse(timeset));
                AppDelegate.LogT("timeValue => " + timeValue);*/
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.dateTime, setdate + " " + timeset);
                mPostAsyncObj = new PostAsync(this, SetLocationActivity.this, ServerRequestConstants.NEWSCHEDULE,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.try_again), "Alert!!!");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
        }
    }
    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {
        ArrayList<CalendarDay> calenderdates;

        public ApiSimulator(ArrayList<CalendarDay> calenderdates) {
            this.calenderdates = calenderdates;
        }

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return calenderdates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            if (isFinishing()) {
                return;
            }
            Log.v("test==", calendarDays + "");
            date.addDecorator(new CurrentDateDecorator(SetLocationActivity.this, calendarDays));
        }
    }
    private void set_time() {
        timePicker = new CustomTimePickerDialog(this,  new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar calender = Calendar.getInstance();
                calender.set(Calendar.HOUR, selectedHour);
                calender.set(Calendar.MINUTE, selectedMinute);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                String test = sdf.format(calender.getTime());
                selectfromhour = selectedHour;
                selectfromminute = selectedMinute;
                time.setText(test+"");
                timeset =test;
            }
        }, selectfromhour, selectfromminute, false);//Yes 24 hour time
        timePicker.show();

        AppDelegate.LogT("date=>..." + timeset);
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.NEWSCHEDULE)) {
            parsenewSchedule(result);
        }
    }

    private void parsenewSchedule(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                JSONObject host_user = jsonObject.getJSONObject(Tags.response);
                ScheduleModel userDataModel = new ScheduleModel();
                userDataModel.team_id = host_user.getString(Tags.team_id);
                userDataModel.user1_action = host_user.getInt(Tags.user1_action);
                userDataModel.user1_id = host_user.getInt(Tags.user1_id);
                userDataModel.status = host_user.getInt(Tags.status);
                userDataModel.user2_id = host_user.getInt(Tags.user2_id);
                userDataModel.user3_id = host_user.getInt(Tags.user3_id);
                userDataModel.user1_date = host_user.getString(Tags.user1_date);
                userDataModel.training_date = host_user.getString(Tags.training_date);
                userDataModel.request_type = host_user.getString(Tags.request_type);
                userDataModel.user2_action = host_user.getInt(Tags.user2_action);
                userDataModel.user3_action = host_user.getInt(Tags.user3_action);
                userDataModel.id = host_user.getInt(Tags.id);
            }
            try {
                ChatModel chatModel = new ChatModel(team_id_type);
                if (CreateTeamActivity.onReciveSocketMessage != null) {
                    CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            finish();
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.schedule)) {
            execute_Schedule();
        }
    }
}
