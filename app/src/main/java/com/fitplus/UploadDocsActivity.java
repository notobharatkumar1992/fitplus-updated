package com.fitplus;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.UploadDocsAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.PostAysnc_Model;
import model.UploadDocsModel;
import utils.Prefs;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by admin on 26-07-2016.
 */
public class UploadDocsActivity extends AppCompatActivity implements OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private LinearLayout ll1, ll2, ll3;
    private String first, second, third;
    private RadioGroup options;
    private TextView questions, next, prev, question_number;
    private RadioButton selected_option, option1, option2, option3, option4;
    private Activity mActivity;
    private Bundle bundle = new Bundle();
    private int index = 0;
    private ArrayList<UploadDocsModel> uploadDocsModelArrayList = new ArrayList<>();
    private int id;
    private TextSwitcher textSwitcher;
    private String encoded_data;
    private int PICK_IMAGE_REQUEST = 1;
    ArrayList<File> bitmaps = new ArrayList<>();
    private String docFilePath;
    private Bitmap OriginalPhoto;
    //private DisplayImageOptions options;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private GridView imageGrid;
    private Dialog builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.upload_docs);
        mActivity = null;
        initView();

    }

    private void initView() {
        findViewById(R.id.browse).setOnClickListener(this);
        imageGrid = (GridView) findViewById(R.id.image_grid);
        findViewById(R.id.save).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.browse:
                showImageSelectorList();
                // select_files();
                break;
            case R.id.save:
                execute_saveFiles();
                break;
        }
    }

    private void execute_saveFiles() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                for (int i = 0; i < uploadDocsModelArrayList.size(); i++) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, "uploadedfile" + i, uploadDocsModelArrayList.get(i).getImage(), ServerRequestConstants.Key_PostFileValue);

                }
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.COACH_DOCUMENTS,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ListView modeList = new ListView(this);
        String[] stringArray = new String[]{"Camera", "Gallery", "Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AppDelegate.showProgressDialog(UploadDocsActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(UploadDocsActivity.this, "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AppDelegate.hideProgressDialog(UploadDocsActivity.this);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppDelegate.SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    imageUri = uri;
                    AppDelegate.LogT("uri=" + uri);
                    if (uri != null) {
                        // User had pick an image.
                        Cursor cursor =
                                getContentResolver()
                                        .query(uri,
                                                new String[]{MediaStore.Images.ImageColumns.DATA},
                                                null, null, null);
                        cursor.moveToFirst();
                        // Link to the image
                        final String imageFilePath = cursor.getString(0);
                        cursor.close();
                        int orientation = 0;
                        if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                            try {
                                ExifInterface ei = new ExifInterface(imageFilePath);
                                orientation = ei.getAttributeInt(
                                        TAG_ORIENTATION,
                                        ORIENTATION_NORMAL);
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            capturedFile = new File(imageFilePath);
                            OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            if (OriginalPhoto != null) {
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(this, OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        //uploadImg.setImageBitmap(OriginalPhoto);
                                        //   bitmaps.add(OriginalPhoto);
                                        UploadDocsModel uploadDocsModel = new UploadDocsModel();
                                        uploadDocsModel.setBitmap(OriginalPhoto);
                                        uploadDocsModel.setImage(capturedFile);
                                        uploadDocsModelArrayList.add(uploadDocsModel);
                                        setAdapter(uploadDocsModelArrayList);

                                    }
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(this, "Failed to deliver result.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                }
                break;
            case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);
                //Get our saved file into a bitmap object:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        int orientation = 0;
                        Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inJustDecodeBounds = true;
                        OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                        Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                        try {
                            ExifInterface ei = new ExifInterface(
                                    uri.getPath());

                            orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);

                            Uri rotateduri = getImageUri(this, OriginalPhoto);
                            if (rotateduri != null) {
                                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                    //  uploadImg.setImageBitmap(OriginalPhoto);
                                    UploadDocsModel uploadDocsModel = new UploadDocsModel();
                                    uploadDocsModel.setBitmap(OriginalPhoto);
                                    uploadDocsModel.setImage(capturedFile);
                                    uploadDocsModelArrayList.add(uploadDocsModel);
                                    setAdapter(uploadDocsModelArrayList);
                                }

                            } else {
                                AppDelegate.showToast(this, "Failure delivery result, please try again later.");
                            }

                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(this, "Failure delivery result, please try again later.");
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                break;
            case AppDelegate.PIC_CROP: {
                AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                try {
                    cropimageUri = data.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        if (OriginalPhoto != null) {
                            OriginalPhoto.recycle();
                            OriginalPhoto = null;
                        }
                        OriginalPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), cropimageUri);
                        AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                        OriginalPhoto = Bitmap.createScaledBitmap(
                                OriginalPhoto, 240, 240, true);
                        AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
                        UploadDocsModel uploadDocsModel = new UploadDocsModel();
                        uploadDocsModel.setBitmap(OriginalPhoto);
                        uploadDocsModel.setImage(capturedFile);
                        uploadDocsModelArrayList.add(uploadDocsModel);
                        setAdapter(uploadDocsModelArrayList);
                    /*    uploadImg.setImageBitmap(OriginalPhoto);*/
                        capturedFile = new File(getNewFile());
                        FileOutputStream fOut = null;
                        try {
                            fOut = new FileOutputStream(capturedFile);
                        } catch (FileNotFoundException e) {
                            AppDelegate.LogE(e);
                        }
                        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        try {
                            fOut.flush();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        // getActivity().getContentResolver().delete(cropimageUri, null, null);
                    } else {
                        AppDelegate.LogE("at onActivicTyResult failed to crop");
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    private void setAdapter(ArrayList<UploadDocsModel> bitmaps) {
        AppDelegate.LogT("upload docs" + bitmaps);
        UploadDocsAdapter adapter = new UploadDocsAdapter(this, bitmaps);
        imageGrid.setAdapter(adapter);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);

            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    //    private void select_files() {
//        Intent intent = new Intent();
//        intent.setType("*/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
//    }
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Uri fileuri = data.getData();
                docFilePath = getFileNameByUri(this, fileuri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                bitmaps.add(docFilePath);
                AppDelegate.LogT("imge Path"+"=>"+uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/
  /*  private String getFileNameByUri(Context context, Uri uri)
    {
        String filepath = "";//default fileName
        File file;
        if (uri.getScheme().toString().compareTo("content") == 0)
        {
            Cursor cursor = context.getContentResolver().query(uri, new String[] { android.provider.MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.ORIENTATION }, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String mImagePath = cursor.getString(column_index);
            cursor.close();
            filepath = mImagePath;

        }
        else
        if (uri.getScheme().compareTo("file") == 0)
        {
            try
            {
                file = new File(new URI(uri.toString()));
                if (file.exists())
                    filepath = file.getAbsolutePath();

            }
            catch (URISyntaxException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {
            filepath = uri.getPath();
        }
        return filepath;
    }*/
    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.COACH_DOCUMENTS)) {
            parse_documentsResponse(result);
        }
    }

    private void parse_documentsResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.ShowDialogID(this,jsonObject.getString(Tags.message), "Alert", Tags.ok, UploadDocsActivity.this);
                AppDelegate.showToast(UploadDocsActivity.this, jsonObject.getString(Tags.message));
                finish();
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.showToast(UploadDocsActivity.this, jsonObject.getString(Tags.message));
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.cancel, UploadDocsActivity.this);
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showToast(UploadDocsActivity.this, jsonObject.getString(Tags.message));
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.cancel, UploadDocsActivity.this);
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            finish();
        }
    }
}
