package com.fitplus;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.BecomeCoachModel;
import model.PostAysnc_Model;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class SurveyFormActivity extends AppCompatActivity implements OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private LinearLayout ll1, ll2, ll3;
    private String first, second, third;
    private RadioGroup options;
    private TextView questions, next, prev, question_number;
    private TextView selected_option, option1, option2, option3, option4;
    private Activity mActivity;
    private Bundle bundle = new Bundle();
    private int index = 0;
    private ArrayList<BecomeCoachModel> become_coach;
    private int id;
    private TextSwitcher textSwitcher;
    private String encoded_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.survay_form_demo);
        bundle = getIntent().getExtras();
        become_coach = bundle.getParcelableArrayList(Tags.parcel_becomeCoach);
        mActivity = null;
        initView();
    }

    private void initView() {
        questions = (TextView) findViewById(R.id.question);
        next = (TextView) findViewById(R.id.next);
        next.setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        question_number = (TextView) findViewById(R.id.question_number);
        option1 = (TextView) findViewById(R.id.rd_option1);
        option2 = (TextView) findViewById(R.id.rd_option2);
        option3 = (TextView) findViewById(R.id.rd_option3);
        option4 = (TextView) findViewById(R.id.rd_option4);
        option1.setOnClickListener(this);
        option2.setOnClickListener(this);
        option3.setOnClickListener(this);
        option4.setOnClickListener(this);
        questions.setText(become_coach.get(0).question);
        option1.setText(become_coach.get(0).option1);
        option2.setText(become_coach.get(0).option2);
        if (AppDelegate.isValidString((become_coach.get(0).option3))) {
            option3.setText(become_coach.get(0).option3);
            option3.setVisibility(View.VISIBLE);
        } else {
            option3.setVisibility(View.GONE);
        }
        if (AppDelegate.isValidString((become_coach.get(0).option4))) {
            option4.setText(become_coach.get(0).option4);
            option4.setVisibility(View.VISIBLE);
        } else {
            option4.setVisibility(View.GONE);
        }
        question_number.setText(index + 1 + "/" + become_coach.size());
    }

    @Override
    public void onBackPressed() {
        if (index < 1) {
        } else {
            executePrev();
        }
    }

    private void fillchooseArray(int index) {
        questions.setText(become_coach.get(index).question);
        option1.setText(become_coach.get(index).option1);
        option2.setText(become_coach.get(index).option2);
        if (AppDelegate.isValidString(become_coach.get(index).option3)) {
            option3.setText(become_coach.get(index).option3);
            option3.setVisibility(View.VISIBLE);
        } else {
            option3.setVisibility(View.GONE);
        }
        if (AppDelegate.isValidString(become_coach.get(index).option4)) {
            option4.setText(become_coach.get(index).option4);
            option4.setVisibility(View.VISIBLE);
        } else {
            option4.setVisibility(View.GONE);
        }
        if (become_coach.get(index).answer_status == 1) {
            AppDelegate.LogT("selcted Number =>" + become_coach.get(index).answer_number);
            setselected(become_coach.get(index).answer_number);
        } else {
            setselected(5);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rd_option1:
                setselected(1);
                break;
            case R.id.rd_option2:
                setselected(2);
                break;
            case R.id.rd_option3:
                setselected(3);
                break;
            case R.id.rd_option4:
                setselected(4);
                break;
            case R.id.back:
                if (index < 1) {
                } else {
                    executePrev();
                }
                break;
            case R.id.next:
                if (!option1.isSelected() && !option2.isSelected() && !option3.isSelected() && !option4.isSelected()) {
                    executeNext();
                } else {
                    checkOptions(find_selected());
                    become_coach.get(index).answer_status = 1;
                    AppDelegate.LogT("save answe list on next  =>" + become_coach.get(index).given_answer + "  index => " + index);
                    executeNext();
                }
                break;
        }
    }

    private int find_selected() {
        if (option1.isSelected()) {
            AppDelegate.LogT(" find_selected option 1 selected");
            become_coach.get(index).answer_number = 1;
            become_coach.get(index).given_answer = option1.getText().toString().trim();
            return R.id.rd_option1;
        } else if ((option2.isSelected())) {
            AppDelegate.LogT("find_selected option 2 selected");
            become_coach.get(index).answer_number = 2;
            become_coach.get(index).given_answer = option2.getText().toString().trim();
            return R.id.rd_option2;
        } else if ((option3.isSelected())) {
            AppDelegate.LogT(" find_selectedoption 3 selected");
            become_coach.get(index).answer_number = 3;
            become_coach.get(index).given_answer = option3.getText().toString().trim();
            return R.id.rd_option3;
        } else if ((option4.isSelected())) {
            AppDelegate.LogT(" find_selected option 4 selected");
            become_coach.get(index).answer_number = 4;
            become_coach.get(index).given_answer = option4.getText().toString().trim();
            return R.id.rd_option3;
        }

        return 0;
    }

    private void checkOptions(int checkedRadioButtonId) {

        switch (checkedRadioButtonId) {
            case R.id.rd_option1:
                AppDelegate.LogT("option 1 selected");
                become_coach.get(index).answer_number = 1;
                become_coach.get(index).given_answer = option1.getText().toString().trim();
                break;
            case R.id.rd_option2:
                AppDelegate.LogT("option 2 selected");
                become_coach.get(index).answer_number = 2;
                become_coach.get(index).given_answer = option2.getText().toString().trim();
                break;
            case R.id.rd_option3:
                AppDelegate.LogT("option 3 selected");
                become_coach.get(index).answer_number = 3;
                become_coach.get(index).given_answer = option3.getText().toString().trim();
                break;
            case R.id.rd_option4:
                AppDelegate.LogT("option 4 selected");
                become_coach.get(index).answer_number = 4;
                become_coach.get(index).given_answer = option4.getText().toString().trim();
                break;
            case 5:

                break;
        }
    }

    private void executePrev() {
        AppDelegate.LogT("Index size on prev before=>" + index);
        index--;
        question_number.setText(index + 1 + "/" + become_coach.size());
        if (index < 0) {
            finish();
        } else if (index < become_coach.size()) {
            next.setText(getResources().getString(R.string.next));
            AppDelegate.LogT("Index size on prev after=>" + index);
            fillchooseArray(index);
        }
    }

    private void executeNext() {
        if (index <= become_coach.size()) {
            if (index < become_coach.size()) {
            }
            index++;
        }
        AppDelegate.LogT("index no.=>" + index);
        question_number.setText(index + 1 + "/" + become_coach.size());
        if (index == become_coach.size() - 1) {
            next.setText("DONE");
        }
        if (index >= become_coach.size()) {
            question_number.setText(become_coach.size() + "/" + become_coach.size());
            execute_saveAnswers();
        } else {
            fillchooseArray(index);
        }
    }

    private void execute_saveAnswers() {
        index = index - 1;
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < become_coach.size(); i++) {
                if (i == become_coach.size() - 1) {
                    sb.append(become_coach.get(i).id).append(Parameters.delimiter).append(become_coach.get(i).given_answer);
                } else {
                    sb.append(become_coach.get(i).id).append(Parameters.delimiter).append(become_coach.get(i).given_answer).append(Parameters.seperator);
                }
                encoded_data = sb.toString().trim();
                AppDelegate.LogT("encoded data is =>" + encoded_data);
            }
            encoded_data = sb.toString().trim();
            AppDelegate.LogT("final encoded data is =>" + encoded_data);
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.encodedData, encoded_data.trim());
                mPostAsyncObj = new PostAsync(this, SurveyFormActivity.this, ServerRequestConstants.SAVE_ANSWERS, mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.SAVE_ANSWERS)) {
            parseAnswers(result);
        }
    }

    void setselected(int value) {
        AppDelegate.LogT("set selected called =>" + value);
        option1.setSelected(false);
        option2.setSelected(false);
        option3.setSelected(false);
        option4.setSelected(false);
        switch (value) {
            case 1:
                option1.setSelected(true);
                break;
            case 2:
                option2.setSelected(true);
                break;
            case 3:
                option3.setSelected(true);
                break;
            case 4:
                option4.setSelected(true);
                break;
            case 5:
                option1.setSelected(false);
                option2.setSelected(false);
                option3.setSelected(false);
                option4.setSelected(false);
                break;
        }
    }

    private void parseAnswers(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, SurveyFormActivity.this);
                AppDelegate.showToast(SurveyFormActivity.this, jsonObject.getString(Tags.message));
                finish();
            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, SurveyFormActivity.this);
                    AppDelegate.showToast(SurveyFormActivity.this, jsonObject.getString(Tags.message));
                    finish();
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            finish();
        }
    }
}
