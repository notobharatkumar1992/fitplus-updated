package com.fitplus;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class InviteActivity extends AppCompatActivity implements OnClickListener {
    TextView invite_facebook, invite_contact, skip;
    Activity mActivity;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;
    private AlertDialog.Builder alertDialogBuilder;
    private String[] alertDialogItems = new String[]{};
    private boolean[] selectedTrueFalse = new boolean[]{};
    private List<String> ItemsIntoList;
    private List<String> nameIntoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.invite_friends);
        mActivity = null;
        initView();
    }

    private void initView() {
        invite_facebook = (TextView) findViewById(R.id.invite_facebook);
        invite_contact = (TextView) findViewById(R.id.invite_contact);
        skip = (TextView) findViewById(R.id.skip);
        invite_facebook.setOnClickListener(this);
        invite_contact.setOnClickListener(this);
        skip.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void openFacebook(final Activity mActivity, final Bundle mBundle) {
        FacebookSdk.sdkInitialize(mActivity);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        inviteFbFriends();
                        // shareFacebook(mActivity, mBundle);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook(mActivity, mBundle);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(mActivity);
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(mActivity, mBundle);
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("LoginActivity onActivityResult called");
        if (callbackManager != null) {
            AppDelegate.LogT("callbackManager onActivityResult called");
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void showContactAlertDialog(Activity mActivity) {
        alertDialogBuilder = new AlertDialog.Builder(mActivity);
        ItemsIntoList = fetchPhoneContacts(this);
        nameIntoList = fetchPhonename(this);
        AppDelegate.LogT("ItemsIntoList.size => " + ItemsIntoList.size());
        alertDialogItems = new String[nameIntoList.size()];
        AppDelegate.LogT("alertDialogItems.size => " + alertDialogItems);
        selectedTrueFalse = new boolean[ItemsIntoList.size()];
        for (int i = 0; i < ItemsIntoList.size(); i++) {
            alertDialogItems[i] = nameIntoList.get(i);
            selectedTrueFalse[i] = false;
        }
        alertDialogBuilder.setMultiChoiceItems(alertDialogItems, selectedTrueFalse, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                selectedTrueFalse[which] = isChecked;
            }
        });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle("Select contact");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int a = 0;
                while (a < selectedTrueFalse.length) {
                    boolean value = selectedTrueFalse[a];
                    if (value) {
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage("phoneno", null, new Prefs(InviteActivity.this).getInviteFriendsTest() + " https://www.google.com ", null, null);
                        Toast.makeText(getApplicationContext(), "SMS Sent!",
                                Toast.LENGTH_LONG).show();
                    }
                    a++;
                }
            }
        });

        alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();
    }

    public ArrayList<String> fetchPhoneContacts(Context mContext) {
        ArrayList<String> contactList = new ArrayList<>();
        ContentResolver cr = mContext.getContentResolver(); //Activity/Application android.content.Context
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactList.add(contactNumber);

                        break;
                    }
                    pCur.close();
                }
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    public ArrayList<String> fetchPhonename(Context mContext) {
        ArrayList<String> contactListname = new ArrayList<>();
        ContentResolver cr = mContext.getContentResolver(); //Activity/Application android.content.Context
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String contactname = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        contactListname.add(contactname);
                        AppDelegate.LogT(contactname + "");
                        break;
                    }
                    pCur.close();
                }
            } while (cursor.moveToNext());
        }
        return contactListname;
    }

    private void inviteFbFriends() {
        String appLinkUrl, previewImageUrl;
        appLinkUrl = "app url(create it from facebook)";
        previewImageUrl = "http://www.droidcular.com/wp-content/uploads/2015/11/Android-Security-Bug-Found-Hackers-Gain-System-Access.png";
        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl("http://www.google.com/")
                    .setPromotionDetails(new Prefs(this).getInviteFriendsTest(), " ")
                    .setPreviewImageUrl(previewImageUrl)
                    .build();

            AppInviteDialog.show(this, content);
        }
        AppDelegate.LogT("inviteFbFriends called");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.invite_facebook:
                final Bundle mBundle = new Bundle();
                mBundle.putString("title", "Invitation for app");
                mBundle.putString("description", "Download this app.");
                openFacebook(this, mBundle);
                break;
            case R.id.invite_contact:
                showContactAlertDialog(this);
                break;
            case R.id.skip:

                startActivity(new Intent(this, BeginnerQuestionActivity.class));
                finish();

                break;
        }
    }

}
