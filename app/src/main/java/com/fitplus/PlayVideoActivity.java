package com.fitplus;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.FitDayModel;
import model.PostAysnc_Model;
import utils.Prefs;

public class PlayVideoActivity extends AppCompatActivity implements OnReciveServerResponse, OnDialogClickListener, View.OnClickListener {

    Boolean show = false;
    private VideoView videoView;
    public static Activity activity;
    private String identifier;
    private String url;
    private Bundle bundle;
    FitDayModel fitday;
    private TextView comment;
    ImageView txt_report_abuse;
    private MediaController mMediaController;
    private int abuse=0;
    private Dialog builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        abuse=getIntent().getIntExtra(Tags.abuse,0);
        activity = this;
        bundle = getIntent().getExtras();
        fitday = bundle.getParcelable("FitDay");
        findIDS();
        if (fitday != null) {
            comment = (TextView) findViewById(R.id.comment);
            comment.setText(fitday.comment + "");
            playvdo(fitday.file_name);
        }
    }

    private void findIDS() {
        videoView = (VideoView) findViewById(R.id.video);
      /*  txt_report_abuse=(ImageView)findViewById(R.id.txt_report_abuse);
        txt_report_abuse.setOnClickListener(this);
        if(abuse==1){
            txt_report_abuse.setVisibility(View.VISIBLE);
            if(fitday.viewstatus==1){
                execute_viewStatus();
            }
        }else{
            txt_report_abuse.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activity != null) {
            activity = null;
        }
    }
    private void execute_viewStatus() {
        try {
            if (AppDelegate.haveNetworkConnection(PlayVideoActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(PlayVideoActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(PlayVideoActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(PlayVideoActivity.this).setPostParamsSecond(mPostArrayList, Parameters.fitday_id, fitday.id);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(PlayVideoActivity.this, PlayVideoActivity.this, ServerRequestConstants.FITDAYCOUNT,
                        mPostArrayList, null);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(PlayVideoActivity.this, getResources().getString(R.string.try_again), "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.ABUSE_REPORT)) {
            parse_ABUSE_REPORT(result);
        }else  if (apiName.equals(ServerRequestConstants.FITDAYCOUNT)) {
            if(FitDayActivity.mHandler!=null){
                FitDayActivity.mHandler.sendEmptyMessage(3);
            }
        }
    }

    private void playvdo(String url) {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();
        mMediaController = new MediaController(this);
        videoView.setMediaController(mMediaController);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (show == false) {
                    pd.dismiss();
                    // AppDelegate.showDialog_okCancel(PlayVideoActivity.this, "Service Time Out!!!", "next", "retry", PlayVideoActivity.this);

                }
            }
        }, 60000);
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("video", "setOnErrorListener ");
                pd.dismiss();
                AppDelegate.ShowDialogID(PlayVideoActivity.this, "Can't play this vedio", "Alert", "ERROR", PlayVideoActivity.this);
                return true;
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                show = true;
                pd.dismiss();
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {

                    }
                });
            }
        });
        videoView.setVideoURI(Uri.parse(url));
        videoView.requestFocus();
        videoView.start();
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase("ERROR")) {

        } else if (name.equalsIgnoreCase("next")) {
            finish();
        } else if (name.equalsIgnoreCase("retry")) {
            playvdo(url);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
           /* case R.id.txt_report_abuse:
                show_dialog();
               // execute_Report_abuse();
                break;*/
        }
    }

    private void show_dialog() {
        if (builder != null && builder.isShowing()) {
            builder.dismiss();
        }
        builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.report_abuse);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok, cancel;
        final EditText et_comment;
        builder.setCancelable(false);
        cancel = (TextView) builder.findViewById(R.id.no);
        ok = (TextView) builder.findViewById(R.id.yes);
        et_comment= (EditText) builder.findViewById(R.id.et_comment);
        builder.show();
        // builder.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AppDelegate.isValidString(et_comment.getText().toString())){
                    execute_Report_abuse();
                    builder.dismiss();
                }else{
                   AppDelegate.showToast(PlayVideoActivity.this,getResources().getString(R.string.abuse_validation));
                }
            }
        });
    }

    private void execute_Report_abuse() {
        try {
            if (AppDelegate.haveNetworkConnection(PlayVideoActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(PlayVideoActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(PlayVideoActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(PlayVideoActivity.this).setPostParamsSecond(mPostArrayList, Parameters.fitday_id, fitday.id);
                AppDelegate.getInstance(PlayVideoActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_comments, fitday.id);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(PlayVideoActivity.this, PlayVideoActivity.this, ServerRequestConstants.ABUSE_REPORT,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(PlayVideoActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(PlayVideoActivity.this, getResources().getString(R.string.try_again), "Alert!!!");
        }

    }

    private void parse_ABUSE_REPORT(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(this, jsonObject.getString(Tags.message));
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }
}
