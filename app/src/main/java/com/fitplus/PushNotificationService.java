package com.fitplus;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.util.Random;

import Constants.Tags;
import fragments.CalendarFragment;
import model.ChatModel;
import model.UserResponseModel;
import utils.Prefs;

/**
 * Created by admin on 23-05-2016.
 */
public class PushNotificationService extends GcmListenerService {
    private int status = 0;
    private String message;
    private String team_id;
    private Intent intent;
    Prefs prefs;
    public static final String TEAMRQUESTACCEPT = "teamRquestAccept";
    public static final String USER = "user";
    public static final String COACH = "coach";
    public static final String FINDCOACH_TEAM = "findcoach_Team";
    public static final String FINDCOACH_TEAM_ACCEPT = "findcoach_team_accept";
    public static final String RQUESTACCEPT_COACH = "RquestAccept_coach";
    public static final String GETCOACH_TEAM = "getcoach_Team";
    public static final String COACH_ADDED = "coach_added";
    public static final String NEWSCHEDULE = "NewSchedule";
    public static final String COACHGETNEWSCHEDULE = "coachGetNewSchedule";
    public static final String LOCKHOURS_TEAM = "lockhours_team";
    public static final String LOACKHOURS_TEAM_ACCEPT = "loackhours_team_accept";

    @Override

    public void onMessageReceived(String from, Bundle data) {
        Log.d("Push message", ", data = " + data);
        Log.d("Push message", message + ", data = " + data);
        prefs = new Prefs(this);
        if (prefs.getnotification(Tags.notification_value)) {
            if (data.getString("requestType").equalsIgnoreCase(TEAMRQUESTACCEPT)) {
                showNotificationUserRequestAccepted(data);
            } else if (data.getString("requestType").equalsIgnoreCase(USER) || data.getString("requestType").equalsIgnoreCase(COACH)) {
                showNotification(data);
                if (MainActivity.mHandler != null) {
                    MainActivity.mHandler.sendEmptyMessage(3);
                }
            } else if (data.getString("requestType").equalsIgnoreCase(FINDCOACH_TEAM)) {
                shownotificationforfindCoach(data);
                if (MainActivity.mHandler != null) {
                    MainActivity.mHandler.sendEmptyMessage(3);
                }
            } else if (data.getString("requestType").equalsIgnoreCase(FINDCOACH_TEAM_ACCEPT) /*|| data.getString("requestType").equalsIgnoreCase(RQUESTACCEPT_COACH)*/ || data.getString("requestType").equalsIgnoreCase(COACH_ADDED) || data.getString("requestType").equalsIgnoreCase(COACHGETNEWSCHEDULE)) {
                showrequestAccepted(data);
            } else if (data.getString("requestType").equalsIgnoreCase(GETCOACH_TEAM)) {
                shownotificationforgetCoach(data);
                if (MainActivity.mHandler != null) {
                    MainActivity.mHandler.sendEmptyMessage(3);
                }
            } else if (data.getString("requestType").equalsIgnoreCase(NEWSCHEDULE)|| data.getString("requestType").equalsIgnoreCase(LOCKHOURS_TEAM)) {
                shownotificationforgetCoach(data);

            } else if (data.getString("requestType").equalsIgnoreCase(RQUESTACCEPT_COACH)) {
                showrequestAcceptedbycoach(data);

            }else if (data.getString("requestType").equalsIgnoreCase(LOACKHOURS_TEAM_ACCEPT)) {
                showrequestAcceptedbyTeam(data);

            }
        }
    }
    private void showrequestAcceptedbyTeam(Bundle bundle) {
        AppDelegate.LogT("bundle=>" + bundle);
        try {
            ChatModel chatModel = new ChatModel(Integer.parseInt(bundle.getString("team_id")));
            if (CreateTeamActivity.onReciveSocketMessage != null) {
                CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        try {
            if(CalendarFragment.mHandler!=null){
                CalendarFragment.mHandler.sendEmptyMessage(0);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.splash)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(bundle.getString("message")))
                .setContentText(bundle.getString("message"));
        intent = new Intent();
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.splash,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
    }

    private void showrequestAcceptedbycoach(Bundle bundle) {
        AppDelegate.LogT("bundle=>" + bundle);
        try {
            ChatModel chatModel = new ChatModel(Integer.parseInt(bundle.getString("team_id")));
            if (CreateTeamActivity.onReciveSocketMessage != null) {
                CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        try {
if(RadarActivity.mActivity!=null){
    RadarActivity.mHandler.sendEmptyMessage(3);
}
        } catch (Exception e) {
AppDelegate.LogE(e);
        }
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.splash)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(bundle.getString("message")))
                .setContentText(bundle.getString("message"));
        intent = new Intent();
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.splash,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
    }

    private void shownotificationforgetCoach(Bundle bundle) {
        AppDelegate.LogT("bundle=>" + bundle);
        // JSONObject json=new JSONObject(bundle.get);
        UserResponseModel coupan = new UserResponseModel();
        try {
            coupan.message = bundle.getString("message");
            coupan.request_type = bundle.getString("requestType");
            coupan.team_id = bundle.getString("team_id");
            coupan.team_name = bundle.getString("teamname");
            coupan.team_members = bundle.getString("teamMembers");
            coupan.user_avtar = bundle.getString("userAvtar");
            coupan.team_avtar = bundle.getString("teamAvtar");
            coupan.notify_id = bundle.getString("notify_id");
            coupan.coach_id = bundle.getString("coach_id");
            coupan.username_first = bundle.getString("username_first");
            coupan.username_second = bundle.getString("username_second");
            coupan.username_third = bundle.getString("username_third");
            coupan.FirstAvtar = bundle.getString("FirstAvtar");
            coupan.SecondAvtar = bundle.getString("SecondAvtar");
            coupan.ThirdAvtar = bundle.getString("ThirdAvtar");
            AppDelegate.LogT("user_avtar:=>" + coupan.user_avtar);
            AppDelegate.LogT("team_avtar:=>" + coupan.team_avtar);
            AppDelegate.LogT("requestType:=>" + coupan.request_type);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        if (coupan.message.equalsIgnoreCase("User")) {
            AppDelegate.LogT("I m In USER");
            intent = new Intent(this, UserResponseNotification.class);
        } else {
            AppDelegate.LogT("I m In USER 2");
            intent = new Intent(this, UserResponseNotification.class);
        }

        Bundle bundleData = new Bundle();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        bundleData.putParcelable("coupan_detail", coupan);
        intent.putExtras(bundleData);

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.splash)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(coupan.message))
                .setContentText(coupan.message);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.splash,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);

    }

    private void showrequestAccepted(Bundle bundle) {
        AppDelegate.LogT("bundle=>" + bundle);
        try {
            ChatModel chatModel = new ChatModel(Integer.parseInt(bundle.getString("team_id")));
            if (CreateTeamActivity.onReciveSocketMessage != null) {
                CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.splash)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(bundle.getString("message")))
                .setContentText(bundle.getString("message"));
        intent = new Intent();
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.splash,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
    }

    private void shownotificationforfindCoach(Bundle bundle) {
        AppDelegate.LogT("bundle=>" + bundle);
        // JSONObject json=new JSONObject(bundle.get);
        UserResponseModel coupan = new UserResponseModel();
        try {
            coupan.message = bundle.getString("message");
            coupan.request_type = bundle.getString("requestType");
            coupan.team_id = bundle.getString("team_id");
            coupan.team_name = bundle.getString("teamname");
            coupan.team_members = bundle.getString("teamMembers");
            coupan.user_avtar = bundle.getString("userAvtar");
            coupan.team_avtar = bundle.getString("teamAvtar");
            coupan.notify_id = bundle.getString("notify_id");
            coupan.username_first = bundle.getString("username_first");
            coupan.username_second = bundle.getString("username_second");
            coupan.username_third = bundle.getString("username_third");
            coupan.FirstAvtar = bundle.getString("FirstAvtar");
            coupan.SecondAvtar = bundle.getString("SecondAvtar");
            coupan.ThirdAvtar = bundle.getString("ThirdAvtar");
            AppDelegate.LogT(" appp First avtar==>" + coupan.FirstAvtar + "   Firstname==" + coupan.username_first + " second avtar==" + coupan.SecondAvtar + "  second name==" + coupan.username_second + "  third avtar==" + coupan.ThirdAvtar + " third name== " + coupan.username_third + "   ");


            AppDelegate.LogT("user_avtar:=>" + coupan.user_avtar);
            AppDelegate.LogT("team_avtar:=>" + coupan.team_avtar);
            AppDelegate.LogT("requestType:=>" + coupan.request_type);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        if (coupan.message.equalsIgnoreCase("User")) {
            AppDelegate.LogT("I m In USER");
            intent = new Intent(this, UserResponseNotification.class);
        } else {
            AppDelegate.LogT("I m In USER 2");
            intent = new Intent(this, UserResponseNotification.class);
        }

        Bundle bundleData = new Bundle();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        bundleData.putParcelable("coupan_detail", coupan);
        intent.putExtras(bundleData);

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.splash)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(coupan.message))
                .setContentText(coupan.message);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.splash,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
    }

    private void showNotificationUserRequestAccepted(Bundle bundle) {
        AppDelegate.LogT("bundle=>" + bundle);

        try {
            ChatModel chatModel = new ChatModel(Integer.parseInt(bundle.getString("team_id")));
            if (CreateTeamActivity.onReciveSocketMessage != null) {
                CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        intent = new Intent();
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.splash)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(bundle.getString("message")))
                .setContentText(bundle.getString("message"));
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.splash,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showNotification(Bundle bundle) {
        AppDelegate.LogT("bundle=>" + bundle);
        // JSONObject json=new JSONObject(bundle.get);
        UserResponseModel coupan = new UserResponseModel();
        try {
            coupan.message = bundle.getString("message");
            coupan.request_type = bundle.getString("requestType");
            coupan.team_id = bundle.getString("team_id");
            coupan.team_name = bundle.getString("teamname");
            coupan.team_members = bundle.getString("teamMembers");
            coupan.user_avtar = bundle.getString("userAvtar");
            coupan.team_avtar = bundle.getString("teamAvtar");
            coupan.notify_id = bundle.getString("notify_id");
            coupan.username_first = bundle.getString("username_first");
            coupan.username_second = bundle.getString("username_second");
            coupan.username_third = bundle.getString("username_third");
            coupan.FirstAvtar = bundle.getString("FirstAvtar");
            coupan.SecondAvtar = bundle.getString("SecondAvtar");
            coupan.ThirdAvtar = bundle.getString("ThirdAvtar");

            AppDelegate.LogT("user_avtar:=>" + coupan.user_avtar);
            AppDelegate.LogT("team_avtar:=>" + coupan.team_avtar);
            AppDelegate.LogT("requestType:=>" + coupan.request_type);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (coupan.message.equalsIgnoreCase("User")) {
            AppDelegate.LogT("I m In USER");
            intent = new Intent(this, UserResponseNotification.class);
        } else {
            AppDelegate.LogT("I m In USER 2");
            intent = new Intent(this, UserResponseNotification.class);
        }
        Bundle bundleData = new Bundle();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        bundleData.putParcelable("coupan_detail", coupan);
        intent.putExtras(bundleData);

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.splash)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(coupan.message))
                .setContentText(coupan.message);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.splash,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
    }

    public static void showUserChatNotification(Context mContext, ChatModel messageModel) {
        try {
            String message = messageModel.txt_name_rcv + " has sent you message.";

            Notification.Builder notificationBuilder = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentTitle(mContext.getString(R.string.app_name))
                        .setStyle(new Notification.BigTextStyle().bigText(message))
                        .setContentText(message);
            } else {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(mContext.getString(R.string.app_name))
                        .setContentText(message);
            }
            Bundle bundle = new Bundle();
            bundle.putString(Tags.FROM, Tags.CHAT);
            bundle.putInt(Tags.PAGE, CreateTeamActivity.FROM_CHAT);
            bundle.putParcelable(Tags.message, messageModel);
            Intent push = new Intent(mContext, NotificationHandler.class);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            push.putExtras(bundle);
            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(mContext, 0, push, PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder.setContentIntent(fullScreenPendingIntent);
            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
                notification = notificationBuilder.build();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                notification = new Notification(R.mipmap.ic_launcher,
                        message, System.currentTimeMillis());
            } else {
                notification = notificationBuilder.build();
            }

            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            ((NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public static int getRandomNumer() {
        Random r = new Random();
        int value = r.nextInt(80 - 65) + 65;
        return value;
    }
}