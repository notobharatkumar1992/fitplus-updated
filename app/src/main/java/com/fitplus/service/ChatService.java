package com.fitplus.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import com.fitplus.AppDelegate;
import com.fitplus.CreateTeamActivity;
import com.fitplus.PushNotificationService;

import org.json.JSONObject;

import Constants.Tags;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import model.ChatModel;
import model.UserDataModel;
import utils.Prefs;

public class ChatService extends Service {

    public static final String DISCONNECT = "disconnect";
    public static final String MESSAGE = "message";
    public static final String ADD_USER = "add user";

    public ChatService() {
    }

    public Prefs prefs;
    public UserDataModel dataModel;
    public static Socket mSocket;
    private boolean isConnected = true;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = new Prefs(this);
        dataModel = prefs.getUserdata();
        AppDelegate.LogS("onCreate called");
        onCreateChat();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
//        callLogoutAsync();
        AppDelegate.LogS("onTaskRemoved called");

        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        //Restart the service once it has been killed android
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 100, restartServicePI);
    }

    public void onCreateChat() {
        AppDelegate.LogC("onCreateChat called");
        mSocket = AppDelegate.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);

        mSocket.on("send", onNewMessage);
        mSocket.connect();
    }

    public Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!isConnected) {
                showToast("connect");
                isConnected = true;
            }

            AppDelegate.LogC("onConnected called");
        }
    };

    public Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            isConnected = false;
            try {
                showToast("disconnect");
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.LogC("onDisconnect called");
        }
    };

    public Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                showToast("error_connect");
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.LogC("onConnectError called");
        }
    };

    public Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
     //  String s = "{\"key\":\"value\"}";
            AppDelegate.LogC("onNewMessage called => " + args[0]);
            try {
                dataModel = prefs.getUserdata();
                JSONObject jsonObject = new JSONObject(args[0].toString());
                jsonObject = jsonObject.getJSONObject("message");
                if (((jsonObject.getJSONObject("team").getString("first_member_id").equalsIgnoreCase(dataModel.userId + "") || jsonObject.getJSONObject("team").getString("second_member_id").equalsIgnoreCase(dataModel.userId + "") || jsonObject.getJSONObject("team").getString("third_member_id").equalsIgnoreCase(dataModel.userId + "") || jsonObject.getJSONObject("team").getString("coach_id").equalsIgnoreCase(dataModel.userId + "")))) {
                    ChatModel chatModel = new ChatModel();
                    chatModel.id = jsonObject.getInt(Tags.sender_id);

                    chatModel.txt_msg_rcv = jsonObject.getString(Tags.message);
                    chatModel.txt_time_rcv = jsonObject.getString(Tags.created);

                    chatModel.team_id = jsonObject.getInt("team_id");
                    chatModel.team_name = jsonObject.getJSONObject("team").getString("team_name");
                    chatModel.team_pic = jsonObject.getJSONObject("team").getString("image_1");

                    chatModel.txt_name_rcv = jsonObject.getJSONObject("user").getString("first_name");
                    chatModel.txt_pic_rcv = jsonObject.getJSONObject("user").getString("image_1");
                    chatModel.chat_status = ChatModel.FROM_OTHER;

                    if (CreateTeamActivity.onReciveSocketMessage != null) {
                        CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("chat", chatModel);
                    } else {
                        PushNotificationService.showUserChatNotification(ChatService.this, chatModel);
                    }
                } else {
                    AppDelegate.LogC("message => " + jsonObject.has("team") + ", ");
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    };

    private void showToast(final String message) {
        try {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
//                    Toast.makeText(ChatService.this, message, Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}

