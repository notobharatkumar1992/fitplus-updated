package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.camerademo.util.NetWorkUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.FitDayAdapter;
import fragments.NotificationFragments;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.ChatModel;
import model.Notification_Fragments_Model;
import model.PostAysnc_Model;
import model.UserResponseModel;
import utils.Prefs;

public class UserActionActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    ImageView img_loading, img_loading1, img_loading2, img_loading3;

    Activity mActivity;
    private RecyclerView trending_list;
    private Prefs prefs;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private FitDayAdapter trending_Adapter;
    private ImageView back;
    Intent intent;
    private int id;
    ImageView img_user_one, img_user_two, img_user_three;
    private TextView team_name, team_count, do_not_disturb, txt_user_one, txt_user_two, txt_user_three, reject, accept;
    private Bundle bundle;
    private Notification_Fragments_Model userResponse;
    int param = 0;
    private ImageView avtar_img;
    public static final String USER = "user";
    public static final String COACH = "coach";
    public static final String FINDCOACH_TEAM = "findcoach_Team";
    public static final String FINDCOACH_TEAM_ACCEPT = "findcoach_team_accept";
    public static final String RQUESTACCEPT_COACH = "RquestAccept_coach";
    public static final String GETCOACH_TEAM = "getcoach_Team";
    public static final String COACH_ADDED = "coach_added";
    public static final String NEWSCHEDULE = "NewSchedule";
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).showImageForEmptyUri(R.drawable.img).
    /* .showImageOnFail(fallback)
     .showImageOnLoading(fallback).*/
            build();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_notification);
        mActivity = this;
        prefs = new Prefs(this);
        bundle = getIntent().getExtras();
        userResponse = bundle.getParcelable(Tags.NotificationList);
        findIDs();
        TextView title = (TextView) findViewById(R.id.title);
        if (userResponse.notificaton_type.equalsIgnoreCase(USER)) {
            title.setText("User request for team.");
            team_count.setVisibility(View.VISIBLE);
            do_not_disturb.setVisibility(View.VISIBLE);
        } else if (userResponse.notificaton_type.equalsIgnoreCase(COACH)) {
            title.setText("Coach request for Team");
            team_count.setVisibility(View.GONE);
            do_not_disturb.setVisibility(View.VISIBLE);
        } else if (userResponse.notificaton_type.equalsIgnoreCase(FINDCOACH_TEAM)) {
            title.setText("Permission to find coach");
            team_count.setVisibility(View.GONE);
            do_not_disturb.setVisibility(View.GONE);
        } else if (userResponse.notificaton_type.equalsIgnoreCase(GETCOACH_TEAM)) {
            title.setText("Permission to get coach");
            team_count.setVisibility(View.GONE);
            do_not_disturb.setVisibility(View.GONE);
        } else if (userResponse.notificaton_type.equalsIgnoreCase(NEWSCHEDULE)) {
            title.setText("Permission to schedule next workout");
            team_count.setVisibility(View.GONE);
            do_not_disturb.setVisibility(View.GONE);
        }

        if (userResponse.action_taken == 1 || userResponse.notificaton_type.equalsIgnoreCase(FINDCOACH_TEAM) || userResponse.notificaton_type.equalsIgnoreCase(GETCOACH_TEAM) || userResponse.notificaton_type.equalsIgnoreCase(NEWSCHEDULE)) {
            do_not_disturb.setVisibility(View.GONE);
            accept.setVisibility(View.GONE);
            reject.setVisibility(View.GONE);
        } else {
            if (userResponse.notificaton_type.equalsIgnoreCase(FINDCOACH_TEAM) || userResponse.notificaton_type.equalsIgnoreCase(GETCOACH_TEAM) || userResponse.notificaton_type.equalsIgnoreCase(NEWSCHEDULE)) {
                do_not_disturb.setVisibility(View.GONE);
            } else {
                do_not_disturb.setVisibility(View.VISIBLE);
            }
            do_not_disturb.setOnClickListener(this);
            accept.setOnClickListener(this);
            reject.setOnClickListener(this);
            accept.setVisibility(View.VISIBLE);
            reject.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
        if (MainActivity.mHandler != null) {
            MainActivity.mHandler.sendEmptyMessage(3);
        }
    }

    private void findIDs() {
        do_not_disturb = (TextView) findViewById(R.id.do_not_disturb);
        accept = (TextView) findViewById(R.id.accept);
        reject = (TextView) findViewById(R.id.reject);
        avtar_img = (ImageView) findViewById(R.id.img_content);
        team_name = (TextView) findViewById(R.id.txt_team_name);
        team_count = (TextView) findViewById(R.id.txt_team_members);
        txt_user_one = (TextView) findViewById(R.id.txt_user_one);
        txt_user_two = (TextView) findViewById(R.id.txt_user_two);
        txt_user_three = (TextView) findViewById(R.id.txt_user_three);
        img_user_one = (ImageView) findViewById(R.id.img_user_one);
        img_user_two = (ImageView) findViewById(R.id.img_user_two);
        img_user_three = (ImageView) findViewById(R.id.img_user_three);
        team_name.setText(userResponse.teamname);
        team_count.setText("Members : " + userResponse.teamMembers);
        img_loading = (ImageView) findViewById(R.id.img_loading);
        img_loading1 = (ImageView) findViewById(R.id.img_loading1);
        img_loading2 = (ImageView) findViewById(R.id.img_loading2);
        img_loading3 = (ImageView) findViewById(R.id.img_loading3);
        //  txt_user_one.setText(userResponse.user_name_one + "");
        set_image();
    }

    private void set_image() {
        if (userResponse.team_avtar.isEmpty()) {
            AppDelegate.LogT("set_image = null");
        } else {
            img_loading.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            imageLoader.loadImage(userResponse.team_avtar, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    avtar_img.setImageBitmap(bitmap);
                    img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

      /*      Picasso.with(this)
                    .load(userResponse.team_avtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    avtar_img.setImageBitmap(bitmap);
                    img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });*/
        }
        if (!userResponse.FirstAvtar.isEmpty() || AppDelegate.isValidString(userResponse.username_first)) {
            AppDelegate.LogT("userResponse.FirstAvtar");
            img_loading1.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading1.getDrawable();
                    frameAnimation.setCallback(img_loading1);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading1.getDrawable()).start();
                }
            });
            img_user_one.setVisibility(View.VISIBLE);
            txt_user_one.setText(userResponse.username_first + "");

            imageLoader.loadImage(userResponse.FirstAvtar, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    img_user_one.setImageBitmap(bitmap);
                    img_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
       /*     Picasso.with(this)
                    .load(userResponse.FirstAvtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    img_user_one.setImageBitmap(bitmap);
                    img_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });*/
        } else {
            img_user_one.setVisibility(View.INVISIBLE);
        }
        if (!userResponse.SecondAvtar.isEmpty() || AppDelegate.isValidString(userResponse.username_second)) {
            AppDelegate.LogT("userResponse.SecondAvtar");
            img_loading2.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading2.getDrawable();
                    frameAnimation.setCallback(img_loading2);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading2.getDrawable()).start();
                }
            });
            img_user_two.setVisibility(View.VISIBLE);
            txt_user_two.setText(userResponse.username_second + "");
            imageLoader.loadImage(userResponse.SecondAvtar, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    img_user_two.setImageBitmap(bitmap);
                    img_loading2.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });


           /* Picasso.with(this)
                    .load(userResponse.SecondAvtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    img_user_two.setImageBitmap(bitmap);
                    img_loading2.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });*/
        } else {
            img_user_two.setVisibility(View.INVISIBLE);
        }
        if (!userResponse.ThirdAvtar.isEmpty() || AppDelegate.isValidString(userResponse.username_third)) {
            AppDelegate.LogT("userResponse.ThirdAvtar");
            img_loading3.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading3.getDrawable();
                    frameAnimation.setCallback(img_loading3);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading3.getDrawable()).start();
                }
            });
            img_user_three.setVisibility(View.VISIBLE);
            txt_user_three.setText(userResponse.username_third + "");
            imageLoader.loadImage(userResponse.ThirdAvtar, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    img_user_three.setImageBitmap(bitmap);
                    img_loading3.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });


       /*     Picasso.with(this)
                    .load(userResponse.ThirdAvtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    img_user_three.setImageBitmap(bitmap);
                    img_loading3.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });*/
        } else {
            img_user_three.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.accept:
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    param = Parameters.accept;
                    execute(Parameters.accept);
                } else {
                    AppDelegate.ShowDialog(this, "Please LoginActivity.", "Alert");
                }
                break;
            case R.id.reject:
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    param = Parameters.reject;
                    execute(Parameters.reject);
                } else {
                    AppDelegate.ShowDialog(this, "Please LoginActivity.", "Alert");
                }
                break;
            case R.id.do_not_disturb:
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    param = Parameters.do_not_disturb;
                    execute(Parameters.do_not_disturb);
                } else {
                    AppDelegate.ShowDialog(this, "Please LoginActivity.", "Alert");
                }
                break;
        }
    }

    private void execute(int type) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj = null;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.team_id, userResponse.team_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.devicetype, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.notify_id, userResponse.notify_id, ServerRequestConstants.Key_PostintValue);

                if (userResponse.notificaton_type.equalsIgnoreCase("user")) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.type, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.USER_RESPONSE,
                            mPostArrayList, null);
                } else if (userResponse.notificaton_type.equalsIgnoreCase("COACH")) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.COACH_ACTION,
                            mPostArrayList, null);
                } else if (userResponse.notificaton_type.equalsIgnoreCase(FINDCOACH_TEAM)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.type, FINDCOACH_TEAM);
                    //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coach_id, 0);

                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.USER_ACTION,
                            mPostArrayList, null);
                } else if (userResponse.notificaton_type.equalsIgnoreCase(GETCOACH_TEAM)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.type, GETCOACH_TEAM);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coach_id, userResponse.coach_id);
                    //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coachname, userResponse.coachname);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.USER_ACTION,
                            mPostArrayList, null);
                } else if (userResponse.notificaton_type.equalsIgnoreCase(NEWSCHEDULE)) {
                    //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coachname, userResponse.coachname);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SCHEDULE_ACTION,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.USER_RESPONSE)) {
            parseTrendingList(result);
        } else if (apiName.equals(ServerRequestConstants.COACH_ACTION)) {
            parseTrendingList(result);
        } else if (apiName.equals(ServerRequestConstants.USER_ACTION)) {
            parseTrendingList(result);
        }else if (apiName.equals(ServerRequestConstants.SCHEDULE_ACTION)) {
            parseTrendingList(result);
        }
    }


    private void parseTrendingList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, UserResponseNotification.this);
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                try {
                    if (NotificationFragments.mHandler != null) {
                        NotificationFragments.mHandler.sendEmptyMessage(1);
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                if (param == 1) {
//                startActivity(new Intent(UserResponseNotification.this, RadarFragment.class));
                    finish();
                } else if (param == 2 || param == 3) {
                    finish();
                }
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.cancel, UserActionActivity.this);
//                    finish();
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.cancel, UserActionActivity.this);
//                    finish();
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }

    }


    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            if (param == 1) {
//                startActivity(new Intent(UserResponseNotification.this, RadarFragment.class));
                finish();
            } else if (param == 2 || param == 3) {
                finish();
            }
        } else if (name.equalsIgnoreCase(Tags.cancel)) {
            finish();
        }
    }
}
