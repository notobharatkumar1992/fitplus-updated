package com.fitplus;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import Async.LocationAddress;
import Constants.Tags;
import utils.Prefs;

public class SelectLocationActivity extends FragmentActivity implements LocationListener, View.OnClickListener {

    private GoogleMap mMap;
    private Location center;
    private LatLng previous, current, tempLatLng;
    private double difference;
    private LocationManager locationManager;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    private Handler mHandler;
    private String address;
    private SupportMapFragment mapFragment;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showMap();
            }
        }, 1500);

        findViewById(R.id.back).setOnClickListener(this);
        setHandler();
    }


    public void showMap() {
        mMap = mapFragment.getMap();
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        center = new Location("center");
        previous = new LatLng(center.getLatitude(), center.getLongitude());

        try {
            if (SetLocationActivity.LATITUDE > 0.0 && SetLocationActivity.LONGITUDE > 0.0) {
                tempLatLng = new LatLng(SetLocationActivity.LATITUDE, SetLocationActivity.LONGITUDE);
            } else {
                tempLatLng = new LatLng(Double.parseDouble(new Prefs(this).getStringValue(Tags.TAG_LAT, "")), Double.parseDouble(new Prefs(this).getStringValue(Tags.TAG_LONG, "")));
            }
            setloc(tempLatLng.latitude, tempLatLng.longitude);
            createMarker(tempLatLng.latitude, tempLatLng.longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tempLatLng, 14.0f));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    createMarker(tempLatLng.latitude, tempLatLng.longitude);
                }
            }, 800);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(final CameraPosition cameraPosition) {
                        center.setLatitude(cameraPosition.target.latitude);
                        center.setLongitude(cameraPosition.target.longitude);
                        current = new LatLng(center.getLatitude(), center.getLongitude());
                        difference = distance(previous.latitude, previous.longitude, current.latitude, current.longitude);
                        AppDelegate.LogT("difference current =>" + difference);
                        if (difference > 1.0) {
                            AppDelegate.LogT("difference =>" + difference);
                            setloc(center.getLatitude(), center.getLongitude());
                            createMarker(center.getLatitude(), center.getLongitude());
                            previous = new LatLng(center.getLatitude(), center.getLongitude());
                        }
                    }
                });
            }
        }, 700);
        try {
            View locationButton = ((View) mapFragment.getView().findViewById(0x1).getParent()).findViewById(0x2);
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void setloc(double latitude, double longitude) {
        mHandler.sendEmptyMessage(12);
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;

                    case 12:
                        progressBar.setVisibility(View.VISIBLE);
                        break;
                    case 13:
                        progressBar.setVisibility(View.GONE);
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        SetLocationActivity.addressName = data.getString(Tags.PLACE_NAME);
        if (AppDelegate.isValidString(SetLocationActivity.addressName)) {
            address = SetLocationActivity.addressName + " " + data.getString(Tags.ADDRESS);
        }
        createMarker(center.getLatitude(), center.getLongitude());
        mHandler.sendEmptyMessage(13);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (SetLocationActivity.mHandler != null) {
            SetLocationActivity.mHandler.sendEmptyMessageDelayed(12, 500);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
//        mMap.animateCamera(cameraUpdate);
//        locationManager.removeUpdates(this);
//      createMarker(location.getLatitude(), location.getLongitude());
    }

    private void createMarker(double latitude, double longitude) {
        SetLocationActivity.LATITUDE = latitude;
        SetLocationActivity.LONGITUDE = longitude;

        mMap.clear();
        mMap.addMarker(AppDelegate.getMarkerOptionsWithTitleSnippet(this, new LatLng(latitude, longitude), "Selected location", address));
//        mMap.addMarker(new MarkerOptions().position(sydney).snippet(address));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }
}
