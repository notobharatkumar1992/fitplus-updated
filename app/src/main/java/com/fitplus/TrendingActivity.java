package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.TrendingListAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnListItemClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.RadarModel;
import model.UserDataModel;
import utils.Prefs;
import utils.SpacesItemDecoration;
import utils.SpacesItemTransDecoration;

public class TrendingActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener, OnListItemClickListener {
    Activity mActivity;
    private RecyclerView trending_list;
    private Prefs prefs;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private TrendingListAdapter trending_Adapter;
    private ImageView back;
    ArrayList<RadarModel> radarModel;
    private Bundle bundle;
    private ArrayList<RadarModel> become_coach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trending);
        mActivity = this;
        findIDs();
        prefs = new Prefs(this);
        execute_createTeam();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    private void execute_createTeam() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(this).getStringValue(Tags.TAG_LONG, ""));
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, TrendingActivity.this, ServerRequestConstants.TRENDING_LIST,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    private void findIDs() {
        trending_list = (RecyclerView) findViewById(R.id.trending);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.TRENDING_LIST)) {
            parseTrendingList(result);
        }
    }

    private void parseTrendingList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                ArrayList<UserDataModel> trending = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    UserDataModel userDataModel = new UserDataModel();
                    JSONObject object = jsonArray.getJSONObject(i);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.first_name = object.getString(Tags.first_name);
                    userDataModel.gid = object.getString(Tags.gid);
                    userDataModel.fid = object.getString(Tags.fid);
                    userDataModel.last_name = object.getString(Tags.last_name);
                    userDataModel.email = object.getString(Tags.email);
                    userDataModel.password = object.getString(Tags.password);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.str_Gender = object.getString(Tags.gender);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.dob = object.getString(Tags.birthdate);
                    userDataModel.nickname = object.getString(Tags.nick_name);
                    userDataModel.avtar = object.has(Tags.avtar) ? object.getString(Tags.avtar) : "http://www.sjafhasfajfh";
                    if (userDataModel.avtar.isEmpty()) {
                        userDataModel.avtar = "http://www.sjafhasfajfh";
                    }
                    userDataModel.avtar_thumb = object.has(Tags.avtar_thumb) ? object.getString(Tags.avtar_thumb) : "http://www.sjafhasfajfh";
                    if (userDataModel.avtar_thumb.isEmpty()) {
                        userDataModel.avtar_thumb = "http://www.sjafhasfajfh";
                    }
                    userDataModel.views = object.getString(Tags.views);
                    userDataModel.follower_count = object.getInt(Tags.follower_count);
                    userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                    userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                    userDataModel.bank_account = object.getString(Tags.bank_account);
                    userDataModel.certificates = object.getString(Tags.certificates);
                    userDataModel.country_id = object.getString(Tags.country_id);
                    userDataModel.role_id = object.getInt(Tags.role_id);
                    userDataModel.state_id = object.getString(Tags.state_id);
                    userDataModel.city_id = object.getString(Tags.city_id);
                    userDataModel.post_code = object.getString(Tags.post_code);
                    userDataModel.sex_group = object.getString(Tags.sex_group);
                    userDataModel.fat_status = object.getString(Tags.fat_status);
                    userDataModel.type = object.getString(Tags.type);
                    userDataModel.token = object.getString(Tags.token);
                    userDataModel.is_login = object.getInt(Tags.is_login);
                    userDataModel.is_social = object.getInt(Tags.is_social);
                    userDataModel.is_verified = object.getInt(Tags.is_verified);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    trending.add(userDataModel);
                }
                setTrendingList(trending);
                //  AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, TrendingActivity.this);

            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, TrendingActivity.this);
                    AppDelegate.showToast(TrendingActivity.this, jsonObject.getString(Tags.message));
                } else {
                    AppDelegate.showToast(TrendingActivity.this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }

    private void setTrendingList(ArrayList<UserDataModel> trending) {
        this.trending = trending;
        trending_list.setPadding(AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5));
        trending_list.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        trending_list.setLayoutManager(gaggeredGridLayoutManager);
       // trending_list.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(this, 5)));
        trending_list.addItemDecoration(new SpacesItemTransDecoration(AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 60)));

        trending_Adapter = new TrendingListAdapter(this, trending, this);
        trending_list.setAdapter(trending_Adapter);
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    trending_Adapter.notifyDataSetChanged();
                    trending_list.invalidate();
                }
            }, 2000);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
    @Override
    public void setOnDialogClickListener(String name) {

    }

    ArrayList<UserDataModel> trending = new ArrayList<>();

    @Override
    public void setOnListItemClickListener(String name, int position) {
        try {
            if (name.equalsIgnoreCase("name") && trending != null && trending.size() > position) {
                Intent intent = new Intent(TrendingActivity.this, ViewProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Parameters.login_id, trending.get(position).userId);
                bundle.putInt(Tags.FROM, ViewProfileActivity.FROM_VIEW);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

    }
}
