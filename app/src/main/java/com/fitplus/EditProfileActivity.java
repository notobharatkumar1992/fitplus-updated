package com.fitplus;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.LanguageAdapter;
import fragments.ViewProfileFragment;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by admin on 26-07-2016.
 */
public class EditProfileActivity extends FragmentActivity implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse {
    private CircleImageView cimg_upload_image;
    private ImageView img_loading, facebook, twitter;
    private EditText password, lname, fname, confirm_password, about_me;
    private Bitmap OriginalPhoto;
    private TextView email, location;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private Prefs prefs;
    private UserDataModel userDataModel;
    private Handler mHandler;
    private AnimationDrawable frameAnimation;
    private ScrollView scroll;

    private ImageView remember_me, notification;
    public TextView user_category, txt_language;

    private Dialog dialog;
    private ListView list;
    private String lang;
    private int notification_type = 0;
    private TextView city, birthday;
    private int mYear, mMonth, mDay;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        prefs = new Prefs(this);
        userDataModel = prefs.getUserdata();
        AppDelegate.LogT("userDataModel" + userDataModel + "");
        initView();
        setValues();
        // execute_getprofile();
    }

    private void setValues() {
        scroll.fullScroll(View.FOCUS_UP);//if you move at the end of the scroll
        scroll.pageScroll(View.FOCUS_UP);//if you move at the middle of the scroll
        scroll.smoothScrollTo(0, 0);
        img_loading.setVisibility(View.VISIBLE);
        frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        imageLoader.loadImage(userDataModel.avtar, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                AppDelegate.LogT("onBitmapLoaded" + userDataModel.avtar);
                cimg_upload_image.setImageBitmap(bitmap);
                frameAnimation.stop();
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

       /* Picasso.with(EditProfileActivity.this).load(userDataModel.avtar).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                AppDelegate.LogT("onBitmapLoaded" + userDataModel.avtar);
                cimg_upload_image.setImageBitmap(bitmap);
                frameAnimation.stop();
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                AppDelegate.LogT("onBitmapFailed" + userDataModel.avtar);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                AppDelegate.LogT("onPrepareLoad" + userDataModel.avtar);
            }
        });*/
        email.setText(userDataModel.email + "");
        fname.setText(AppDelegate.isValidString(userDataModel.first_name) ? userDataModel.first_name + "" : "");
        lname.setText(AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name + "" : "");
        about_me.setText(AppDelegate.isValidString(userDataModel.about_me) ? userDataModel.about_me + "" : "");
        birthday.setText(AppDelegate.isValidString(userDataModel.dob) ? userDataModel.dob + "" : "");
        city.setText(AppDelegate.isValidString(userDataModel.city_name) ? userDataModel.city_name + "" : "");
        setHandler();
        setloc(Double.parseDouble(new Prefs(this).getStringValue(Tags.TAG_LAT, "")), Double.parseDouble(new Prefs(this).getStringValue(Tags.TAG_LONG, "")));
    }

    private void initView() {
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);
        findViewById(R.id.Linear_upload).setOnClickListener(this);
        img_loading = (ImageView) findViewById(R.id.img_loading);
        cimg_upload_image = (CircleImageView) findViewById(R.id.cimg_upload_image);
        cimg_upload_image.setOnClickListener(this);
        email = (TextView) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        confirm_password = (EditText) findViewById(R.id.confirm_password);
        location = (TextView) findViewById(R.id.location);
        about_me = (EditText) findViewById(R.id.about_me);
        scroll = (ScrollView) findViewById(R.id.scroll);
        birthday = (TextView) findViewById(R.id.birthday);
        city = (TextView) findViewById(R.id.city);
        user_category = (TextView) findViewById(R.id.user_category);
        user_category.setOnClickListener(this);
        txt_language = (TextView) findViewById(R.id.language);
        txt_language.setOnClickListener(this);
        remember_me = (ImageView) findViewById(R.id.remember_me);
        remember_me.setOnClickListener(this);
        notification = (ImageView) findViewById(R.id.notification);
        notification.setOnClickListener(this);
        birthday.setOnClickListener(this);
        user_category.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + " ," + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "") + " ," + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));

        if (AppDelegate.isValidString(userDataModel.language)) {
            txt_language.setText(userDataModel.language + "");
        } else {
            txt_language.setText(getResources().getString(R.string.english));
        }
        if (userDataModel.notification_status == 1) {
            //  AppDelegate.LogT("notification on -====" + (prefs.getnotification(Tags.notification_value)));
            AppDelegate.LogT("userDataModel.notification_status == 1====" + userDataModel.notification_status);
            notification.setSelected(true);
        } else {
            AppDelegate.LogT("userDataModel.notification_status == 0===" + userDataModel.notification_status);
            // AppDelegate.LogT("notification off-====" + (prefs.getnotification(Tags.notification_value)));
            notification.setSelected(false);
        }
        String emailid = AppDelegate.getValue(EditProfileActivity.this, Tags.Email);
        if (AppDelegate.isValidString(emailid)) {
            AppDelegate.Log("emailid", emailid + "" + "hellooo");
            remember_me.setSelected(true);
        } else {
            AppDelegate.Log("emailid", emailid + "" + "byee");
            remember_me.setSelected(false);
        }

    }

    private void showDateDialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        birthday.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        c.add(Calendar.YEAR, -10);
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis()/*c.getTimeInMillis()*//*System.currentTimeMillis() - 10000*/);
        dpd.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cimg_upload_image:
                showImageSelectorList();
                break;
            case R.id.done:
                if (validateAll() == true) {
                    executeEdit();
                }
                break;
            case R.id.back:
                finish();
                break;
            case R.id.birthday:
                showDateDialog();
                break;
            case R.id.language:
                setLanguage();
                break;
            case R.id.user_category:
                AppDelegate.setting_or_not = true;
                startActivity(new Intent(EditProfileActivity.this, BeginnerQuestionActivity.class));
                break;
            case R.id.remember_me:
                if (remember_me.isSelected()) {
                    remember_me.setSelected(false);
                    AppDelegate.remember_me = false;
                    AppDelegate.save(EditProfileActivity.this, "", Tags.Email);
                    AppDelegate.save(EditProfileActivity.this, "", Tags.Password);
                    AppDelegate.LogT("userdata is on=====" + AppDelegate.getValue(EditProfileActivity.this, Tags.Email));
                    update();
                } else {
                    remember_me.setSelected(true);
                    AppDelegate.remember_me = true;
                    AppDelegate.LogT("userdata is off=====" + AppDelegate.getValue(EditProfileActivity.this, Tags.Email));
                    update();
                }
                break;
            case R.id.notification:
                if (notification.isSelected()) {
                    prefs.putnotification(Tags.notification_value, false);
                    notification.setSelected(false);
                    AppDelegate.LogT("off=====" + prefs.getnotification(Tags.notification_value));
                } else {
                    prefs.putnotification(Tags.notification_value, true);
                    notification.setSelected(true);
                    AppDelegate.LogT("on=====" + prefs.getnotification(Tags.notification_value));
                }
                break;
        }
    }

    private void update() {
        if (LoginActivity.mHandler != null) {
            AppDelegate.LogT("updateGlobal handler calld");
            LoginActivity.mHandler.sendEmptyMessage(1);
        }
    }

    private void Dialogue() {
        dialog = new Dialog(EditProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        list = (ListView) dialog.findViewById(R.id.dialog_list);
    }

    String setLanguage() {
        final String[] language = {getResources().getString(R.string.french), getResources().getString(R.string.english)};
        Dialogue();
        LanguageAdapter spinnerAdapter = new LanguageAdapter(EditProfileActivity.this, language);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                lang = language[position];
                txt_language.setText(lang);
                dialog.dismiss();
            }
        });
        dialog.show();
        return lang;
    }

    private void executeEdit() {
        if (notification.isSelected()) {
            notification_type = 1;
        } else {
            notification_type = 0;
        }
        try {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.first_name, fname.getText().toString() + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.last_name, lname.getText().toString() + "");
//                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude,"");
//                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude,"");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.password, password.getText().toString() + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(this).getStringValue(Tags.TAG_LONG, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.notification_status, notification_type, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.language, txt_language.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.about_me, about_me.getText().toString() + "");
                if (AppDelegate.isValidString(city.getText().toString())) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.city, city.getText().toString() + "");
                }
                if (AppDelegate.isValidString(birthday.getText().toString())) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.birthdate, birthday.getText().toString() + "");
                }
                // AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.email, email_address.getText().toString() + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, EditProfileActivity.this, ServerRequestConstants.EDIT_PROFILE,
                        mPostArrayList, null);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, capturedFile, ServerRequestConstants.Key_PostFileValue);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }

    }

    private boolean validateAll() {
        boolean result = true;
        if (AppDelegate.isValidString(password.getText().toString()) && AppDelegate.isValidString(confirm_password.getText().toString())) {
            if (!password.getText().toString().equals(confirm_password.getText().toString())) {
                result = false;
                AppDelegate.showToast(this, getResources().getString(R.string.formatch));
            } else {

            }
        } else if (AppDelegate.isValidString(password.getText().toString()) && !AppDelegate.isValidString(confirm_password.getText().toString())) {
            result = false;
            AppDelegate.showToast(this, getResources().getString(R.string.forconpass));

        } else if (!AppDelegate.isValidString(password.getText().toString()) && AppDelegate.isValidString(confirm_password.getText().toString())) {
            result = false;
            AppDelegate.showToast(this, getResources().getString(R.string.forpass));

        } else if (!AppDelegate.isValidString(fname.getText().toString())) {
            result = false;
            AppDelegate.showToast(this, getResources().getString(R.string.forname));
        } else if (!AppDelegate.isValidString(lname.getText().toString())) {
            result = false;
            AppDelegate.showToast(this, getResources().getString(R.string.forlastname));
        }
        return result;
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(EditProfileActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        ListView modeList = new ListView(EditProfileActivity.this);
        String[] stringArray = new String[]{"Camera", "Gallery", "Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(EditProfileActivity.this, android.R.layout.simple_spinner_dropdown_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(EditProfileActivity.this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(EditProfileActivity.this, EditProfileActivity.this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            //parsegetprofile(result);
        } else if (apiName.equals(ServerRequestConstants.EDIT_PROFILE)) {
            parseEditProfile(result);
        }
    }

    private void parseEditProfile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.gid = object.getString(Tags.gid);
                userDataModel.fid = object.getString(Tags.fid);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_name = object.getString(Tags.city);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.about_me = object.getString(Tags.about_me);
                userDataModel.notification_status = object.getInt(Tags.notification_status);
                userDataModel.language = object.getString(Tags.language);
                prefs.setUserData(userDataModel);
                updateGlobal();

                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                AppDelegate.LogT("after  updatation" + new Prefs(this).getUserdata());
                finish();
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    private void updateGlobal() {
        //Side bar
       // MainActivity.getInstance().new LoadImage().execute(new Prefs(this).getUserdata().avtar);
        MainActivity.getInstance().set_image();
        // For profile
        if (ViewProfileFragment.mHandler != null) {
            AppDelegate.LogT("updateGlobal handler calld");
            ViewProfileFragment.mHandler.sendEmptyMessage(1);
        }
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        location.setText(data.getString(Tags.ADDRESS) + "");
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AppDelegate.showProgressDialog(EditProfileActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(EditProfileActivity.this, "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AppDelegate.hideProgressDialog(EditProfileActivity.this);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = EditProfileActivity.this.getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppDelegate.SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    imageUri = uri;
                    AppDelegate.LogT("uri=" + uri);
                    if (uri != null) {
                        // User had pick an image.
                        Cursor cursor = EditProfileActivity.this
                                .getContentResolver()
                                .query(uri,
                                        new String[]{MediaStore.Images.ImageColumns.DATA},
                                        null, null, null);
                        cursor.moveToFirst();
                        // Link to the image
                        final String imageFilePath = cursor.getString(0);
                        cursor.close();
                        int orientation = 0;
                        if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                            try {
                                ExifInterface ei = new ExifInterface(imageFilePath);
                                orientation = ei.getAttributeInt(
                                        TAG_ORIENTATION,
                                        ORIENTATION_NORMAL);
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            capturedFile = new File(imageFilePath);
                            OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            if (OriginalPhoto != null) {
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(EditProfileActivity.this, OriginalPhoto);
                                if (rotateduri != null) {
                                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        cimg_upload_image.setImageBitmap(OriginalPhoto);
                                        img_loading.setVisibility(View.GONE);
                                    }
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(EditProfileActivity.this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(EditProfileActivity.this, "Failed to deliver result.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(EditProfileActivity.this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(EditProfileActivity.this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(EditProfileActivity.this, "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                }
                break;
            case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);
                //Get our saved file into a bitmap object:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        int orientation = 0;
                        Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inJustDecodeBounds = true;
                        OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                        Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                        try {
                            ExifInterface ei = new ExifInterface(
                                    uri.getPath());

                            orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);

                            Uri rotateduri = getImageUri(EditProfileActivity.this, OriginalPhoto);
                            if (rotateduri != null) {
                                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                    cimg_upload_image.setImageBitmap(OriginalPhoto);
                                    img_loading.setVisibility(View.GONE);
                                }

                            } else {
                                AppDelegate.showToast(EditProfileActivity.this, "Failure delivery result, please try again later.");
                            }

                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(EditProfileActivity.this, "Failure delivery result, please try again later.");
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                break;
            case AppDelegate.PIC_CROP: {
                AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                try {
                    cropimageUri = data.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        if (OriginalPhoto != null) {
                            OriginalPhoto.recycle();
                            OriginalPhoto = null;
                        }
                        OriginalPhoto = MediaStore.Images.Media.getBitmap(EditProfileActivity.this.getContentResolver(), cropimageUri);
                        AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                        OriginalPhoto = Bitmap.createScaledBitmap(
                                OriginalPhoto, 240, 240, true);
                        AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
                        cimg_upload_image.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(EditProfileActivity.this, 100)));
                        img_loading.setVisibility(View.GONE);
                        capturedFile = new File(getNewFile());
                        FileOutputStream fOut = null;
                        try {
                            fOut = new FileOutputStream(capturedFile);
                        } catch (FileNotFoundException e) {
                            AppDelegate.LogE(e);
                        }
                        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        try {
                            fOut.flush();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                    } else {
                        AppDelegate.LogE("at onActivicTyResult failed to crop");
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "cropuser");
            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");
            cropimageUri = EditProfileActivity.this.getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(EditProfileActivity.this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }
}
