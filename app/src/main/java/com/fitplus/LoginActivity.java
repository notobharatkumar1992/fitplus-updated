package com.fitplus;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.FbDetails;
import model.Fb_detail_GetSet;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnReciveServerResponse, OnDialogClickListener {

    private static final int RC_SIGN_IN = 0;
    private static final String TAG = "LOGIN";
    private static final int PROFILE_PIC_SIZE = 400;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    private TextView btnSignIn, create_account, login_with_fb;
    private Intent mainIntent;
    private TextView enter;
    EditText username, password;
    public static CallbackManager callbackManager;
    private String fb_LoginToken;
    private boolean isCalledOnce = false;
    private Fb_detail_GetSet fbUserData;
    private Prefs prefs;
    private ImageView toggle;
    public static Handler mHandler;
    private boolean calledFailedOnce = false;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.login);
        prefs = new Prefs(this);
        findIDS();
        initView();
        setHandler();
        mHandler.sendEmptyMessage(1);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                AppDelegate.LogT("dispatchMessage handler calld");
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(LoginActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(LoginActivity.this);
                } else if (msg.what == 1) {
                    String emailid = AppDelegate.getValue(LoginActivity.this, Tags.Email);
                    if (AppDelegate.isValidString(emailid) || AppDelegate.remember_me) {
                        AppDelegate.Log("emailid", emailid + "" + "hellooo");
                        toggle.setSelected(true);
                        username.setText("" + AppDelegate.getValue(LoginActivity.this, Tags.Email));
                        password.setText("" + AppDelegate.getValue(LoginActivity.this, Tags.Password));
                    } else {
                        AppDelegate.Log("emailid", emailid + "" + "byee");
                        toggle.setSelected(false);
                        username.setText("");
                        password.setText("");
                    }
                }
            }
        };
    }

    /*Override
     public void onDestroyView() {
         super.onDestroyView();
         AppDelegate.LogT("ViewProfile onDestroyView called");
         mHandler = null;
     }*/
    private void findIDS() {
        btnSignIn = (TextView) findViewById(R.id.btn_sign_in);
        login_with_fb = (TextView) findViewById(R.id.login_with_facebook);
        enter = (TextView) findViewById(R.id.enter);
        create_account = (TextView) findViewById(R.id.create_account);
        findViewById(R.id.forgot_password).setOnClickListener(this);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        enter.setOnClickListener(this);
        create_account.setOnClickListener(this);
        login_with_fb.setOnClickListener(this);
        AppDelegate.LogT("login notification on -====" + (prefs.getnotification(Tags.notification_value)));
    }

    private void initView() {
        toggle = (ImageView) findViewById(R.id.remember_me);
        toggle.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);
       /* GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestScopes(new Scope("https://www.googleapis.com/auth/user.birthday.read"))
                .requestEmail()
                .build();*/
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(new Scope("https://www.googleapis.com/auth/user.birthday.read"))
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();

       /* String emailid = AppDelegate.getValue(this, Tags.Email);
        if (AppDelegate.isValidString(emailid)) {
            AppDelegate.Log("emailid", emailid + "" + "hellooo");
            toggle.setSelected(true);
            username.setText("" + AppDelegate.getValue(this, Tags.Email));
            password.setText("" + AppDelegate.getValue(this, Tags.Password));
        } else {
            AppDelegate.Log("emailid", emailid + "" + "byee");
            toggle.setSelected(false);
            username.setText("");
            password.setText("");
        }*/
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            signOutFromGplus();
        } else {
            if (FacebookSdk.isInitialized()) {
                AppDelegate.LogT("INITIALIZED facebook sdk");
                disconnectFromFacebook();
            }
        }
    }

    public void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
                AppDelegate.LogT("Logout from facebook");
            }
        }).executeAsync();
    }

    @Override
    public void onConnected(Bundle arg0) {
        AppDelegate.LogT("onConnection called");
        mSignInClicked = false;
        getProfileInformation();

    }

    private void signInWithGplus() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    private void signOutFromGplus() {
        Log.v(TAG, "In if condition to log off");
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            AppDelegate.LogT("Logout from GOOGLE PLUS");
            // mGoogleApiClient.connect();
            mSignInClicked = true;
            btnSignIn.setEnabled(true);
        }
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String last_name = currentPerson.getName().getMiddleName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                int gender = currentPerson.getGender();
                String Gender = null;
                if (gender == 0) {
                    Gender = "Male";
                } else if (gender == 1) {
                    Gender = "Female";
                } else if (gender == 2) {
                    Gender = "Other";
                }
                if (AppDelegate.isValidString(personPhotoUrl) && personPhotoUrl.contains("&")) {
                    personPhotoUrl = personPhotoUrl.replace("&", "~");
                }
                String social_id = currentPerson.getId();
                callGooglePlusAsync(social_id, personName, last_name, Gender);
                Fb_detail_GetSet fb_detail_getSet = new Fb_detail_GetSet();
                fb_detail_getSet.setGid(currentPerson.getId());
                fb_detail_getSet.setSocial_id("");
                fb_detail_getSet.setFirstname(currentPerson.getDisplayName());
                fb_detail_getSet.setLast_name(currentPerson.getName().getMiddleName());
                fb_detail_getSet.setGender(Gender);
                fb_detail_getSet.setProfile_pic(personPhotoUrl);
                String birthday = currentPerson.getBirthday();
                if (AppDelegate.isValidString(birthday)) {
                    Date dateformate = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
                    AppDelegate.LogT("timeformate==>" + dateformate + "date");
                    birthday = new SimpleDateFormat("dd/MM/yyyy").format(dateformate);
                    fb_detail_getSet.setBitrhday(birthday);
                }
                fb_detail_getSet.setCity(currentPerson.getCurrentLocation());
                AppDelegate.LogT("google get data==" + fb_detail_getSet + "");
                bundle = new Bundle();
                bundle.putParcelable(Tags.data, fb_detail_getSet);
                AppDelegate.LogT("google get data==" + fb_detail_getSet + "");
            } else {
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callGooglePlusAsync(String social_id, String name, String last_name, String gender) {
        if (social_id == null) {
            AppDelegate.showAlert(this, "Google Plus data is incorrect, please try after some time.");
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            try {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_id, String.valueOf(AppDelegate.getValue(this, Tags.REGISTRATION_ID)).trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_type, String.valueOf(2));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.gid, social_id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.fid, "");

                PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SOCIAL_SIGNIN,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
                if (AppDelegate.getBooleanFromgoogle(this) == false) {
                    AppDelegate.saveBooleanInGoogle(this, true);
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.SOCIAL_SIGNIN)) {
            new Prefs(this).clearTempPrefs();
            parseSocialLogin(result);
        } else if (apiName.equals(ServerRequestConstants.LOGIN)) {
            new Prefs(this).clearTempPrefs();
            parseLogin(result);
        }
    }

    private void parseSocialLogin(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.verify) == 0) {
                Intent intent = new Intent(LoginActivity.this, SocialSignIn2Activity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.verify) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.gid = object.getString(Tags.gid);
                userDataModel.fid = object.getString(Tags.fid);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.notification_status = object.getInt(Tags.notification_status);
                userDataModel.language = object.getString(Tags.language);
                prefs.setUserData(userDataModel);
                AppDelegate.LogT("user data => " + prefs.getUserdata());
                prefs.putUserId(String.valueOf(userDataModel.userId));
                //  AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, LoginActivity.this);
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    if (!AppDelegate.isValidString(String.valueOf(prefs.getUserdata().fat_status))) {
                        startActivity(new Intent(LoginActivity.this, BeginnerQuestionActivity.class));
                    } else {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }
                    finish();
                } else {
                    startActivity(new Intent(LoginActivity.this, GetStartActivity.class));
                    finish();
                }
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void parseLogin(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_name = object.getString(Tags.city);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.notification_status = object.getInt(Tags.notification_status);
                userDataModel.language = object.getString(Tags.language);
                prefs.setUserData(userDataModel);
                prefs.setRemembered(toggle.isSelected() + "");
                prefs.putUserId(String.valueOf(userDataModel.userId));
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    if (!AppDelegate.isValidString(String.valueOf(prefs.getUserdata().fat_status))) {
                        startActivity(new Intent(LoginActivity.this, BeginnerQuestionActivity.class));
                    } else {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }
                    finish();
                } else {
                    startActivity(new Intent(LoginActivity.this, GetStartActivity.class));
                    finish();
                }
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                if (!AppDelegate.isValidString(String.valueOf(prefs.getUserdata().fat_status))) {
                    startActivity(new Intent(LoginActivity.this, BeginnerQuestionActivity.class));
                } else {
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }
                finish();
            } else {
                startActivity(new Intent(LoginActivity.this, GetStartActivity.class));
                finish();
            }
        }
    }


    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        AppDelegate.LogT("onConnectionSuspended called");
        mGoogleApiClient.connect();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in:
                if (AppDelegate.haveNetworkConnection(this, false)) {
                    try {
                        calledFailedOnce = true;
                        signInWithGplus();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else {
                    AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
                }

                break;
            case R.id.login_with_facebook:
                if (AppDelegate.haveNetworkConnection(this, false)) {
                    openFacebook();
                } else {
                    AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
                }

                break;
            case R.id.enter:
                checkValidity();

                break;
            case R.id.remember_me:
                if (toggle.isSelected()) {
                    username.setText("");
                    password.setText("");
                    toggle.setSelected(false);
                } else {
                    toggle.setSelected(true);
                }
                break;
            case R.id.create_account:
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
                break;
            case R.id.forgot_password:
                intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void checkValidity() {
        if (!AppDelegate.CheckEmail(username.getText().toString())) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.foremail), "Alert!!!");
        } else if (!AppDelegate.isValidString(password.getText().toString())) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forpass), "Alert!!!");
        } else if (password.getText().toString().length() < 6) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forlength), "Alert!!!");
        } else if (!AppDelegate.password_validation(this, password.getText().toString())) {

        } else {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                if (toggle.isSelected()) {
                    AppDelegate.Log("toggle _if", toggle.isSelected() + "");
                    AppDelegate.save(this, username.getText().toString(), Tags.Email);
                    AppDelegate.save(this, password.getText().toString(), Tags.Password);
                }
                executeLogin();
            } else {
                AppDelegate.ShowDialog(this, getResources().getString(R.string.not_connected), "Alert!!!");
            }
        }
    }

    private void executeLogin() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.email, username.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.password, password.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_id, String.valueOf(AppDelegate.getValue(this, Tags.REGISTRATION_ID)).trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_type, String.valueOf(2));
                //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.LOGIN,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    public void openFacebook() {
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",  "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("login success" + loginResult.getAccessToken() + "");
                        AppDelegate.LogT("onSuccess = " + loginResult.getAccessToken() + "");
                        AppDelegate.showProgressDialog(LoginActivity.this);
                        fb_LoginToken = loginResult.getAccessToken().toString();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Application code
                                        if (response != null) {
                                            fbUserData = new Fb_detail_GetSet();
                                            fbUserData = new FbDetails().getFacebookDetail(response.getJSONObject().toString());
                                            AppDelegate.LogT("Facebook details==" + fbUserData + "");
                                            AppDelegate.hideProgressDialog(LoginActivity.this);
                                            callAsyncFacebookVerify(fbUserData);
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,id,name,gender,birthday,picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");

                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(LoginActivity.this);
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook();
                                }
                            }
                        }
                    }
                });
    }

    private void callAsyncFacebookVerify(Fb_detail_GetSet fbUserData) {
        bundle = new Bundle();
        bundle.putParcelable(Tags.data, fbUserData);
        if (fbUserData == null) {
            AppDelegate.showAlert(this, "Facebook data is incorrect, please try after some time.");
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            try {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_id, String.valueOf(AppDelegate.getValue(this, Tags.REGISTRATION_ID)).trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_type, String.valueOf(2));
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.gid, "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.fid, fbUserData.getSocial_id());

                // AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));
                PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SOCIAL_SIGNIN,
                        mPostArrayList, null);
                //mHandler.sendEmptyMessage(10);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();

            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {

            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
            //AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("LoginActivity onActivityResult called");
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        AppDelegate.LogT("onConnectionFailed called => " + result);
        try {
            if (calledFailedOnce) {
                calledFailedOnce = false;
                mConnectionResult = result;
                resolveSignInError();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
//        if (!result.hasResolution()) {
//            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
//            return;
//        }
//        if (!mIntentInProgress) {
//            mConnectionResult = result;
//            if (mSignInClicked) {
//                resolveSignInError();
//            }
//        }
    }
}
